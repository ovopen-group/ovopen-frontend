/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define BOOST_TEST_MODULE OvOpenTest
#define BOOST_TEST_NO_MAIN
#define BOOST_TEST_ALTERNATIVE_INIT_API

#include <asio.hpp>
#include <atomic>
#include <iostream>
#include <unordered_map>
#include "zlib.h"

#include "packet.hpp"
#include "packetHandlers.hpp"
#include "protocolHandler.hpp"
#include "coreUtil.hpp"
#include "serverLogger.hpp"

#include "INIReader.h"
#include "log.hpp"
#include "serverLogger.hpp"

#define CATCH_CONFIG_MAIN
#ifdef __GNUC__
#define DO_NOT_USE_WMAIN
#endif
#include "catch.hpp"



bool OVProtocolHandler::onPreloginPacket(OVPacketHeader* header)
{
   return true;
}

void OVProtocolHandler::onLogout()
{
}

void addDummyPacket(uint8_t* buffer, int idx, int pos, int size)
{
   OVPacketHeader* header = (OVPacketHeader*)(buffer+pos);
   header->setBasicPacket();
   header->setNativeSize(size);
   header->setNativeId(idx);
}

void addDummyEncryptedPacket(uint8_t* buffer, int pos, int size, BLOWFISH_CONTEXT* key)
{
   OVPacketHeader* header = (OVPacketHeader*)(buffer+pos);
   header->setBasicPacket();
   header->setNativeSize(size);
   header->setEncrypted();
   EncryptPacketInPlace(header, key);
}

// Check invalid flag is set for bad data
TEST_CASE( "PacketIteratorInvalidTest" )
{
   const int bufferSize = 1869946;
   uint8_t packetBuffer[bufferSize];
   uint8_t modBuffer[bufferSize];
   
   int count=0;
   for (uint32_t i=0; i<sizeof(packetBuffer); i+= 106)
   {
      addDummyPacket(packetBuffer, count+1, i, 100);
      for (int k=0; k<100; k++)
      {
         packetBuffer[i+k+6] = count;
      }
      count++;
   }
   
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   PacketIterator<bufferSize> packetItr(&modBuffer[0],
                                        0, 106,
                                        0,
                                        NULL);
   
   // Invalidate first packet
   modBuffer[0] = 0;
   
   REQUIRE((packetItr.next_packet() == NULL));
   REQUIRE(packetItr.invalid_stream);
   
   // Try again but with processed range being correct
   packetItr.reset(bufferSize);
   packetItr.processed_offset = 106;
   
   REQUIRE((packetItr.next_packet() != NULL));
   REQUIRE(!packetItr.invalid_stream);
   
   // Try again but make the range not cover the whole packet
   // (packet should be null in this case since packet isn't ready)
   packetItr.reset(bufferSize);
   packetItr.processed_offset = 6;
   
   REQUIRE((packetItr.next_packet() == NULL));
   REQUIRE(packetItr.invalid_stream);
   
   // Try reading twice
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   packetItr.reset(bufferSize);
   REQUIRE((packetItr.next_packet() != NULL));
   REQUIRE(!packetItr.invalid_stream);
   packetItr.pos = 0;
   OVPacketHeader* middlePacket = packetItr.next_packet();
   REQUIRE((middlePacket != NULL));
   REQUIRE(!packetItr.invalid_stream);
   REQUIRE((packetItr.next_packet() != NULL));
   REQUIRE(!packetItr.invalid_stream);
   
   // Nerf middle packet and check everything again
   packetItr.dispose_packet(middlePacket);
   packetItr.pos = 0;
   REQUIRE((packetItr.next_packet() != NULL));
   REQUIRE(!packetItr.invalid_stream);
   REQUIRE((packetItr.next_packet() != NULL));
   REQUIRE(!packetItr.invalid_stream);
   REQUIRE((packetItr.next_packet() != NULL));
   REQUIRE(!packetItr.invalid_stream);
   REQUIRE((packetItr.next_packet() != NULL));
   REQUIRE(!packetItr.invalid_stream);
}

// Check partial reads occur as expected
TEST_CASE( "PacketIteratorPartialReadTest" )
{
   const int bufferSize = 1869946;
   uint8_t packetBuffer[bufferSize];
   uint8_t modBuffer[bufferSize];
   
   int count=0;
   for (uint32_t i=0; i<sizeof(packetBuffer); i+= 106)
   {
      addDummyPacket(packetBuffer, count+1, i, 100);
      for (int k=0; k<100; k++)
      {
         packetBuffer[i+k+6] = count;
      }
      count++;
   }
   
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   PacketIterator<bufferSize> packetItr(&modBuffer[0],
                                        0, 3,
                                        0,
                                        NULL);
   
   OVPacketHeader* packet = packetItr.next_packet();
   REQUIRE((packet == NULL));
   REQUIRE((packetItr.pos == 0));
   REQUIRE(!packetItr.eof());
   REQUIRE(!packetItr.invalid_stream);
   
   packetItr.reset(6);
   packet = packetItr.next_packet();
   REQUIRE((packet == NULL));
   REQUIRE((packetItr.pos == 0));
   REQUIRE(!packetItr.eof());
   REQUIRE(!packetItr.invalid_stream);
   
   packetItr.reset(106);
   packet = packetItr.next_packet();
   REQUIRE(packet);
   REQUIRE(packet->getNativeId() == 1);
   REQUIRE(packetItr.pos == 106);
   REQUIRE(packetItr.eof());
   REQUIRE(!packetItr.invalid_stream);
   
   packetItr.reset(110);
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   packet = packetItr.next_packet();
   REQUIRE(packet);
   REQUIRE(packet->getNativeId() == 1);
   REQUIRE(packetItr.pos == 106);
   REQUIRE(!packetItr.eof());
   REQUIRE(!packetItr.invalid_stream);
   packet = packetItr.next_packet();
   REQUIRE((packet == NULL));
   REQUIRE(packetItr.pos == 106);
   REQUIRE(!packetItr.eof());
   REQUIRE(!packetItr.invalid_stream);
   
   // Back to beginning but remember processed offset
   packetItr.pos = 0;
   packet = packetItr.next_packet();
   REQUIRE(packet);
   packetItr.dispose_packet(packet);
   packet = packetItr.next_packet();
   REQUIRE((packet == NULL));
   REQUIRE(packetItr.pos == 0);
   REQUIRE(packetItr.processed_offset == 0);
   REQUIRE(!packetItr.eof());
   REQUIRE(!packetItr.invalid_stream);
}

// Check Iterator and buffer are modified correctly when disposing of packets
TEST_CASE( "PacketIteratorDisposePacketTest" )
{
   const int bufferSize = 1869946;
   uint8_t packetBuffer[bufferSize];
   uint8_t modBuffer[bufferSize];
   
   int count=0;
   for (uint32_t i=0; i<sizeof(packetBuffer); i+= 106)
   {
      addDummyPacket(packetBuffer, count, i, 100);
      for (int k=0; k<100; k++)
      {
         packetBuffer[i+k+6] = count;
      }
      count++;
   }
   
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   PacketIterator<bufferSize> packetItr(&modBuffer[0],
                                        0, bufferSize,
                                        0,
                                        NULL);
   
   OVPacketHeader* packet = packetItr.next_packet();
   REQUIRE(packet->getNativeId() == 0);
   REQUIRE(packetItr.pos == 106);
   packetItr.dispose_packet(packet);
   REQUIRE(packetItr.pos == 0);
   
   packet = packetItr.next_packet();
   REQUIRE(packet->getNativeId() == 1);
   packetItr.dispose_packet(packet);
   REQUIRE(packetItr.pos == 0);
   
   packet = packetItr.next_packet();
   REQUIRE(packet->getNativeId() == 2);
   packetItr.dispose_packet(packet);
   REQUIRE(packetItr.pos == 0);
   
   // Remove middle
   packet = packetItr.next_packet();
   REQUIRE(packet->getNativeId() == 3);
   packet = packetItr.next_packet();
   REQUIRE(packet->getNativeId() == 4);
   OVPacketHeader* prePacket = packet;
   packetItr.dispose_packet(packet);
   packet = packetItr.next_packet();
   REQUIRE(prePacket == packet);
   REQUIRE(packet->getNativeId() == 5);
   packetItr.dispose_packet(packet);
   packet = packetItr.next_packet();
   REQUIRE(packet->getNativeId() == 6);
   
   // Go back to start
   packetItr.pos = 0;
   packet = packetItr.next_packet();
   REQUIRE(packet->getNativeId() == 3);
   
   // Dispose until we get to an unprocessed packet
   packetItr.dispose_packet(packet);
   packetItr.dispose_packet(packet);
   packetItr.dispose_packet(packet);
   packetItr.dispose_packet(packet);
   REQUIRE(packetItr.pos == 0);
   REQUIRE(packetItr.processed_offset == 0);
   REQUIRE(packet->isValid());
   REQUIRE(!packet->isProcessed());
   
   // Get next packet, making it processed
   prePacket = packet;
   packet = packetItr.next_packet();
   REQUIRE(packet == prePacket);
   REQUIRE(packet->isProcessed());
   REQUIRE(prePacket->isProcessed());
   REQUIRE(packetItr.processed_offset == 106);
   REQUIRE(packetItr.pos == 106);
}

// Check Iterator and buffer are modified correctly when disposing of ranges
TEST_CASE( "PacketIteratorDisposeRangeTest" )
{
   const int bufferSize = 256;
   uint8_t packetBuffer[bufferSize];
   uint8_t modBuffer[bufferSize];
   
   for (uint32_t i=0; i<sizeof(packetBuffer); i++)
   {
      packetBuffer[i] = i%256;
   }
   
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   //
   // Start of buffer
   //
   PacketIterator<bufferSize> packetItr = PacketIterator<bufferSize> (&modBuffer[0],
                                        0, bufferSize,
                                        0,
                                        NULL);
   
   
   packetItr.dispose_buffer(0, 100);
   REQUIRE(packetItr.pos == 0);
   REQUIRE(packetItr.end == 156);
   REQUIRE(packetItr.processed_offset == 0);
   for (uint32_t i=0; i<101; i++)
   {
      REQUIRE(modBuffer[i] == i+100);
   }
   
   //
   // Middle of buffer
   //
   packetItr.reset(bufferSize);
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   packetItr.dispose_buffer(50, 100);
   REQUIRE(packetItr.pos == 0);
   REQUIRE(packetItr.end == 206);
   REQUIRE(packetItr.processed_offset == 0);
   for (uint32_t i=0; i<50; i++)
   {
      REQUIRE(modBuffer[i] == i);
   }
   for (int i=50; i<(256-100); i++)
   {
      REQUIRE(modBuffer[i] == i+50);
   }
   REQUIRE(modBuffer[255] == 255); // no movement here
   
   //
   // End of buffer
   //
   packetItr.reset(bufferSize);
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   packetItr.dispose_buffer(100, 256);
   REQUIRE(packetItr.pos == 0);
   REQUIRE(packetItr.end == 100);
   REQUIRE(packetItr.processed_offset == 0);
   for (uint32_t i=0; i<256; i++) // i.e. nothing moved
   {
      REQUIRE(modBuffer[i] == i);
   }
   
   //
   // pos < start TODO
   //
   packetItr.reset(bufferSize);
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   packetItr.pos = packetItr.processed_offset = 50;
   packetItr.dispose_buffer(100, 150);
   REQUIRE(packetItr.pos == 50);
   REQUIRE(packetItr.end == 206);
   REQUIRE(packetItr.processed_offset == 50);
   
   //
   // pos > start, pos < end
   //
   packetItr.reset(bufferSize);
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   packetItr.pos = packetItr.processed_offset = 120;
   packetItr.dispose_buffer(100, 150);
   REQUIRE(packetItr.pos == 100);
   REQUIRE(packetItr.end == 206);
   REQUIRE(packetItr.processed_offset == 100);
   
   //
   // pos > end
   //
   packetItr.reset(bufferSize);
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   packetItr.pos = packetItr.processed_offset = 160;
   packetItr.dispose_buffer(100, 150);
   REQUIRE(packetItr.pos == 110);
   REQUIRE(packetItr.end == 206);
   REQUIRE(packetItr.processed_offset == 110);
}

// Test packets are decrypted in-place
TEST_CASE( "PacketIteratorEncTest" )
{
   const int bufferSize = 1870000;
   uint8_t packetBuffer[bufferSize];
   uint8_t modBuffer[bufferSize];
   
   for (uint32_t i=0; i<sizeof(packetBuffer); i++)
   {
      packetBuffer[i] = rand()%255;
   }
   
   const char* securityCode = "TESTKEY";
   BLOWFISH_CONTEXT encKey;
   BLOWFISH_Init(&encKey, (unsigned char*)securityCode, strlen(securityCode), BLOWFISH_MODE_ECB, 0, 0);
   
   for (uint32_t i=0; i<sizeof(packetBuffer); i+= 110)
   {
      addDummyEncryptedPacket(packetBuffer, i, 100, &encKey);
   }
   
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   PacketIterator<bufferSize> packetItr(&modBuffer[0],
                                            0, bufferSize,
                                            0,
                                            &encKey);
   
   OVPacketHeader* header = packetItr.next_packet();
   REQUIRE(header);
   REQUIRE(header->getPacketSize() == 104);
   REQUIRE(packetItr.pos == 110);
   REQUIRE(packetItr.processed_offset == 110);
   REQUIRE((header->isDecrypted() && header->isProcessed()));
   REQUIRE((((uint8_t*)header) - modBuffer) == 0);
   uint32_t packet_int = *((uint32_t*)header->getData());
   
   header = packetItr.next_packet();
   REQUIRE(header);
   REQUIRE(header->getPacketSize() == 104);
   REQUIRE(packetItr.pos == 220);
   REQUIRE(packetItr.processed_offset == 220);
   REQUIRE((((uint8_t*)header) - modBuffer) == 110);
   REQUIRE((header->isDecrypted() && header->isProcessed()));
   uint32_t packet2_int = *((uint32_t*)header->getData());
   
   // Go through a second time. Decryption should not happen again.
   packetItr.pos = 0;
   
   header = packetItr.next_packet();
   REQUIRE(header);
   REQUIRE(header->getPacketSize() == 104);
   REQUIRE(packetItr.pos == 110);
   REQUIRE(packetItr.processed_offset == 220);
   REQUIRE((header->isDecrypted() && header->isProcessed()));
   REQUIRE((((uint8_t*)header) - modBuffer) == 0);
   
   REQUIRE(*((uint32_t*)header->getData()) == packet_int);
   
   header = packetItr.next_packet();
   REQUIRE(header);
   REQUIRE(header->getPacketSize() == 104);
   REQUIRE(packetItr.pos == 220);
   REQUIRE(packetItr.processed_offset == 220);
   REQUIRE((((uint8_t*)header) - modBuffer) == 110);
   REQUIRE((header->isDecrypted() && header->isProcessed()));
   
   REQUIRE(*((uint32_t*)header->getData()) == packet2_int);
   
   while (header)
   {
      header = packetItr.next_packet();
   }
   
   REQUIRE(packetItr.pos == sizeof(packetBuffer));
   REQUIRE(packetItr.processed_offset == sizeof(packetBuffer));
}

// ReadBufferProcessor tests

class TestPacketHandler
{
public:
   ReadBufferProcessor::ReadState mReadState;
   uint32_t mReadWindowLimit;
   bool mClosing;
   bool mPacketAccept;
   bool mJunkPacket;
   std::vector<uint32_t> mPacketLog;
   
   uint32_t mLastReadOffset;
   uint32_t mLastReadSize;
   
   bool isReading()
   {
      return mReadState == ReadBufferProcessor::READ_READING;
   }
   
   uint32_t getReadWindowLimit()
   {
      return mReadWindowLimit;
   }
   
   bool isClosing()
   {
      return mClosing;
   }
   
   bool onPacket(OVPacketHeader* packet)
   {
      mPacketLog.push_back(packet->getNativeId());
      return mPacketAccept;
   }
   
   void reset()
   {
      clearReading();
      mClosing = false;
      mPacketAccept = true;
      mJunkPacket = false;
      mPacketLog.clear();
      mLastReadSize = 0;
      mLastReadOffset = 0;
   }
   
   void setReading()
   {
      mReadState = ReadBufferProcessor::READ_READING;
   }
   
   void clearReading()
   {
      mReadState = ReadBufferProcessor::READ_IDLE;
   }
   
   void doRead(uint32_t offset, uint32_t size)
   {
      mLastReadOffset = offset;
      mLastReadSize = size;
   }
   
   void onJunkPacket()
   {
      mJunkPacket = true;
   }
   
   void onClose()
   {
      mClosing = true;
   }
};

// Reads should be limited by window size
TEST_CASE( "ReadProcessor_ReadLimiter" )
{
   ReadBufferProcessor processor;
   TestPacketHandler handler;
   
   handler.reset();
   handler.mReadWindowLimit = 200;
   handler.mPacketAccept = false;
   
   const int bufferSize = 1869946;
   uint8_t packetBuffer[bufferSize];
   uint8_t modBuffer[bufferSize];
   
   int count=0;
   for (uint32_t i=0; i<sizeof(packetBuffer); i+= 106)
   {
      addDummyPacket(packetBuffer, count, i, 100);
      for (int k=0; k<100; k++)
      {
         packetBuffer[i+k+6] = count;
      }
      count++;
   }
   
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   PacketIterator<bufferSize> packetItr(&modBuffer[0],
                                        0, 100,
                                        0,
                                        NULL);
   
   // Pretend we read 100 bytes
   processor.mBytesReadDuringWindow = 100;
   processor.processData(&handler, packetItr, 0);
   auto state = processor.enqueueRead(&handler, packetItr);
   
   REQUIRE(state == ReadBufferProcessor::READ_READING);
   REQUIRE(handler.mLastReadOffset == 100);
   REQUIRE(handler.mLastReadSize == 100);
   
   // Pretend we read another 110 bytes
   packetItr.reset(210, 100);
   handler.reset();
   handler.mPacketAccept = false;
   processor.mBytesReadDuringWindow += 110;
   processor.processData(&handler, packetItr, 0);
   state = processor.enqueueRead(&handler, packetItr);
   
   REQUIRE(state == ReadBufferProcessor::READ_LIMITED);
   REQUIRE(handler.mLastReadOffset == 0); // 210
   REQUIRE(handler.mLastReadSize == 0);
   
   // Try again just for the heck of it
   handler.reset();
   packetItr.reset(210, 100);
   handler.mPacketAccept = false;
   processor.processData(&handler, packetItr, 0);
   state = processor.enqueueRead(&handler, packetItr);
   
   REQUIRE(state == ReadBufferProcessor::READ_LIMITED);
   REQUIRE(handler.mLastReadOffset == 0);
   REQUIRE(handler.mLastReadSize == 0);
   
   // Advance time, should now read
   handler.reset();
   packetItr.reset(210, 100);
   handler.mPacketAccept = false;
   processor.mBytesReadDuringWindow = 0;
   processor.processData(&handler, packetItr, 1001);
   state = processor.enqueueRead(&handler, packetItr);
   
   REQUIRE(state == ReadBufferProcessor::READ_READING);
   REQUIRE(handler.mLastReadOffset == 210);
   REQUIRE(handler.mLastReadSize == 200);
   
   // Give some 50 more bytes of data
   handler.reset();
   packetItr.reset(260, 100);
   handler.mPacketAccept = false;
   processor.mBytesReadDuringWindow += 50;
   processor.processData(&handler, packetItr, 1001);
   state = processor.enqueueRead(&handler, packetItr);
   
   REQUIRE(state == ReadBufferProcessor::READ_READING);
   REQUIRE(handler.mLastReadOffset == 260);
   REQUIRE(handler.mLastReadSize == 150);
}

// Packets should be processed in order, buffer should be modified accordingly
TEST_CASE( "ReadProcessor_ProcessesPackets" )
{
   ReadBufferProcessor processor;
   TestPacketHandler handler;
   
   handler.reset();
   handler.mReadWindowLimit = 0xFFFFFF;
   handler.mPacketAccept = true;
   
   const int bufferSize = 1869946;
   uint8_t packetBuffer[bufferSize];
   uint8_t modBuffer[bufferSize];
   
   int count=0;
   for (uint32_t i=0; i<sizeof(packetBuffer); i+= 106)
   {
      addDummyPacket(packetBuffer, count, i, 100);
      for (int k=0; k<100; k++)
      {
         packetBuffer[i+k+6] = count;
      }
      count++;
   }
   
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   PacketIterator<bufferSize> packetItr(&modBuffer[0],
                                        0, bufferSize,
                                        0,
                                        NULL);
   
   // Read in every packet
   processor.mBytesReadDuringWindow = 100;
   REQUIRE(processor.processData(&handler, packetItr, 0) == true);
   REQUIRE(!handler.mJunkPacket);
   REQUIRE(!handler.isClosing());
   auto state = processor.enqueueRead(&handler, packetItr);
   
   REQUIRE(state == ReadBufferProcessor::READ_READING);
   REQUIRE(handler.mLastReadOffset == 0);
   REQUIRE(handler.mLastReadSize == bufferSize);
   REQUIRE(handler.mPacketLog.size() == 17641);
   REQUIRE(handler.mPacketLog[10] == 10);
   REQUIRE(packetItr.pos == 0);
   REQUIRE(packetItr.end == 0);
   
   // Partial read
   handler.reset();
   packetItr.reset(50, 0);
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   processor.mBytesReadDuringWindow = 100;
   REQUIRE(processor.processData(&handler, packetItr, 0) == true);
   state = processor.enqueueRead(&handler, packetItr);
   
   REQUIRE(state == ReadBufferProcessor::READ_READING);
   REQUIRE(handler.mLastReadOffset == 50);
   REQUIRE(handler.mLastReadSize == bufferSize-50);
   REQUIRE(handler.mPacketLog.size() == 0);
   REQUIRE(packetItr.pos == 0);
   REQUIRE(packetItr.end == 50);
   REQUIRE(!handler.mJunkPacket);
   REQUIRE(!handler.isClosing());
   
   // Ignoring packets should leave data in-place
   packetItr.reset(106*10, 0);
   handler.reset();
   handler.mPacketAccept = false;
   REQUIRE(processor.processData(&handler, packetItr, 0) == true);
   state = processor.enqueueRead(&handler, packetItr);
   REQUIRE(handler.mPacketLog.size() == 10);
   REQUIRE(handler.mPacketLog[5] == 5);
   REQUIRE(packetItr.pos == 1060);
   REQUIRE(packetItr.processed_offset == 1060);
   REQUIRE(packetItr.end == 1060);
   
   packetItr.reset(106*10, 106*10);
   handler.reset();
   handler.mPacketAccept = false;
   REQUIRE(processor.processData(&handler, packetItr, 0) == true);
   state = processor.enqueueRead(&handler, packetItr);
   REQUIRE(handler.mPacketLog.size() == 10);
   REQUIRE(handler.mPacketLog[5] == 5);
   REQUIRE(packetItr.pos == 1060);
   REQUIRE(packetItr.processed_offset == 1060);
   REQUIRE(packetItr.end == 1060);
}

// Junk handler should be called for junk packets
TEST_CASE( "ReadProcessor_JunkPackets" )
{
   ReadBufferProcessor processor;
   TestPacketHandler handler;
   
   handler.reset();
   handler.mReadWindowLimit = 0xFFFFFF;
   handler.mPacketAccept = true;
   
   const int bufferSize = 1060;
   uint8_t packetBuffer[bufferSize];
   uint8_t modBuffer[bufferSize];
   
   int count=0;
   for (uint32_t i=0; i<sizeof(packetBuffer); i+= 106)
   {
      addDummyPacket(packetBuffer, count, i, 100);
      for (int k=0; k<10; k++)
      {
         packetBuffer[i+k+6] = count;
      }
      count++;
   }
   
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   PacketIterator<bufferSize> packetItr(&modBuffer[0],
                                        0, bufferSize,
                                        0,
                                        NULL);
   
   // Nuke second packet
   modBuffer[107] = 0;
   
   // Read in every packet
   processor.mBytesReadDuringWindow = 100;
   REQUIRE(processor.processData(&handler, packetItr, 0) == false);
   REQUIRE(handler.mJunkPacket);
   REQUIRE(handler.isClosing());
   REQUIRE(handler.mPacketLog.size() == 1);
   REQUIRE(handler.mPacketLog[0] == 0);
   REQUIRE(packetItr.pos == 0);
   REQUIRE(packetItr.end == bufferSize-106);
}

// Should not try and read past end of buffer
TEST_CASE( "ReadProcessor_BufferFull" )
{
   ReadBufferProcessor processor;
   TestPacketHandler handler;
   
   handler.reset();
   handler.mReadWindowLimit = 0xFFFFFF;
   handler.mPacketAccept = true;
   
   const int bufferSize = 1060;
   uint8_t packetBuffer[bufferSize];
   uint8_t modBuffer[bufferSize];
   
   int count=0;
   for (uint32_t i=0; i<sizeof(packetBuffer); i+= 106)
   {
      addDummyPacket(packetBuffer, count, i, 100);
      for (int k=0; k<10; k++)
      {
         packetBuffer[i+k+6] = count;
      }
      count++;
   }
   
   memcpy(modBuffer, packetBuffer, sizeof(packetBuffer));
   
   PacketIterator<bufferSize> packetItr(&modBuffer[0],
                                        0, bufferSize-5,
                                        0,
                                        NULL);
   
   
   // Read in every packet
   auto state = processor.enqueueRead(&handler, packetItr);
   REQUIRE(state == ReadBufferProcessor::READ_READING);
   REQUIRE(handler.mLastReadOffset == bufferSize-5);
   REQUIRE(handler.mLastReadSize == 5);
   
   packetItr.end = handler.mLastReadOffset + handler.mLastReadSize;
   handler.reset();
   state = processor.enqueueRead(&handler, packetItr);
   REQUIRE(state == ReadBufferProcessor::READ_BUFFER_FULL);
   REQUIRE(handler.mLastReadOffset == 0);
   REQUIRE(handler.mLastReadSize == 0);
}

TEST_CASE("EncodePacket with PacketBuilder")
{
   MemStream stream(4096);
   PacketBuilder builder(&stream);
   builder.beginBasicPacket(2);

   REQUIRE((stream.getPosition() == 6));

   uint8_t secKey[20];
   memset(secKey, '\0', sizeof(secKey));
   
   const char* strWrite = "TEST STRING";
   stream.write(strlen(strWrite)+1, strWrite);

   BLOWFISH_CONTEXT encKey;
   BLOWFISH_Init(&encKey, (uint8_t*)&secKey[0], 20, BLOWFISH_MODE_ECB, 0, 0);
   
   OVFreeableData* dat = builder.commitToEncodedPacket(&encKey);
   REQUIRE((dat != NULL));
   if (!dat)
      return;

   OVPacketHeader* hdr = (OVPacketHeader*)dat->ptr();
   REQUIRE((hdr->isValid() == true));
   REQUIRE((hdr->getNativeId() == 2));
   REQUIRE((hdr->getPacketSize() == strlen(strWrite)+1));
   dat->free();

   // Compress packet

   hdr = (OVPacketHeader*)stream.mPtr;
   builder.setCompressed(true);
   dat = builder.commitToEncodedPacket(&encKey);
   hdr = (OVPacketHeader*)dat->ptr();
   REQUIRE((hdr->isValid() == true));
   REQUIRE((hdr->getNativeId() == 2));
   REQUIRE((hdr->getPacketSize() != strlen(strWrite)+1));
   REQUIRE((hdr->isCompressed() == true));
   REQUIRE((hdr->getPacketSize() == dat->size()-6));

   // Decode compressed packet
   OVPacketInfo info = DecodePacket(hdr, &encKey, true);
   REQUIRE((info.code == 2));
   REQUIRE((info.isAuto == false));
   REQUIRE((info.isValid == true));
   REQUIRE((info.size == strlen(strWrite)+1));
   REQUIRE((memcmp(info.data, strWrite, strlen(strWrite)+1) == 0));
   info.cleanup();
   
   builder.setCompressed(false);
   
   // Encrypt packet

   hdr = (OVPacketHeader*)stream.mPtr;
   builder.setEncrypted(true);
   dat = builder.commitToEncodedPacket(&encKey);
   hdr = (OVPacketHeader*)dat->ptr();
   REQUIRE((hdr->isValid() == true));
   REQUIRE((hdr->getNativeId() == 2));
   REQUIRE((hdr->getPacketSize() != strlen(strWrite)+1));
   REQUIRE((hdr->isEncrypted() == true));
   REQUIRE((hdr->getPacketSize() == dat->size()-6));

   // Decode encrypted packet
   
   info = DecodePacket(hdr, &encKey, true);
   REQUIRE((info.code == 2));
   REQUIRE((info.isAuto == false));
   REQUIRE((info.isValid == true));
   REQUIRE((info.size == strlen(strWrite)+1));
   REQUIRE((memcmp(info.data, strWrite, strlen(strWrite)+1) == 0));
   info.cleanup();
   
   // Encrypt AND COMPRESS packet

   hdr = (OVPacketHeader*)stream.mPtr;
   builder.setEncrypted(true);
   builder.setCompressed(true);
   dat = builder.commitToEncodedPacket(&encKey);
   hdr = (OVPacketHeader*)dat->ptr();
   REQUIRE((hdr->isValid() == true));
   REQUIRE((hdr->getNativeId() == 2));
   REQUIRE((hdr->getPacketSize() != strlen(strWrite)+1));
   REQUIRE((hdr->isCompressed() == true));
   REQUIRE((hdr->isEncrypted() == true));
   REQUIRE((hdr->getPacketSize() == dat->size()-6));

   // Decode encrypted packet
   
   info = DecodePacket(hdr, &encKey, true);
   REQUIRE((info.code == 2));
   REQUIRE((info.isAuto == false));
   REQUIRE((info.isValid == true));
   REQUIRE((info.size == strlen(strWrite)+1));
   REQUIRE((memcmp(info.data, strWrite, strlen(strWrite)+1) == 0));
   info.cleanup();
}


TEST_CASE("EncodePacket with PacketBuilder using RPC")
{
   MemStream stream(4096);
   PacketBuilder builder(&stream);
   builder.beginRPC(2, 3, 4);

   uint8_t secKey[20];
   memset(secKey, '\0', sizeof(secKey));
   
   const char* strWrite = "TEST STRING 2";
   stream.write(strlen(strWrite)+1, strWrite);

   BLOWFISH_CONTEXT encKey;
   BLOWFISH_Init(&encKey, (uint8_t*)&secKey[0], 20, BLOWFISH_MODE_ECB, 0, 0);
   
   OVFreeableData* dat = NULL;
   OVPacketHeader* hdr = NULL;
   OVPacketInfo info;
   int compressedSize = 0;
   
   // Encrypt AND COMPRESS packet
   hdr = (OVPacketHeader*)stream.mPtr;
   builder.setEncrypted(true);
   builder.setCompressed(true);
   dat = builder.commitToEncodedPacket(&encKey);
   hdr = (OVPacketHeader*)dat->ptr();
   compressedSize = hdr->getNativeSize(); // (HDR) + RPC(6) + HDR(6) + DATA
   REQUIRE((hdr->isValid() == true));
   REQUIRE((hdr->getNativeId() == 10001));
   REQUIRE((hdr->getPacketSize() == 48));
   REQUIRE((hdr->isCompressed() == false));
   REQUIRE((hdr->isEncrypted() == true));
   REQUIRE((hdr->getPacketSize() == dat->size()-6));

   // Decode encrypted packet
   
   info = DecodePacket(hdr, &encKey, true);
   hdr = (OVPacketHeader*)(dat->ptr()+12);
   REQUIRE((hdr->getNativeId() == 4));
   REQUIRE((hdr->getPacketSize() != strlen(strWrite)+1));
   REQUIRE((hdr->isCompressed() == true));
   REQUIRE((hdr->isEncrypted() == false));
   REQUIRE((info.code == 10001));
   REQUIRE((info.isAuto == true));
   REQUIRE((info.isValid == true));
   REQUIRE((info.size == compressedSize)); // (HDR) + RPC(6) + HDR(6) + DATA
   
   info = DecodePacket(hdr, &encKey, true);
   REQUIRE((info.code == 4));
   REQUIRE((info.isAuto == false));
   REQUIRE((info.isValid == true));
   REQUIRE((info.size == strlen(strWrite)+1));
   REQUIRE((memcmp(info.data, strWrite, strlen(strWrite)+1) == 0));
}

TEST_CASE("EncodePacket with PacketBuilder using RPC and no encryption")
{
   MemStream stream(4096);
   PacketBuilder builder(&stream);
   builder.beginRPC(2, 3, 4);

   uint8_t secKey[20];
   memset(secKey, '\0', sizeof(secKey));
   
   const char* strWrite = "TEST STRING 2";
   stream.write(strlen(strWrite)+1, strWrite);

   BLOWFISH_CONTEXT encKey;
   BLOWFISH_Init(&encKey, (uint8_t*)&secKey[0], 20, BLOWFISH_MODE_ECB, 0, 0);
   
   OVFreeableData* dat = NULL;
   OVPacketHeader* hdr = NULL;
   OVPacketInfo info;
   int compressedSize = 0;
   
   // Encrypt AND COMPRESS packet
   // (total_out = 28, packet size is thus 6+6+28 = 40)
   hdr = (OVPacketHeader*)stream.mPtr;
   builder.setEncrypted(false);
   builder.setCompressed(true);
   dat = builder.commitToEncodedPacket(&encKey);
   hdr = (OVPacketHeader*)dat->ptr();
   compressedSize = hdr->getNativeSize(); // (HDR) + RPC(6) + HDR(6) + DATA
   REQUIRE((hdr->isValid() == true));
   REQUIRE((hdr->getNativeId() == 10001));
   REQUIRE((hdr->getPacketSize() == 40)); //
   REQUIRE((hdr->isCompressed() == false));
   REQUIRE((hdr->isEncrypted() == false));
   REQUIRE((hdr->getPacketSize() == dat->size()-6));

   // Decode encrypted packet
   
   info = DecodePacket(hdr, &encKey, true);
   hdr = (OVPacketHeader*)(dat->ptr()+12);
   REQUIRE((hdr->getNativeId() == 4));
   REQUIRE((hdr->getPacketSize() != strlen(strWrite)+1));
   REQUIRE((hdr->isCompressed() == true));
   REQUIRE((hdr->isEncrypted() == false));
   REQUIRE((info.code == 10001));
   REQUIRE((info.isAuto == true));
   REQUIRE((info.isValid == true));
   REQUIRE((info.size == compressedSize)); // (HDR) + RPC(6) + HDR(6) + DATA
   
   info = DecodePacket(hdr, &encKey, true);
   REQUIRE((info.code == 4));
   REQUIRE((info.isAuto == false));
   REQUIRE((info.isValid == true));
   REQUIRE((info.size == strlen(strWrite)+1));
   REQUIRE((memcmp(info.data, strWrite, strlen(strWrite)+1) == 0));
}


TEST_CASE("EncodePacket which needs padding for encryption")
{
   MemStream stream(4096);
   PacketBuilder builder(&stream);
   builder.beginBasicPacket(404);

   uint8_t secKey[20];
   memset(secKey, '\0', sizeof(secKey));
   
   const char* strWrite = "TE";
   stream.write(strlen(strWrite)+1, strWrite);

   BLOWFISH_CONTEXT encKey;
   BLOWFISH_Init(&encKey, (uint8_t*)&secKey[0], 20, BLOWFISH_MODE_ECB, 0, 0);
   
   OVFreeableData* dat = NULL;
   OVPacketHeader* hdr = NULL;
   OVPacketInfo info;
   int compressedSize = 0;
   
   // Encrypt packet
   hdr = (OVPacketHeader*)stream.mPtr;
   builder.setEncrypted(true);
   dat = builder.commitToEncodedPacket(&encKey);
   hdr = (OVPacketHeader*)dat->ptr();
   compressedSize = hdr->getNativeSize();
   REQUIRE((hdr->isValid() == true));
   REQUIRE((hdr->getNativeId() == 404));
   REQUIRE((hdr->getPacketSize() == 8));
   REQUIRE((hdr->isEncrypted() == true));
   REQUIRE((hdr->getPacketSize() == dat->size()-6));

   // Decode encrypted packet
   info = DecodePacket(hdr, &encKey, true);
   hdr = (OVPacketHeader*)(dat->ptr());
   REQUIRE((hdr->getNativeId() == 404));
   REQUIRE((hdr->getNativeSize() == strlen(strWrite)+1));
   REQUIRE((hdr->isEncrypted() == true));
   REQUIRE((info.code == 404));
   REQUIRE((info.isAuto == false));
   REQUIRE((info.isValid == true));
   REQUIRE((info.size == strlen(strWrite)+1));
   
   uint8_t* ptr = hdr->getData();
   uint32_t stored_adler = ((uint32_t*)(ptr+3))[0];
   uint32_t expected_adler = OV_BYTESWAP32(25755802);
   REQUIRE(( stored_adler == expected_adler));
   REQUIRE((ptr[7] == 0));
}

TEST_CASE("EncodePacket with padding using PacketBuilder using RPC")
{
   MemStream stream(4096);
   PacketBuilder builder(&stream);
   builder.beginRPC(2, 3, 4);

   uint8_t secKey[20];
   memset(secKey, '\0', sizeof(secKey));
   
   const char* strWrite = "TE";
   stream.write(strlen(strWrite)+1, strWrite);

   BLOWFISH_CONTEXT encKey;
   BLOWFISH_Init(&encKey, (uint8_t*)&secKey[0], 20, BLOWFISH_MODE_ECB, 0, 0);
   
   OVFreeableData* dat = NULL;
   OVPacketHeader* hdr = NULL;
   OVPacketInfo info;
   int compressedSize = 0;
   
   // Encrypt AND COMPRESS packet
   hdr = (OVPacketHeader*)stream.mPtr;
   builder.setEncrypted(true);
   builder.setCompressed(true);
   dat = builder.commitToEncodedPacket(&encKey);
   hdr = (OVPacketHeader*)dat->ptr();
   compressedSize = hdr->getNativeSize(); // (HDR) + RPC(6) + HDR(6) + DATA
   REQUIRE((hdr->isValid() == true));
   REQUIRE((hdr->getNativeId() == 10001));
   REQUIRE((hdr->getPacketSize() == 40));
   REQUIRE((hdr->isCompressed() == false));
   REQUIRE((hdr->isEncrypted() == true));
   REQUIRE((hdr->getPacketSize() == dat->size()-6));

   // Decode encrypted packet
   
   info = DecodePacket(hdr, &encKey, true);
   hdr = (OVPacketHeader*)(dat->ptr()+12);
   REQUIRE((hdr->getNativeId() == 4));
   REQUIRE((hdr->getPacketSize() != strlen(strWrite)+1));
   REQUIRE((hdr->isCompressed() == true));
   REQUIRE((hdr->isEncrypted() == false));
   REQUIRE((info.code == 10001));
   REQUIRE((info.isAuto == true));
   REQUIRE((info.isValid == true));
   REQUIRE((info.size == compressedSize)); // (HDR) + RPC(6) + HDR(6) + DATA
   
   OVPacketHeader* ohdr = (OVPacketHeader*)(dat->ptr());
   uint8_t* ptr = ohdr->getData()+ohdr->getNativeSize();
   uint32_t stored_adler = ((uint32_t*)ptr)[0];
   uint32_t expected_adler = OV_BYTESWAP32(1132791320);
   REQUIRE(stored_adler == expected_adler);
   REQUIRE((ptr[4] == 0));
   
   info = DecodePacket(hdr, &encKey, true);
   REQUIRE((info.code == 4));
   REQUIRE((info.isAuto == false));
   REQUIRE((info.isValid == true));
   REQUIRE((info.size == strlen(strWrite)+1));
   REQUIRE((memcmp(info.data, strWrite, strlen(strWrite)+1) == 0));
}


TEST_CASE("EncodePacket with PacketBuilder using large compressed packet")
{
   srand(183918);
   MemStream stream(4096);
   PacketBuilder builder(&stream);
   builder.beginBasicPacket(2);

   REQUIRE((stream.getPosition() == 6));

   uint8_t secKey[20];
   memset(secKey, '\0', sizeof(secKey));
   
   uint8_t sdat[30*1000];
   for (int i=0; i<sizeof(sdat); i++)
   {
      sdat[i] = (uint8_t)(rand()%256);
   }
   stream.write(sizeof(sdat), &sdat[0]);

   BLOWFISH_CONTEXT encKey;
   BLOWFISH_Init(&encKey, (uint8_t*)&secKey[0], 20, BLOWFISH_MODE_ECB, 0, 0);
   
   OVFreeableData* dat = builder.commitToEncodedPacket(&encKey);
   REQUIRE((dat != NULL));
   if (!dat)
      return;

   OVPacketHeader* hdr = (OVPacketHeader*)dat->ptr();
   REQUIRE((hdr->isValid() == true));
   REQUIRE((hdr->getNativeId() == 2));
   REQUIRE((hdr->getPacketSize() != sizeof(sdat)));

   // Decode compressed packet
   OVPacketInfo info = DecodePacket(hdr, &encKey, true);
   REQUIRE((info.code == 2));
   REQUIRE((info.isAuto == false));
   REQUIRE((info.isValid == true));
   REQUIRE((info.size == sizeof(sdat)));
   REQUIRE((memcmp(info.data, &sdat[0], sizeof(sdat)) == 0));
   info.cleanup();
}
