/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef statsController_hpp
#define statsController_hpp

#include <vector>
#include "protocolHandler.hpp"

class StatsController
{
public:
   
   static void logUserLogin();
   static void logUserLogout();
   static void logConnect();
   static void logDisconnect();
   static void logRPCTicket(const RPCTicketPtr &ticket);
   static void logDBQuery(uint64_t completionTime);
   
   static void dump();
};

class TestTimerController
{
public:
   
   class Timer : public std::enable_shared_from_this<Timer>
   {
   public:
      OVRuntimeTimerType mIdleTimer;
      
      Timer(asio::io_context &ctx) : mIdleTimer(ctx) {;}
   };
   
   typedef std::shared_ptr<Timer> TimerPtr;
   std::vector<TimerPtr> mTimers;
   asio::io_context* mContext;
   
   TestTimerController(asio::io_context &ctx)
   {
      mContext = &ctx;
   }
   
   template<typename T> void createTimer(uint64_t limit, T func)
   {
      TimerPtr t = std::make_shared<Timer>(*mContext);
      auto ms =  std::chrono::milliseconds(limit);
      std::chrono::time_point<OVRuntimeTimerType::clock_type> tp(ms);
      t->mIdleTimer.expires_at(tp);
      t->mIdleTimer.async_wait([t, func](const std::error_code &ec){
         func(ec);
         instance()->killTimer(t);
      });
      
      mTimers.push_back(t);
   }
   
   void killTimer(TimerPtr t)
   {
      auto itr = std::find(mTimers.begin(), mTimers.end(), t);
      
      if (itr != mTimers.end())
      {
         mTimers.erase(itr);
      }
   }
   
   static void init(asio::io_service &svc);
   static TestTimerController* instance();
};


#endif /* statsController_hpp */
