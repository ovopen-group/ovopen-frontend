/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef RPCQueue_hpp
#define RPCQueue_hpp

#include <stdio.h>
#include <stdint.h>

#include <asio.hpp>
#include <atomic>
#include <iostream>
#include <unordered_map>
#include "coreTypes.hpp"

class RPCPacketHandler;

struct RPCTicketInfo
{
   uint32_t mRequestID;
   uint32_t mFailResponseCode;
   RPCPacketHandler* mRPCHandler;
   uint16_t mFailPacketID;
   
   uint64_t mUserVar1; // usually as part of cancel functionality
   int64_t mUserVar2; // usually as part of cancel functionality
   int64_t mUserVar3; // usually as part of cancel functionality
   
   RPCTicketInfo() : mRequestID(0), mFailResponseCode(-2), mRPCHandler(0), mFailPacketID(0), mUserVar1(0), mUserVar2(0), mUserVar3(0) {;}
};

class RPCTicket : public std::enable_shared_from_this<RPCTicket>
{
public:
   
   RPCTicketInfo mInfo;
   std::chrono::milliseconds mStartTime;
   
   bool mCancelled;
   bool mCompleted;
   bool mErrorFlagged;
   
   RPCTicket() : mCancelled(false), mCompleted(false), mErrorFlagged(false)
   {
   }
   
   RPCTicket(const RPCTicketInfo &info) : mInfo(info), mCancelled(false), mCompleted(false)
   {
      auto now = OVRuntimeTimerType::clock_type::now().time_since_epoch();
      mStartTime = std::chrono::duration_cast<std::chrono::milliseconds>(now);
   }
   
   inline uint32_t getRequestID() const { return mInfo.mRequestID; }
   static inline bool isServerTicketID(uint32_t rid) { return (rid & 0x0000FFFF) != 0; }
};

typedef std::shared_ptr<RPCTicket> RPCTicketPtr;

#endif /* RPCQueue_hpp */
