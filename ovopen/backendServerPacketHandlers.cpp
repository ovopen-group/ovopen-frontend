/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "packetHandlers.hpp"
#include "packet.hpp"
#include "protocolHandler.hpp"
#include "friendController.hpp"
#include "adminQueue.hpp"
#include "groupChat.hpp"
#include <unordered_map>
#include "userConnectionController.hpp"
#include "backendConnectionController.hpp"
#include "statsController.hpp"
#include "LUrlParser.hpp"
#include "log.hpp"
#include "coreUtil.hpp"

// LOCATION SERVER PACKETS
// Raw packets related to this:

/*
 
 LOCATION_UPDATE_INVENTORY (version, add/remove/replace, category, item_list)
 
 Lazily updates the inventory. This may be sent by the server to the location server, or in reverse.
 Usually the location server will cache the intentory.
 
 RECORD_TRANSACTION (txid, desc)
 
 Records a transaction in the main db
 
 LOCATION_USER_ENTER (uid)
 LOCATION_USER_LEAVE (uid)
 
 Lets master server know who logs in and out.
 
 */



struct HouseInfo
{
   uint32_t deedID;
   uint32_t plotID;
   uint32_t ownerID;
   uint32_t typeID;
   uint32_t maxFurniture;
   uint32_t cc;
   uint32_t pp;
   std::string name;
   std::string data;
   
   void writeHouseInfoPacket(MemStream &packetStream)
   {
      packetStream.writeUintBE(deedID);
      packetStream.writeUintBE(plotID);
      packetStream.writeUintBE(ownerID);
      packetStream.writeUintBE(typeID);
      packetStream.writeUintBE(maxFurniture);
      packetStream.writeUintBE(cc);
      packetStream.writeUintBE(pp);
      packetStream.writeString(name.c_str());
      packetStream.write(data.size(), data.c_str());
   }
};

class RPC_RegisterLocationServer : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9001, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0,
      RESPONSE_ERR=-2
   };
   
   static void sendHousingInfo(OVProtocolHandler& client)
   {
      OVBackendGameConnectionState* backendState = dynamic_cast<OVBackendGameConnectionState*>(client.mState);
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT id, owner_id, local_id, type_id, name, poi_name, cc, pp, max_furniture, data FROM house_plots "
      "WHERE location_instance_id = $1 ORDER BY local_id ASC;", (int64_t)backendState->mInternalID), false, [clientHandler](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
         
         MemStream outStream(4096);
         
         HouseInfo houseInfo;
         for (auto res : results)
         {
            auto row = res.value;
            houseInfo.deedID = DBToU32(row[0]);
            houseInfo.plotID = DBToU32(row[2]);
            houseInfo.ownerID = DBToU32(row[1]);
            houseInfo.typeID = DBToU32(row[3]);
            houseInfo.maxFurniture = DBToU32(row[8]);
            houseInfo.cc = DBToU32(row[6]);
            houseInfo.pp = DBToU32(row[7]);
            houseInfo.name = row[4];
            houseInfo.data = row[9];
            
            PacketBuilder packet(&outStream);
            packet.beginAutoPacket(AutoPacketBackendHousePlot);
            houseInfo.writeHouseInfoPacket(outStream);

            OVFreeableData* responseData = packet.commitToEncodedPacket(&clientHandler->mEncryptionKey);
            if (responseData)
               clientHandler->queuePacketData(responseData);
         }
      });
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      if (info.code != 1)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      // First find our record
      
      uint32_t dbID = 0;
      uint16_t portID = 0;
      stream.readUintBE(dbID);
      stream.readUintBE(portID);
      
      char buf2[1024];
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT locations.name, location_instances.name, location_id, instance_id "
      "FROM location_instances "
      "INNER JOIN locations ON locations.id = location_instances.location_id "
      "WHERE location_instances.id = $1 AND owner_id = $2 LIMIT 1;", (int64_t)dbID, (int64_t)client.mUid), false, [this, clientHandler, requestTicket, dbID, portID](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
         if (ec || results.size() == 0)
         {
            endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_ERR);
         }
         else
         {
            OVBackendGameConnectionState* state = dynamic_cast<OVBackendGameConnectionState*>(clientHandler->mState);
            auto row = results[0].value;
            
            int locID = DBToS32(row[2]);
            int instID = DBToS32(row[3]);
            
            if (portID > 0)
            {
               // Set details
               state->mInternalID = dbID;
               state->mLocationName = row[0];
               state->mInstanceName = row[1];
               state->mAcceptPermissions = 0;
               state->mLocationID = locID;
               state->mInstanceID = instID;
               state->mNumHomes = 0;
               state->mLastAcceptState = 0;
               state->mReady = true;
               
               auto bytes = clientHandler->mSocket.remote_endpoint().address().to_v4().to_bytes();
               memcpy(&state->mLocationAddress[0], bytes.data(), 4);
               state->mLocationPort = portID;
               
               BackendConnectionController::registerLocation(clientHandler);
            }
            
            std::string serverURL = "ONVERSE:/";
            
            serverURL += "/" + std::string(row[0]) + "/" + std::string(row[1]);
            
            MemStream packetStream(4096);
            PacketBuilder packet(&packetStream);
            packet.beginAutoPacket(AutoPacketBackendServerInfo);
            
            packetStream.writeString(serverURL.c_str());
            
            packetStream.writeIntBE(locID);
            packetStream.writeIntBE(instID);
            
            packet.commit();
            
            PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
            clientHandler->queuePacketData(responseData);
            
            sendHousingInfo(*(clientHandler.get()));
            
            endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_OK);
         }
         
      });
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_RegisterLocationServer);

class RPC_SetLocationReadyState : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9002, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0,
      RESPONSE_ERR = -2,
   };
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      if (info.code != 1)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      // Check permissions
      OVBackendGameConnectionState* backendState = dynamic_cast<OVBackendGameConnectionState*>(client.mState);
      if (!backendState->mReady)
      {
         endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      uint32_t permID = 0;
      uint8_t vipOnly = false;
      stream.readUintBE(permID);
      stream.read(vipOnly);
      
      OVBackendGameConnectionState* state = dynamic_cast<OVBackendGameConnectionState*>(client.mState);
      if (state && state->mInternalID >= 0)
      {
         std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
         
         state->mAcceptPermissions = permID;
         state->mVIPOnly = vipOnly;
         BackendConnectionController::markLocationReady(clientHandler);
         
         endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_OK);
      }
      else
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_OK);
      }
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_SetLocationReadyState);

class RPC_RequestInventory : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9003, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0,
      RESPONSE_ERR = -2
   };
   
   enum
   {
      Request_InventoryInfo=1,
      Request_InventoryID=2
   };
   
   static void doSendInventoryID(OVProtocolHandler& clientHandler, uint32_t uid, uint32_t inv_id)
   {
      uint32_t packet_len = 6 + 8;
      
      MemStream packetStream(packet_len);
      PacketBuilder packet(&packetStream);
      packet.beginAutoPacket(AutoPacketUserInventoryId);
      
      packetStream.writeUintBE(uid);
      packetStream.writeUintBE(inv_id);
      
      packet.commit();
      
      PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
      clientHandler.queuePacketData(responseData);
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      /*
       
       Sent from location server -> master
       */
      MemStream stream(info.size, info.data);
      
      if (info.code != Request_InventoryInfo && info.code != Request_InventoryID)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      // Handle inventory info
      if (info.code == Request_InventoryID)
      {
         uint32_t uid;
         stream.readUintBE(uid);
         
         std::shared_ptr<OVProtocolHandler> uidConnection = UserConnectionController::resolveUIDConnection(uid);
         if (uidConnection)
         {
            doSendInventoryID(client, uid, uidConnection->mLastInventoryID);
            endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_OK);
         }
         else
         {
            endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         }
         
         return;
      }
      
      // Check permissions
      OVBackendGameConnectionState* backendState = dynamic_cast<OVBackendGameConnectionState*>(client.mState);
      if (!backendState->mReady)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      uint32_t uid = 0;
      stream.readUintBE(uid);
      
      uint32_t inv_id = 0;
      stream.readUintBE(inv_id);
      
      char buf2[1024];
      const int numSize = 10;
      
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT item_id, datablock_id "
      "FROM inventory_items "
      "WHERE user_id = $1 AND type_id = $2 ORDER BY item_id ASC;", (int64_t)uid, (int64_t)inv_id), false, [this, clientHandler, requestTicket, uid, inv_id, numSize](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
         
         int32_t numInv = results.size();
         const int32_t itemsRequired = numInv * (numSize + numSize + 3);
         
         MemStream packetStream(itemsRequired + 64);
         PacketBuilder packet(&packetStream);
         packet.beginAutoPacket(AutoPacketBackendInventoryData); // 502
         
         packetStream.writeUintBE(uid);
         packetStream.writeUintBE(inv_id);
         
         // Write items into packet
         for (auto &row : results)
         {
            char buf[256];
            snprintf(buf, 256, "%u\t%u\n", DBToU32(row.value[0]), DBToU32(row.value[1]));
            packetStream.write(strlen(buf), buf);
         }
         
         if (numInv > 0) packetStream.setPosition(packetStream.getPosition()-1); // chop off last \n

         OVFreeableData* responseData = packet.commitToEncodedPacket(&clientHandler->mEncryptionKey);
         if (responseData)
            clientHandler->queuePacketData(responseData);
         
         endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, responseData ? RESPONSE_OK : RESPONSE_ERR);
      });
      //
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_RequestInventory);

class RPC_NotifyTransaction : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9004, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0,
      RESPONSE_ERR = -2
   };
   
   enum
   {
      Request_TransactionInfo=1,
      Response_TransactionID=2,
      Request_QueryBalance=3
   };
   
   void doNotifyTransaction(OVProtocolHandler& clientHandler, RPCTicketPtr requestTicket, uint64_t txid)
   {
      uint32_t packet_len = 6 + 6 + (6 + 8);
   
      MemStream packetStream(packet_len);
      PacketBuilder packet(&packetStream);
      packet.beginRPC(getRPCID(), requestTicket->getRequestID(), Response_TransactionID, false);
   
      packetStream.writeUintBE(txid);
   
      packet.commit();
   
      PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);

      clientHandler.queuePacketData(responseData);
   }
   
   void doSendBalance(OVProtocolHandler& clientHandler, uint32_t uid, int32_t cc, int32_t pp)
   {
      uint32_t packet_len = 6 + 12;
      
      MemStream packetStream(packet_len);
      PacketBuilder packet(&packetStream);
      packet.beginAutoPacket(AutoPacketUserBalance);
      
      packetStream.writeUintBE(uid);
      packetStream.writeIntBE(cc);
      packetStream.writeIntBE(pp);
      
      packet.commit();
      
      PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
      
      clientHandler.queuePacketData(responseData);
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      if (info.code != Request_TransactionInfo && info.code != Request_QueryBalance)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      // Check permissions
      OVBackendGameConnectionState* backendState = dynamic_cast<OVBackendGameConnectionState*>(client.mState);
      if (!backendState->mReady)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      uint32_t uid = 0;
      stream.readUintBE(uid);
      
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      // Handle balance packet
      if (info.code == Request_QueryBalance)
      {
         DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT cc, pp FROM users WHERE id = $1 LIMIT 1;", (int64_t)uid), false, [this, clientHandler, requestTicket, uid](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
            
            if (ec || results.size() == 0)
            {
               endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_ERR);
            }
            else
            {
               auto row = results[0].value;
               int cc = DBToS32(row[0]);
               int pp = DBToS32(row[1]);
               doSendBalance(*(clientHandler.get()), uid, cc, pp);
               endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_OK);
            }
         });
         
         return;
      }
      
      uint32_t item_id = 0;
      stream.readUintBE(item_id);
      
      uint32_t action_id = 0;
      stream.readUintBE(action_id);
      
      int32_t cc_diff = 0;
      stream.readIntBE(cc_diff);
      
      int32_t pp_diff = 0;
      stream.readIntBE(pp_diff);
      
      uint32_t obj_server_id = 0;
      stream.readUintBE(obj_server_id);

      DBQueryQueue::getQueue().addToQueueMultipleRows(clientHandler->getStrandRef(), SQLBindPack(
      "BEGIN TRANSACTION;\n"
      "INSERT INTO transactions(item_id, user_id, action_id, created_at, cc, pp, obj_server_id) "
      "VALUES ($1, $2, $3, NOW(), $4, $5, $6);\n"
      "UPDATE users SET cc = cc + $7, pp = pp + $8 WHERE id = $9 LIMIT 1;\n"
      "SELECT cc, pp FROM users WHERE id = $9 LIMIT 1;\n"
      "COMMIT;\n",
      (int64_t)item_id, (int64_t)uid, (int64_t)action_id, (int64_t)cc_diff, (int64_t)pp_diff, (int64_t)obj_server_id,
      (int64_t)cc_diff, (int64_t)pp_diff, (int64_t)uid, (int64_t)uid), true, [this, clientHandler, requestTicket, uid](OVDBConnection* async, const std::error_code &ec, uint32_t resultSet, std::vector<DBResult> &results){
         
         // Handle error case
         if (ec)
         {
            if (!requestTicket->mErrorFlagged)
            {
               endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_ERR);
               requestTicket->mErrorFlagged = true;
            }
         }
         else if (resultSet == 1) // INSERT
         {
            requestTicket->mInfo.mUserVar1 = async->insert_id();
         }
         else if (resultSet == 3) // SELECT user
         {
            if (results.size() > 0)
            {
               auto row = results[0].value;
               requestTicket->mInfo.mUserVar2 = std::strtoll(row[0], NULL, 10);
               requestTicket->mInfo.mUserVar3 = std::strtoll(row[1], NULL, 10);
            }
            else
            {
               requestTicket->mInfo.mUserVar2 = 0;
               requestTicket->mInfo.mUserVar3 = 0;
            }
         }
         else if (resultSet == 4) // COMMIT
         {
            // Send user balance
            doSendBalance(*(clientHandler.get()), uid, requestTicket->mInfo.mUserVar2, requestTicket->mInfo.mUserVar3);
            
            // Send txid
            doNotifyTransaction(*(clientHandler.get()), requestTicket, requestTicket->mInfo.mUserVar1);
            
            // End request
            endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_OK);
         }
      });
      
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_NotifyTransaction);

class RPC_CommitInventory : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9005, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0,
      RESPONSE_ERR=-2
   };
   
   virtual void addItems(OVProtocolHandler& client, RPCTicketPtr requestTicket, uint32_t uid, int32_t type_id, const char* list, bool replace)
   {
      StringUtil::StringSplitter split;
      split.scan(list, "\t\n");

      MemStream sBuffer((strlen(list) * 2) + 256);
      uint32_t scan[2] = {0,0};
      uint32_t idx = 0;
      bool emit=false;
      
      sBuffer.writeRawString("BEGIN TRANSACTION;\n");
      
      if (replace)
      {
         char replaceBuf[128];
         snprintf(replaceBuf, sizeof(replaceBuf), "DELETE FROM inventory_items WHERE user_id = %u AND type_id = %u;\n", uid, type_id);
         sBuffer.write(strlen(replaceBuf), replaceBuf);
      }
      
      uint32_t oldPos = sBuffer.mPos;
      uint32_t itemsInList = 0;
      sBuffer.writeRawString("INSERT INTO inventory_items(type_id,item_id,datablock_id,user_id) VALUES ");
      
      uint32_t numUnits = split.getUnitCount();
      for (uint32_t i=0; i<numUnits; i++)
      {
         char buf[128];
         char unit[256];

         char term = split.getUnit(i, &unit[0], sizeof(unit));
         if (idx < 2)
         {
            scan[idx] = std::atoi(unit);
            if (idx == 1 && scan[idx] == 0)
            {
               printf("WTF\n");
            }
         }
         
         if ((term == '\n' || (i == (numUnits-1))) && scan[0] > 0)
         {
            idx = 0;
            snprintf(buf, 128, "%s(%i,%i,%i,%u)", emit ? "," : "", type_id, scan[0], scan[1], uid);
            sBuffer.write(strlen(buf), buf);
            emit = true;
            scan[0] = 0;
            itemsInList++;
         }
         else
         {
            idx++;
         }
      }
      
      if (itemsInList > 0)
      {
         sBuffer.writeRawString(";\nCOMMIT;\n");
      }
      else
      {
         sBuffer.mPos = oldPos;
         sBuffer.writeRawString("\nCOMMIT;\n");
      }
      
      sBuffer.write(1, "");
      
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      char* querySTR = (char*)sBuffer.mPtr;
      sBuffer.mOwnPtr = false;
      
      DBQueryQueue::getQueue().addToQueueMultipleRows(clientHandler->getStrandRef(), SQLBindPack(querySTR), true, [this, clientHandler, requestTicket, querySTR](OVDBConnection* async, const std::error_code &ec, uint32_t resultSet, std::vector<DBResult> &results){
         
         if (!async->more_results())
         {
            endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_OK);
         }
         
      });
   }
   
   virtual void removeItems(OVProtocolHandler& client, RPCTicketPtr requestTicket, uint32_t uid, int32_t type_id, const char* list)
   {
      StringUtil::StringSplitter split;
      split.scan(list, "\t\n");
      
      MemStream sBuffer(strlen(list) * 2);
      uint32_t scan[2] = {0,0};
      uint32_t idx = 0;
      bool emit=false;
      uint32_t itemsInList = 0;
      
      sBuffer.writeRawString("DELETE FROM inventory_items WHERE user_id = %u AND item_id IN (");
      
      uint32_t numUnits = split.getUnitCount();
      for (uint32_t i=0; i<numUnits; i++)
      {
         char buf[128];
         char unit[256];

         char term = split.getUnit(i, &unit[0], sizeof(unit));
         if (idx < 2)
         {
            scan[idx] = std::atoi(unit);
         }
         
         if ((term == '\n' || (i == (numUnits-1))) && scan[0] > 0)
         {
            idx = 0;
            snprintf(buf, 128, "%s%u", emit ? "," : "", scan[0]);
            sBuffer.write(strlen(buf), buf);
            emit = true;
            scan[0] = 0;
            itemsInList++;
         }
         else
         {
            idx++;
         }
      }
      
      sBuffer.write(3, ");");
      
      // Shortcut: no items, do nothing.
      if (itemsInList == 0)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_OK);
         return;
      }
      
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      // TOFIX: better alloc for querySTR
      char* querySTR = (char*)sBuffer.mPtr;
      sBuffer.mOwnPtr = false;
      
      DBQueryQueue::getQueue().addToQueueAffected(clientHandler->getStrandRef(), SQLBindPack(querySTR), [this, clientHandler, requestTicket, querySTR](OVDBConnection* async, const std::error_code &ec, uint64_t affRows){
         
         {
            free(querySTR);
            endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_OK);
         }
         
      });
   }
   
   virtual void updateLastItemID(OVProtocolHandler& client, RPCTicketPtr requestTicket, uint32_t uid, uint32_t item_id)
   {
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      clientHandler->mLastInventoryID = item_id;
      
      DBQueryQueue::getQueue().addToQueueAffected(clientHandler->getStrandRef(), SQLBindPack("UPDATE users SET last_item_id = $1 WHERE id = $2", (int64_t)item_id, (int64_t)uid), [this, clientHandler, requestTicket](OVDBConnection* async, const std::error_code &ec, uint64_t affRows){
         endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_OK);
      });
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      if (info.code == 0 || info.code > 4)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      // Check permissions
      OVBackendGameConnectionState* backendState = dynamic_cast<OVBackendGameConnectionState*>(client.mState);
      if (!backendState->mReady)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      uint32_t uid = 0;
      stream.readUintBE(uid);
      
      uint32_t inv_id = -1;
      stream.readUintBE(inv_id);
      
      int32_t action_id=info.code;
      
      std::string invBuf;
      std::string list;
      char* data = NULL;
      
      if (info.size > 8)
      {
         data = new char[info.size];
         uint32_t string_size = info.size - stream.getPosition();
         stream.read(string_size, &data[0]);
         data[string_size] = '\0';
      }
      
      switch (action_id)
      {
         case 1:
            addItems(client, requestTicket, uid, inv_id, data ? data : "", true);
            break;
         case 2:
            addItems(client, requestTicket, uid, inv_id, data ? data : "", false);
            break;
         case 3:
            removeItems(client, requestTicket, uid, inv_id, data ? data : "");
            break;
         case 4:
            updateLastItemID(client, requestTicket, uid, inv_id);
            break;
      }
      
      if (data)
      {
         delete[] data;
      }
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_CommitInventory);

class RPC_ExpendUserLoginQueue : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9006, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK = 0,
      RESPONSE_ERR = -2
   };
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      uint32_t accept_state = 0;
      
      if (info.code != 1)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      // Check permissions
      OVBackendGameConnectionState* backendState = dynamic_cast<OVBackendGameConnectionState*>(client.mState);
      if (!backendState->mReady)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      stream.readUintBE(accept_state);
      BackendConnectionController::expendReadyQueue(client.shared_from_this(), accept_state);
      endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_OK);
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_ExpendUserLoginQueue);

class RPC_NotifyUserPresence : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9007, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum PresenceState
   {
      Presence_NotPresent=0,
      Presence_Present=1,
   };
   
   enum
   {
      RESPONSE_OK = 0,
      RESPONSE_ERR = -2
   };
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      uint32_t uid = 0;
      uint8_t presence_state = 0;
      
      if (info.code != 1)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      // Check permissions
      OVBackendGameConnectionState* backendState = dynamic_cast<OVBackendGameConnectionState*>(client.mState);
      if (!backendState->mReady)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      stream.readUintBE(uid);
      stream.read(presence_state);
      
      //
      std::shared_ptr<OVProtocolHandler> userClient = UserConnectionController::resolveUIDConnection(uid);
      if (userClient)
      {
         std::shared_ptr<OVProtocolHandler> backendClient = client.shared_from_this();
         OVServerGameConnectionState* clientState = dynamic_cast<OVServerGameConnectionState*>(userClient->mState);
         if (clientState && backendState)
         {
            userClient->getStrand().post([this, requestTicket, presence_state, clientState, backendState, userClient, backendClient, uid]{
               int64_t internalID = -1;
               
               if (presence_state == Presence_Present)
               {
                  // Kick client from previous location
                  if (clientState->mLastInstanceID != -1 &&
                      backendState->mLocationID != clientState->mLastLocationID &&
                      backendState->mInstanceID != clientState->mLastInstanceID )
                  {
                     std::shared_ptr<OVProtocolHandler> prevBackend = BackendConnectionController::getInstance(clientState->mLastInstanceID, clientState->mLastLocationID);
                     if (prevBackend)
                     {
                        OVBackendGameConnectionState* prevBackendState = dynamic_cast<OVBackendGameConnectionState*>(prevBackend->mState);
                        
                        MemStream packetStream(6 + 4);
                        PacketBuilder packet(&packetStream);
                        packet.beginAutoPacket(AutoPacketBackendUserShouldBeKicked);
                        
                        packetStream.writeUintBE(uid);
                        
                        packet.commit();
                        
                        PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
                        prevBackend->queuePacketData(responseData);
                        prevBackend->mStrand.post([prevBackendState, uid]{
                           prevBackendState->eraseUID(uid);
                        });
                     }
                  }
                  
                  backendClient->mStrand.post([backendState, uid]{
                     backendState->insertUID(uid);
                  });
                  
                  clientState->mLastLocationID = backendState->mLocationID;
                  clientState->mLastInstanceID = backendState->mInstanceID;
                  internalID = backendState->mInstanceID;
               }
               else if (clientState->mLastLocationID == backendState->mLocationID &&
                        clientState->mLastInstanceID == backendState->mInstanceID)
               {
                  // Left location entirely
                  clientState->mLastLocationID = -1;
                  clientState->mLastInstanceID = -1;
                  
                  backendClient->mStrand.post([backendState, uid]{
                     backendState->eraseUID(uid);
                  });
               }
               else
               {
                  endRPCResponseWithTicket(*(backendClient.get()), requestTicket, 0, RESPONSE_OK);
                  return;
               }
               
               DBQueryQueue::getQueue().addToQueueAffected(backendClient->getStrandRef(), SQLBindPack("UPDATE users SET last_instance_id = $1 WHERE id = $2", (int64_t)backendState->mInternalID, (int64_t)uid), &DBQueryQueue::defaultBasicHandler);
               endRPCResponseWithTicket(*(backendClient.get()), requestTicket, 0, RESPONSE_OK);
            });
         }
      }
      else
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_OK);
      }
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_NotifyUserPresence);

class RPC_CommitHousePlot : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9008, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK=0,
      RESPONSE_ERR= -2,
   };
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      if (info.code != 1)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      // Check permissions
      OVBackendGameConnectionState* backendState = dynamic_cast<OVBackendGameConnectionState*>(client.mState);
      if (!backendState->mReady)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      uint32_t uid = 0;
      stream.readUintBE(uid);
      
      uint32_t plot_id = 0;
      stream.readUintBE(plot_id);
      
      char buf2[128];
      
      OVBackendGameConnectionState* clientState = dynamic_cast<OVBackendGameConnectionState*>(client.mState);
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      SQLPack* query = SQLBindPack(
               "INSERT INTO house_plots (owner_id, instance_id, location_id, local_id, location_instance_id, data) "
               "VALUES ($1, $2, $3, $4, $5, $6) ON CONFLICT (local_id,location_instance_id) DO UPDATE SET owner_id=excluded.owner_id, data=excluded.data;",
               (int64_t)uid, (int64_t)clientState->mInstanceID, (int64_t)clientState->mLocationID, (int64_t)plot_id, (int64_t)clientState->mInternalID, SQLBlobInfo(stream.getPositionPtr(), info.size - stream.getPosition()));
      
      DBQueryQueue::getQueue().addToQueueAffected(clientHandler->getStrandRef(), query, [this, clientHandler, requestTicket](OVDBConnection* a, const std::error_code &ec, uint64_t affected_rows){
         
         if (ec)
         {
            endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_ERR);
         }
         else
         {
            endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_OK);
         }
      });
      
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_CommitHousePlot);


class RPC_TestCommand : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9020, OVProtocolHandler::PROTOCOL_BACKEND);
   
   enum
   {
      RESPONSE_OK=0,
      RESPONSE_ERR= -2,
   };
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      if (info.code != 1)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      // Check permissions
      OVBackendGameConnectionState* backendState = dynamic_cast<OVBackendGameConnectionState*>(client.mState);
      if (!backendState->mReady)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      auto now = OVRuntimeTimerType::clock_type::now().time_since_epoch();
      uint64_t timeSinceEpoch = std::chrono::duration_cast<std::chrono::milliseconds>(now).count();
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      TestTimerController::instance()->createTimer(timeSinceEpoch + 60000, [this, clientHandler, requestTicket](std::error_code ec){
         Log::client_printf(*(clientHandler.get()), LogEntry::Debug, "Test timer done");
         endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_OK);
         Log::client_printf(*(clientHandler.get()), LogEntry::Debug, "Test timer finished handler");
      });
      
      
      
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_TestCommand);

// END BACKEND
