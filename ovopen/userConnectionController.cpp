/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "userConnectionController.hpp"
#include "backendConnectionController.hpp"

std::unordered_map<uint32_t, std::shared_ptr<OVProtocolHandler>> gUserLookup;
std::unordered_map<std::string, std::shared_ptr<OVProtocolHandler>> gUserNameLookup;
std::unordered_map<uint32_t, std::shared_ptr<OVProtocolHandler>> gConnectionIDLookup;

std::mutex gUserLock;

std::mutex & UserConnectionController::userMutex()
{
   return gUserLock;
}

UserConnectionController::UserConnectionIterator UserConnectionController::userStart()
{
   return gUserLookup.begin();
}

UserConnectionController::UserConnectionIterator UserConnectionController::userEnd()
{
   return gUserLookup.end();
}

std::shared_ptr<OVProtocolHandler> UserConnectionController::resolveUsernameConnection(const char* name)
{
   std::lock_guard<std::mutex> lock(gUserLock);
   return gUserNameLookup[name];
}

std::shared_ptr<OVProtocolHandler> UserConnectionController::resolveUIDConnection(uint32_t uid)
{
   std::lock_guard<std::mutex> lock(gUserLock);
   return gUserLookup[uid];
}

bool UserConnectionController::isConnectionSame(uint32_t uid, std::shared_ptr<OVProtocolHandler> connection)
{
   std::lock_guard<std::mutex> lock(gUserLock);
   return gUserLookup[uid] == connection;
}

bool UserConnectionController::clearConnectionIfSame(uint32_t uid, std::shared_ptr<OVProtocolHandler> connection)
{
   std::lock_guard<std::mutex> lock(gUserLock);
   if (gUserLookup[uid] == connection)
   {
      gUserLookup.erase(uid);
      gUserNameLookup.erase(connection->mUsername);
      return true;
   }
   
   return false;
}


void UserConnectionController::handleLogin(uint32_t uid, std::string& username, std::shared_ptr<OVProtocolHandler> connection)
{
   bool isPatcher = (connection->mProtocol & OVProtocolHandler::PROTOCOL_PATCHER) != 0;
   bool isBackend = (connection->mProtocol & OVProtocolHandler::PROTOCOL_BACKEND) != 0;
   if (isPatcher)
      return;
   
   if (isBackend)
   {
      // Backend connections are not users
      BackendConnectionController::registerConnection(connection);
      return;
   }
   
   std::lock_guard<std::mutex> lock(gUserLock);
   std::shared_ptr<OVProtocolHandler> existingConnection = gUserLookup[uid];
   
   gUserLookup[uid] = connection;
   gUserNameLookup[username] = connection;
   
   if (existingConnection)
   {
      // Kick existing login
      existingConnection->kick(0);
   }
   
   gUserLookup[uid] = connection;
   gUserNameLookup[username] = connection;
}
