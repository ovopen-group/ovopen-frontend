/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef userConnectionController_hpp
#define userConnectionController_hpp

#include "protocolHandler.hpp"
#include "dbQueue.hpp"

class UserConnectionController
{
public:
   typedef std::unordered_map<uint32_t, std::shared_ptr<OVProtocolHandler>>::iterator UserConnectionIterator;
   
   static UserConnectionIterator userStart();
   static UserConnectionIterator userEnd();
   static std::mutex & userMutex();
   
   static std::shared_ptr<OVProtocolHandler> resolveUsernameConnection(const char* username);
   static std::shared_ptr<OVProtocolHandler> resolveUIDConnection(uint32_t uid);
   
   static bool isConnectionSame(uint32_t uid, std::shared_ptr<OVProtocolHandler> connection);
   
   static bool clearConnectionIfSame(uint32_t uid, std::shared_ptr<OVProtocolHandler> connection);
   
   static void handleLogin(uint32_t uid, std::string& username, std::shared_ptr<OVProtocolHandler> connection);
   
   /// Resolve Username
   /// NOTE: resultHandler will be executed in default queue
   template<typename T> static void resolveUsername(uint32_t uid, T func)
   {
      std::shared_ptr<OVProtocolHandler> existing_connection = resolveUIDConnection(uid);
      if (existing_connection)
      {
         T(existing_connection->mUsername);
      }
      else if (!existing_connection)
      {
         // Check database
         DBQueryQueue::getQueue().addToQueueRows(DBQueryQueue::getStrandRef(), SQLBindPack("SELECT name FROM users WHERE id = $1 LIMIT 1", (int64_t)uid), false,
            [func](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult>& rows) {
            
               if (ec)
               {
                  func("");
               }
               else if (rows.size() > 0)
               {
                  func(rows[0].value[0]);
               }
               
            }
         );
      }
   }
   
   /// Resolve UID and Name
   /// NOTE: resultHandler will be executed in default queue
   template<typename T> static void resolveUIDAndName(const char* username, T func)
   {
      std::shared_ptr<OVProtocolHandler> existing_connection = resolveUsernameConnection(username);
      if (existing_connection)
      {
         // username never changes so should be safe enough
         func(existing_connection->mUid, existing_connection->mUsername.c_str());
      }
      else
      {
         // Check database
         DBQueryQueue::getQueue().addToQueueRows(DBQueryQueue::getStrandRef(), SQLBindPack("SELECT id, username FROM users WHERE username = $1 LIMIT 1", username), false,
            [func](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &rows) {
            
               if (ec)
               {
                  func(0, "");
               }
               else if (rows.size() > 0)
               {
                  func(std::atoi(rows[0].value[0]), rows[0].value[1]);
               }
               
            }
         );
      }
   }
   
   template<typename T> static void eachLoggedInConnection(T func)
   {
      std::lock_guard<std::mutex> lock(userMutex());
      for (auto itr = userStart(), itrEnd = userEnd(); itr != itrEnd; itr++)
      {
		 std::shared_ptr<OVProtocolHandler> handlerPtr = itr->second;
         func(handlerPtr.get());
      }
   }
};

#endif /* userConnectionController_hpp */
