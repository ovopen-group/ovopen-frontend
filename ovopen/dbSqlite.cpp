#include <vector>
#include <thread>
#include <system_error>
#include <time.h>
#include <limits.h>
#include "dbQueue.hpp"
#include "sqlite3.h"
#include <thread>
#include <functional>

class OVDBConnectionSqlite : public OVDBConnection
{
public:

	sqlite3* mDB;
	DBQueryQueue::ConfigInfo mCfg;
	int mMode;
   int mUseState;

	std::thread* mThread;
   sqlite3_stmt *mCurrentStatement;
   std::vector<uintptr_t*> mCurrentRowBlobs;
   
   uint64_t mLastID;
   uint64_t mChangedRows;
   
   std::recursive_mutex mMutex;

	OVDBConnectionSqlite() : mDB(NULL), mMode(0), mUseState(0), mThread(NULL), mCurrentStatement(NULL), mLastID(0), mChangedRows(0) {

	}
   
   ~OVDBConnectionSqlite()
   {
      //disconnect();
   }

	virtual bool connect()
	{
      std::lock_guard<std::recursive_mutex> lock(mMutex);
      if (mDB != NULL)
         return true;
      
      int res = 0;
		if ((res = sqlite3_open_v2(mCfg.mDatabase.c_str(), &mDB, mMode, NULL)) != SQLITE_OK)
		{
         if (mDB) sqlite3_close_v2(mDB);
         mDB = NULL;
			return false;
		}
      
      Log::printf("Sqlite: connected %p", this);
		return true;
	}

	virtual void disconnect()
	{
      std::lock_guard<std::recursive_mutex> lock(mMutex);
		if (mDB == NULL)
			return;

      finishQuery();
		sqlite3_close_v2(mDB);
		mDB = NULL;
      
      Log::printf("Sqlite: disconnected %p %i", this, rand());
	}
	

	virtual uint64_t insert_id()
	{
      return mLastID;
	}
	
	virtual uint64_t affected_rows()
	{
      return mChangedRows;
	}
   
   virtual bool more_results()
   {
      return false;
   }

	virtual bool isFreeForQuery()
	{
      return mUseState == 0;
	}
   
   void doBindPack(sqlite3_stmt* stmt, SQLPack* pack)
   {
      for (int i=1; i<SQLPack::MAX_ARGS; i++)
      {
         if (pack->packs[i].code == SQLPackInfo::BIND_DONE)
            break;
         
         switch (pack->packs[i].code)
         {
            case SQLPackInfo::BIND_INT:
            {
               int64_t val = *((int64_t*)(pack->data() + pack->packs[i].ptr));
               sqlite3_bind_int64(stmt, i, val);
            }
            break;
            case SQLPackInfo::BIND_UINT:
            {
               int64_t val = *((uint32_t*)(pack->data() + pack->packs[i].ptr));
               sqlite3_bind_int64(stmt, i, val);
            }
            break;
            case SQLPackInfo::BIND_STR:
            {
               const char* val = (const char*)(pack->data() + pack->packs[i].ptr);
               sqlite3_bind_text(stmt, i, val, -1, NULL);
            }
               break;
            case SQLPackInfo::BIND_BLOB:
            {
               sqlite3_bind_blob (stmt, i, pack->data() + pack->packs[i].ptr + 4, *((uint32_t*)(pack->data() + pack->packs[i].ptr)), SQLITE_STATIC);
            }
            break;
            default:
               break;
         }
         
      }
   }

	virtual std::error_code doBasicQuery(std::shared_ptr<SQLPackQueueRef> query)
	{
      std::lock_guard<std::recursive_mutex> lock(mMutex);
      finishQuery();
      
      sqlite3_busy_timeout(mDB, 1);
      
      // SQLITE_ABORT
      int code = SQLITE_BUSY;
      uint32_t retryCount = 0;
      while (code == SQLITE_BUSY)
      {
         code = sqlite3_prepare_v2(mDB, query->mPack->query(), -1, &mCurrentStatement, NULL);
         retryCount++;
         if (retryCount >= 3)
         {
            Log::warnf("Sqlite query prepare timed out");
            finishQuery();
            return std::error_code(1, std::system_category());
         }
      }
      retryCount = 0;
      
      if (code != SQLITE_OK)
      {
         Log::warnf("Sqlite error preparing statement (%s)", sqlite3_errmsg(mDB));
         finishQuery();
         return std::error_code(1, std::system_category());
      }
      
      doBindPack(mCurrentStatement, query->mPack);
      
      //char * str = sqlite3_expanded_sql(mCurrentStatement);
      //Log::printf("exec: %s", str);
      
      // SQLITE_OK, SQLITE_ROW, and SQLITE_DONE
      int status = SQLITE_BUSY;
      while (status == SQLITE_ROW || status == SQLITE_BUSY)
      {
         status = sqlite3_step(mCurrentStatement);
         
         if (status == SQLITE_DONE)
         {
            mLastID = sqlite3_last_insert_rowid(mDB);
            mChangedRows = sqlite3_changes(mDB);
         }
      }
      
      finishQuery();
      
      if (status == SQLITE_BUSY)
      {
         Log::warnf("Sqlite query timed out");
      }
      
      return std::error_code(status == SQLITE_DONE ? 0 : 1, std::system_category());
	}
   
   virtual void finishQuery()
   {
      std::lock_guard<std::recursive_mutex> lock(mMutex);
      if (mCurrentRowBlobs.size() > 0)
      {
         for (int i=0; i<mCurrentRowBlobs.size(); i++)
         {
            delete[] mCurrentRowBlobs[i];
         }
         mCurrentRowBlobs.clear();
      }
      if (mCurrentStatement)
         sqlite3_finalize(mCurrentStatement);
      mCurrentStatement = NULL;
      mChangedRows = 0;
   }
	
	virtual std::error_code doRowQuery(std::shared_ptr<SQLPackQueueRef> query, bool multi, std::vector<DBResult>& outRows, std::function<void(std::error_code&,uint32_t,std::vector<DBResult>&)>&& callback)
	{
      std::lock_guard<std::recursive_mutex> lock(mMutex);
      finishQuery();
      
      sqlite3_busy_timeout(mDB, 1);
      const char *zSql = query->mPack->query();

      int code = SQLITE_OK;
      uint32_t resultCount = 0;
      outRows.clear();
      uint32_t retryCount = 0;
      
      // SQLITE_ABORT
      while (zSql[0])
      {
         code = SQLITE_BUSY;
         while (code == SQLITE_BUSY)
         {
            code = sqlite3_prepare_v2(mDB, zSql, -1, &mCurrentStatement, &zSql);
            retryCount++;
            if (retryCount >= 3)
            {
               Log::warnf("Sqlite query prepare timed out");
               finishQuery();
               std::error_code ec(1, std::system_category());
               callback(ec, 0, outRows);
               return ec;
            }
         }
         retryCount = 0;
         
         if (code != SQLITE_OK)
         {
            Log::warnf("Sqlite error preparing statement (%s)", sqlite3_errmsg(mDB));
            finishQuery();
            std::error_code ec(1, std::system_category());
            callback(ec, 0, outRows);
            return ec;
         }
         
         if (zSql[0] && !multi)
         {
            Log::errorf("Multi query with multiple statements");
            finishQuery();
            std::error_code ec(1, std::system_category());
            callback(ec, 0, outRows);
            return ec;
         }
         
         doBindPack(mCurrentStatement, query->mPack);
         
         // SQLITE_OK, SQLITE_ROW, and SQLITE_DONE
         int status = SQLITE_BUSY;
         uint32_t outPos = 0;
         
         while (status == SQLITE_ROW || status == SQLITE_BUSY)
         {
            status = sqlite3_step(mCurrentStatement);
            
            // might get SQLITE_ROW
            if (status == SQLITE_ROW)
            {
               DBResult row;
               memset(&row, '\0', sizeof(row));
               
               int numCols = sqlite3_column_count(mCurrentStatement);
               uint32_t totSz = 0;
               for (int i=0; i<numCols; i++)
               {
                  if (sqlite3_column_type(mCurrentStatement, i) == SQLITE_NULL)
                  {
                     row.lengths[i] = 0;
                     continue;
                  }
                  int sz = sqlite3_column_bytes(mCurrentStatement, i);
                  row.lengths[i] = (uint32_t)std::max<uint32_t>(sz,0);
                  totSz += row.lengths[i]+1;
               }
               
               totSz += sizeof(uintptr_t)*numCols;
               totSz += sizeof(uintptr_t)-1;
               totSz /= sizeof(uintptr_t);
               
               uintptr_t* blob = new uintptr_t[totSz];
               mCurrentRowBlobs.push_back(blob);
               
               const char** outVals = (const char**)blob;
               char* blobc = (char*)(blob+numCols);
               
               for (int i=0; i<numCols; i++)
               {
                  outVals[i] = NULL;
                  
                  if (row.lengths[i] > 0)
                  {
                     outVals[i] = blobc;
                     memcpy(blobc, sqlite3_column_blob(mCurrentStatement, i), row.lengths[i]);
                     blobc += row.lengths[i];
                     *blobc++ = '\0';
                  }
               }
               
               assert((char*)blobc <= (char*)(blob + totSz));
               
               row.value = outVals;
               outRows.push_back(row);
               
               continue;
            }
            
            if (status == SQLITE_DONE)
            {
               mLastID = sqlite3_last_insert_rowid(mDB);
               std::error_code ec(0, std::system_category());
               callback(ec, resultCount, outRows);
               outRows.clear();
               
               break;
            }
            
            if (status == SQLITE_BUSY)
            {
               retryCount++;
               if (retryCount >= 3)
               {
                  // error out
                  Log::warnf("Sqlite query timed out");
                  break;
               }
            }
         }
         
         // Query error?
         if (status != SQLITE_DONE)
         {
            std::error_code ec(1, std::system_category());
            callback(ec, resultCount, outRows);
            outRows.clear();
         }
         
         resultCount++;
      }
      
      return std::error_code(code == SQLITE_DONE ? 0 : 1, std::system_category());
	}

   enum
   {
      NumMassInsertBatch = 5
   };
   
   template<typename T> int doWhileBusy(int maxRetries, T func)
   {
      int status = SQLITE_BUSY;
      uint32_t retryCount = 0;
      while (status == SQLITE_BUSY)
      {
         status = func();
         retryCount++;
         if (retryCount >= maxRetries)
         {
            Log::warnf("Sqlite query prepare timed out");
            finishQuery();
            return status;
         }
      }
      
      return status;
   }
   
   template<typename T> int doWhileBusyOrRow(int maxRetries, T func)
   {
      int status = SQLITE_BUSY;
      uint32_t retryCount = 0;
      while (status == SQLITE_BUSY || status == SQLITE_ROW)
      {
         status = func();
         retryCount++;
         if (status == SQLITE_BUSY && retryCount >= maxRetries)
         {
            Log::warnf("Sqlite query prepare timed out");
            finishQuery();
            return status;
         }
      }
      
      return status;
   }
   
   inline int startTransaction()
   {
      return doWhileBusy(3, [this]{
         return sqlite3_exec(mDB, "BEGIN TRANSACTION", NULL, NULL, NULL);
      });
   }
   
   inline int endTransaction()
   {
      return doWhileBusy(3, [this]{
         return sqlite3_exec(mDB, "COMMIT", NULL, NULL, NULL);
      });
   }
   
   inline int abortTransaction()
   {
      return doWhileBusy(3, [this]{
         return sqlite3_exec(mDB, "ROLLBACK", NULL, NULL, NULL);
      });
   }
   
   std::error_code doMassQuery(std::shared_ptr<SQLPackQueueRef> baseQuery, DBMassQueryFunc&& func)
   {
      std::lock_guard<std::recursive_mutex> lock(mMutex);
      finishQuery();
      
      sqlite3_busy_timeout(mDB, 1);
      
      const char *zSql = baseQuery->mPack->query();
      
      /*
       * Format:
       BEGIN TRANSACTION
       [sql...]   (ROLLBACK if error)
       COMMIT;
       */
      
      if (startTransaction() != SQLITE_OK)
         return std::error_code(1, std::system_category());
      
      SQLPack* packs[NumMassInsertBatch];
      
      // Prepare main query
      sqlite3_busy_timeout(mDB, 1);
      
      // SQLITE_ABORT
      int status = doWhileBusy(3, [this, baseQuery]{
         return sqlite3_prepare_v2(mDB, baseQuery->mPack->query(), -1, &mCurrentStatement, NULL);
      });
      
      if (status != SQLITE_OK)
      {
         abortTransaction();
         finishQuery();
         return std::error_code(1, std::system_category());
      }
      
      bool validQuery = true;
      while (validQuery)
      {
         std::error_code okEC(0, std::system_category());
         int numPacks = func(this, okEC, &packs[0], NumMassInsertBatch);
         
         // End of packs?
         if (numPacks == 0)
         {
            status = endTransaction();
            if (status != SQLITE_OK)
            {
               func(this, std::error_code(1, std::system_category()), NULL, 0);
               return std::error_code(1, std::system_category());
            }
            else
            {
               return std::error_code(0, std::system_category());
            }
            break;
         }
         
         // Do packs
         for (int i=0; i<numPacks; i++)
         {
            doBindPack(mCurrentStatement, packs[i]);
            
            // SQLITE_OK, SQLITE_ROW, and SQLITE_DONE
            status = doWhileBusyOrRow(3, [this]{
               return sqlite3_step(mCurrentStatement);
            });

            if (status == SQLITE_BUSY)
            {
               Log::warnf("Sqlite query timed out");
               validQuery = false;
               break;
            }
            else if (status != SQLITE_DONE)
            {
               Log::warnf("Sqlite query error");
               validQuery = false;
            }
            
            sqlite3_reset(mCurrentStatement);
         }
         
         // Cleanup current packs
         for (int i=0; i<numPacks; i++)
         {
            free(packs[i]);
         }
      }
      
      if (status != SQLITE_DONE)
      {
         abortTransaction();
         func(this, std::error_code(1, std::system_category()), NULL, 0);
      }

      finishQuery();
      
      return std::error_code(status == SQLITE_DONE ? 0 : 1, std::system_category());
   }

	void doThread(asio::io_service* svc)
	{
      DBQueryQueue::setConnection(this);
      std::shared_ptr<OVDBConnection> ptr = shared_from_this();
      
		if (!connect())
		{
			Log::errorf("Failed to open sqlite db");
			return;
		}

		// handle queries
      auto work = std::make_shared<asio::io_service::work>(*svc);
		svc->run();

		disconnect();
      
      Log::errorf("Sqlite work thread done");
	}

	void startThread(asio::io_service* svc)
	{
		mThread = new std::thread(std::bind(&OVDBConnectionSqlite::doThread, this, svc));
	}
	
};

OVDBConnectionPtr SQLiteConnectionSpawn(DBQueryQueue* queue, uint32_t mode, DBQueryQueue::ConfigInfo* cfg)
{
	std::shared_ptr<OVDBConnectionSqlite> ptr = std::make_shared<OVDBConnectionSqlite>();
	ptr->mCfg = *cfg;
   ptr->mMode = SQLITE_OPEN_SHAREDCACHE | SQLITE_OPEN_NOMUTEX;

	asio::io_service* dest = NULL;

	// Start thread
	if (mode == 0)
	{
      ptr->mMode |= SQLITE_OPEN_READONLY;
      dest = &queue->mReadQueue;
	}
	else if (mode == 1)
	{
      ptr->mMode |= SQLITE_OPEN_READWRITE;
      dest = &queue->mWriteQueue;
	}
	else
	{
		return std::shared_ptr<OVDBConnectionSqlite>();
	}

	ptr->startThread(dest);

	return ptr;
}

