/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "statsController.hpp"

static std::atomic<int64_t> statCountLoggedIn;
static std::atomic<int64_t> statCountLogin;
static std::atomic<int64_t> statCountLogout;

static std::atomic<int64_t> statCountConnected;
static std::atomic<int64_t> statCountConnect;
static std::atomic<int64_t> statCountDisconnect;

void StatsController::logUserLogin()
{
   statCountLoggedIn++;
   statCountLogin++;
}

void StatsController::logUserLogout()
{
   statCountLogout++;
   statCountLoggedIn--;
}

void StatsController::logConnect()
{
   statCountConnect++;
   statCountConnected++;
}

void StatsController::logDisconnect()
{
   statCountDisconnect++;
   statCountConnected--;
}

void StatsController::logRPCTicket(const RPCTicketPtr &ticket)
{
   
}

void StatsController::logDBQuery(uint64_t completionTime)
{
   
}

void StatsController::dump()
{
   Log::printf("Stats: Connections=%" PRId64 ", Logged in=%" PRId64, (int64_t)statCountLoggedIn, (int64_t)statCountConnected);
}

static TestTimerController* sTimerController;

void TestTimerController::init(asio::io_context &svc)
{
   sTimerController = new TestTimerController(svc);
}

TestTimerController* TestTimerController::instance()
{
   return sTimerController;
}
