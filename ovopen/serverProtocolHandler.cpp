/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <asio.hpp>
#include <atomic>
#include <iostream>
#include <unordered_map>
#include <ctime>
#include "coreUtil.hpp"
#include "packet.hpp"
#include "packetHandlers.hpp"
#include "protocolHandler.hpp"
#include "friendController.hpp"
#include "groupChat.hpp"
#include "userConnectionController.hpp"
#include "backendConnectionController.hpp"
#include "statsController.hpp"

std::string gBannerString;

bool OVProtocolHandler::onPreloginPacket(OVPacketHeader* header)
{
   char buf2[1024];
   
   if (header->getNativeId() == 102 && !header->isAuto())
   {
      char username[33];
      std::array<uint8_t, 20> providedHash;
      
      MemStream mem(header->getNativeSize(), header->getData());
      mem.read(32, &username[0]);
      username[32] = '\0';
      mem.read(20, &providedHash[0]);
      
      std::shared_ptr<OVProtocolHandler> self = shared_from_this();
      
      // Grab password from db

      mUsername = username;

      // Again
      DBQueryQueue::getQueue().addToQueueRows(self->getStrandRef(), SQLBindPack("SELECT password, username, id, permissions_mask, banned_until_time, is_vip, last_item_id, avatar_sex FROM users WHERE username = $1 LIMIT 1", username), false, [this, self, providedHash](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
         if (self->mClosed)
            return;
         
         bool isPatcher = (mProtocol & PROTOCOL_PATCHER) != 0;
         bool isBackend = (mProtocol & PROTOCOL_BACKEND) != 0;
         
         if (!ec && results.size() > 0)
         {
            auto row = results[0].value;
            uint64_t banTime = strtoull(row[4], NULL, 10);
            
            // Grab hash
            uint8_t tempHash[28];
            uint8_t actualHash[20];
            
            mUsername = row[1];
            
            memset(&tempHash[0], '\0', 28);
            hexs2bin(row[0], &tempHash[0], 20);
            memcpy(&mLocationPWDHash[0], &tempHash[0], 20);
            memcpy(&tempHash[20], &mSecurityCode[0], 8);
            
            sha1 hash;
            hash.add(&tempHash[0], 28);
            hash.finalize();
            hash.dump(&actualHash[0]);
            
            mPermissionsMask.code = DBToU32(row[3]);
            mIsVIP = DBToU32(row[5]);
            mLastInventoryID = DBToU32(row[6]);
            mAvatarSex = (uint8_t)DBToU32(row[7]);
            
            if (isPatcher && strcasecmp(mUsername.c_str(), "anonymous") != 0)
            {
               // anonymous only login
               uint8_t responsePacket[] = {0x4F,0x56,0x00,0x04,0x00,0x68, 0x00,0x00,0x00,0x02,
                  0x4F,0x56,0x00,0x04,0x00,0x01, 0x00,0x00,0x00,0x04};
               
               PacketMallocData* responseData = PacketMallocData::createFromData(sizeof(responsePacket), &responsePacket[0]);
               this->queuePacketData(responseData);
               this->queueCloseAfterWrite();
               return;
            }
            
            // Only super admins can login to backend
            if (isBackend && !mPermissionsMask.hasSuperAdminLevel())
            {
               uint8_t responsePacket[] = {0x4F,0x56,0x00,0x04,0x00,0x68, 0x00,0x00,0x00,0x02,
                  0x4F,0x56,0x00,0x04,0x00,0x01, 0x00,0x00,0x00,0x04};
               
               PacketMallocData* responseData = PacketMallocData::createFromData(sizeof(responsePacket), &responsePacket[0]);
               this->queuePacketData(responseData);
               this->queueCloseAfterWrite();
               return;
            }
            
            if (memcmp(&providedHash[0], &actualHash[0], 20) == 0)
            {
               Log::client_printf(*this, LogEntry::General, "Password is correct! uid=%s uname=%s", row[2], mUsername.c_str());
               
               
               // Quickly check if we are banned or suspended
               if (banTime == 1)
               {
                  uint8_t responsePacket[] = {0x4F,0x56,0x00,0x04,0x00,0x68, 0x00,0x00,0x00,0x04,
                     0x4F,0x56,0x00,0x04,0x00,0x01, 0x00,0x00,0x00,0x04}; // 4 == banned
                  
                  PacketMallocData* responseData = PacketMallocData::createFromData(sizeof(responsePacket), &responsePacket[0]);
                  this->queuePacketData(responseData);
                  this->queueCloseAfterWrite();
                  return;
               }
               else if (banTime > 1)
               {
                  auto unix_timestamp = std::chrono::seconds(std::time(NULL)).count();
                  if (unix_timestamp < banTime)
                  {
                     uint8_t responsePacket[] = {0x4F,0x56,0x00,0x04,0x00,0x68, 0x00,0x00,0x00,0x05,
                        0x4F,0x56,0x00,0x04,0x00,0x01, 0x00,0x00,0x00,0x04}; // 5 == suspended
                     
                     PacketMallocData* responseData = PacketMallocData::createFromData(sizeof(responsePacket), &responsePacket[0]);
                     this->queuePacketData(responseData);
                     this->queueCloseAfterWrite();
                     return;
                  }
               }
               
               
               mUid = DBToU32(row[2]);
               Log::client_printf(*this, LogEntry::Debug, "Client logged in with uid %u\n", mUid);
               
               UserConnectionController::handleLogin(mUid, mUsername, shared_from_this());
               
               // We'll respond with two packets: 205(MasterVersion) and 103(LoginSuccess).
               
               //PacketMallocData* responseData = PacketMallocData::createOfSize(50 + (6*2) + 16);
               
               const char* banner = gBannerString.c_str();//"Beta ovopen 0.01 (00000000000000)";
               
               // Generate the security key
               const char* salt = "%^%zre$%^";
               uint8_t tempKey[38];
               memset(tempKey, '\0', sizeof(tempKey));
               memcpy(&tempKey[0], &mSecurityCode[0], 8);
               hexs2bin(row[0], &tempKey[8], 20);
               memcpy(&tempKey[28], salt, 10);
               
               sha1 hash;
               hash.add(&tempKey[0], 38);
               hash.finalize();
               hash.dump(&mSecurityCode[0]);
               BLOWFISH_Init(&mEncryptionKey, (uint8_t*)&mSecurityCode[0], 20, BLOWFISH_MODE_ECB, 0, 0);
               //
               
               MemStream packetStream(gBannerString.size() + 128);
               PacketBuilder packet(&packetStream);
               
               // First packet: banner
               // Server banner needs to be of the Form "Word Word (00000000000000)" to accept most clients.
               OVPacketHeader header;
               uint32_t bannerSize = strlen(banner);
               
               if (!isPatcher)
               {
                  packet.beginBasicPacket(PacketMasterVersion);
                  packetStream.write(bannerSize, banner);
                  packet.commit();
               }

               packet.mOffset = packetStream.getPosition();
               
               //
               packet.beginBasicPacket(PacketAuthSuccess);
               packetStream.writeUintBE(mUid);
               packetStream.writeUintBE(mPermissionsMask.code);
               packet.commit();
            
               PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
               this->queuePacketData(responseData);
               
               StatsController::logUserLogin();
               
               if (!self->allowMultipleLogins())
               {
                  // Update friend list
                  this->mStrand.post(std::bind(&FriendController::onUpdateFriendList, self));
                  
                  // Register with guides if we are a guide
                  if (!isPatcher && mPermissionsMask.hasGuideLevel())
                  {
                     GroupChatHandler* guideRoom = GroupChatHandler::getRoom(GroupChatHandler::ROOM_GUIDE);
                     if (guideRoom)
                     {
                        guideRoom->addListener(*this);
                     }
                  }
               }
            }
            else
            {
               //printf("Password is incorrect!\n");
               Log::client_printf(*this, LogEntry::General, "Password is incorrect for user %s!", mUsername.c_str());
               uint8_t responsePacket[] = {0x4F,0x56,0x00,0x04,0x00,0x68, 0x00,0x00,0x00,0x02,
                  0x4F,0x56,0x00,0x04,0x00,0x01, 0x00,0x00,0x00,0x04};
               
               PacketMallocData* responseData = PacketMallocData::createFromData(sizeof(responsePacket), &responsePacket[0]);
               this->queuePacketData(responseData);
               this->queueCloseAfterWrite();
            }
         }
         else if (ec)
         {
            // DB error
            Log::client_printf(*this, LogEntry::General, "DB Error (%s)!", ec.message().c_str());
            uint8_t responsePacket[] = {0x4F,0x56,0x00,0x04, 0x00,0x68,0x00,0x00, 0x00,0x02,0x4F,0x56,0x00,0x04,0x00,0x01, 0x00,0x00,0x00,0x05};
            
            PacketMallocData* responseData = PacketMallocData::createFromData(sizeof(responsePacket), &responsePacket[0]);
            this->queuePacketData(responseData);
            this->queueCloseAfterWrite();
         }
         else
         {
            // User doesn't exist
            Log::client_printf(*this, LogEntry::General, "Invalid user %s!", mUsername.c_str());
            uint8_t responsePacket[] = {0x4F,0x56,0x00,0x04, 0x00,0x68,0x00,0x00, 0x00,0x02,0x4F,0x56,0x00,0x04,0x00,0x01, 0x00,0x00,0x00,0x04};
            
            PacketMallocData* responseData = PacketMallocData::createFromData(sizeof(responsePacket), &responsePacket[0]);
            this->queuePacketData(responseData);
            this->queueCloseAfterWrite();
         }
      });
   }
   
   return true;
}

void OVProtocolHandler::onLogout()
{
   if (mUid != 0)
   {
      // TODO: clear in strand?
      if (UserConnectionController::clearConnectionIfSame(mUid, shared_from_this()))
      {
         FriendController::onDispatchOnlineStatusToFriends(shared_from_this(), FRIENDSTATE_OFFLINE);
         
         // Unregister with guides if we are a guide
         if (mPermissionsMask.hasGuideLevel())
         {
            GroupChatHandler* guideRoom = GroupChatHandler::getRoom(GroupChatHandler::ROOM_GUIDE);
            if (guideRoom)
            {
               guideRoom->removeListener(*this);
            }
         }
      }
      
      BackendConnectionController::notifyLogout(shared_from_this());
      StatsController::logUserLogout();
   }
   
   if (mProtocol & PROTOCOL_BACKEND)
   {
      BackendConnectionController::unregisterConnection(shared_from_this());
   }
}
