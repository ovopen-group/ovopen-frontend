/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <asio.hpp>
#include <atomic>
#include <iostream>
#include <unordered_map>
#include "coreUtil.hpp"
#include "packet.hpp"
#include "packetHandlers.hpp"
#include "protocolHandler.hpp"
#include "log.hpp"
#include "statsController.hpp"

OVProtocolHandler::OVProtocolHandler(asio::io_service &svc, tcp::socket socket, uint8_t protocol, OVConnectionState* state, uint32_t socketID)
: mStrand(svc), mIdleTimer(svc), mReadRestartTimer(svc), mSocket(std::move(socket)), mUid(0)
{
   mIsWriting = false;
   
   mIdleState = IDLE_WAIT_PING;
   mCloseAfterWrite = false;
   mClosed = false;
   mReadState = ReadBufferProcessor::READ_IDLE;
   
   mProtocol = protocol;
   mSocketID = socketID;
   
   mPermissionsMask.code = 0;
   mUid = 0;
   mLastRequestID = 0;
   mLimitPendingRPCRequests = 0;
   
   mState = state;
   mReadLimitKB = 0xFFFF;
   
   mRPCTickets.reserve(ReserveRPCRequests);
}

OVProtocolHandler::~OVProtocolHandler()
{
   delete mState;
}

void OVProtocolHandler::setAuthCode()
{
   for (int i=0; i<20; i++)
   {
      mSecurityCode[i] = rand();
   }
}

void OVProtocolHandler::onClose()
{
   if (mClosed)
      return;
   
   mIdleTimer.cancel();
   onLogout();
   
   std::error_code ignored_ec;
   mSocket.shutdown(asio::ip::tcp::socket::shutdown_both, ignored_ec);
   mClosed = true;
   
   Log::client_printf(*this, LogEntry::General, "Connection closed");
   StatsController::logDisconnect();
}

void OVProtocolHandler::onCloseAfterWrite()
{
   if (!mIsWriting)
      onClose();
   else
      mCloseAfterWrite = true;
}

void OVProtocolHandler::kick(int delayMS)
{
   mStrand.post(std::bind(&OVProtocolHandler::onKick, shared_from_this(), delayMS));
}

void OVProtocolHandler::onKick(int delayMs)
{
   mIdleState = IDLE_CLOSING;
   if (delayMs < 0) {
      onClose();
   } else if (delayMs == 0) {
      onCloseAfterWrite();
   } else {
      // Repurpose idle timer to close the connection
      mIdleTimer.expires_after(std::chrono::milliseconds(delayMs));
      mIdleTimer.async_wait(mStrand.wrap(std::bind(&OVProtocolHandler::onIdleTimer, shared_from_this(), std::placeholders::_1)));
   }
}

/*
 Idle timer works as follows:
 - If a packet has been received, the last packet time is set
 - Every PingTime, the onIdleTimer is called
 - If the last packet time exceeds the expected time, or the state is in IDLE_CLOSING, the connection is closed
 - onIdleTimer schedules the next check time to the last_packet_time + PingTime
 */
void OVProtocolHandler::onIdleTimer(const std::error_code& error)
{
   if (error)
      return;
   auto now = OVRuntimeTimerType::clock_type::now().time_since_epoch();
   uint64_t timeSinceEpoch = std::chrono::duration_cast<std::chrono::milliseconds>(now).count();
   uint64_t expectedPacketTime = mReadProcessor.mLastPacketTime + PingTime;
   
   if (mIdleState == IDLE_WAIT_PING)
   {
#ifdef PING_DEBUG
      Log::printf("IDLE TIMER CALLED, last=%" PRIu64 ", time = %" PRIu64 ", expected_time = %" PRIu64 " state=%u", mReadProcessor.mLastPacketTime, timeSinceEpoch, expectedPacketTime, mIdleState);
#endif
      // Send a ping if its time
      if (timeSinceEpoch >= expectedPacketTime)
      {
#ifdef PING_DEBUG
         Log::printf("  ^^ TIME TO SEND PING");
#endif
         uint8_t pingPacket[] = {0x4F,0x56,0x00,0x00,0x00,PacketPing};
         mIdleState = IDLE_WAIT_PING_ACK;
         queuePacketData(PacketMallocData::createFromData( sizeof(pingPacket), pingPacket));
         expectedPacketTime += PingTime;
      }
   }
   else
   {
      
      if (mIdleState == IDLE_CLOSING)
      {
         onClose();
         return;
      }
      
      // Should be waiting for the delay after the ping was sent
      expectedPacketTime += PingTime;
      
      //Log::printf("IDLE TIMER CALLED, last=%" PRIu64 ", time = %" PRIu64 ", expected_time = %" PRIu64 " state=%u", mLastPacketTime.count(), timeSinceEpoch.count(), expectedPacketTime.count(), mIdleState);
      
      if (timeSinceEpoch >= expectedPacketTime)
      {
         //Log::printf("  ^^ WAITED TOO LONG FOR PING");
         onClose();
         return;
      }
   }
   
   // Re-schedule. Note we add 10ms since this timer isn't 100% precise for whatever reason
   std::chrono::time_point<OVRuntimeTimerType::clock_type> tp(std::chrono::milliseconds(expectedPacketTime) + std::chrono::milliseconds(10));
   asio::error_code ec;
   mIdleTimer.expires_at(tp, ec);
   mIdleTimer.async_wait(mStrand.wrap(std::bind(&OVProtocolHandler::onIdleTimer, shared_from_this(), std::placeholders::_1)));
   //Log::printf("  ^^ RE-SCHEDULED FOR %" PRIu64 , expectedPacketTime.count());
}

void OVProtocolHandler::queuePacketData(OVFreeableData* data)
{
   if ((mProtocol & PROTOCOL_BACKEND) == 0)
   {
      OVPacketHeader *header = (OVPacketHeader*)data->ptr();
      if (header->isAuto() && header->getNativeId() == 10001)
      {
         MemStream str(8, data->ptr() + 6);
         uint16_t rpcID;
         str.readUintBE(rpcID);
         if (rpcID > 9000)
         {
            Log::errorf("WTF!!!!! WRONG RPC PACKET TYPE HERE!!!\n");
         }
      }
   }
   mStrand.post(std::bind(&OVProtocolHandler::onQueueWriteData, shared_from_this(), data));
}

void OVProtocolHandler::queueCloseAfterWrite()
{
   mStrand.post(std::bind(&OVProtocolHandler::onCloseAfterWrite, shared_from_this()));
}

// Queue freeable data. This should always be called within a strand handler.
void OVProtocolHandler::onQueueWriteData(OVFreeableData* data)
{
   if (mClosed)
   {
      data->free();
      return;
   }
   mQueue.push_back(data);
   onSendNextDataPackets();
}

// Write callback. This should always be called within a strand handler.
void OVProtocolHandler::onDataWritten(std::error_code ec, std::size_t sz)
{
   if (!ec)
   {
      Log::client_printf(*this, LogEntry::Debug, "Sent %i bytes", sz);
      //assert(sz == mBytesInWriteBuffer);
      mIsWriting = false;
      onSendNextDataPackets();
      return;
   }
   else if (ec != asio::error::operation_aborted)
   {
      Log::client_printf(*this, LogEntry::Debug, "client, LogEntry::Debug, Socket write error (%s)", ec.message().c_str());
      onClose();
   }
   mIsWriting = false;
}

// Send next data packets. This should always be called within a strand handler.
void OVProtocolHandler::onSendNextDataPackets()
{
   if (mIsWriting)
      return;
   
   uint32_t writeBufferBytes = 0;
   
   uint32_t packetsWritten = 0;
   for (std::vector<OVFreeableData*>::iterator itr = mQueue.begin(), itrEnd = mQueue.end(); itr != itrEnd; itr++)
   {
      // Write as many small packets as we can fit in the buffer
      if (*itr)
      {
         if (Log::acceptType(LogEntry::Debug))
         {
            char buf[128];
            snprintf(buf, 128, "[C%u]: sendData", mSocketID);
            (*itr)->debugString(buf);

#ifdef EXTRA_PACKET_DEBUG
            // DEBUG
            int bytesLeft = ((*itr)->size());
            uint8_t* basePtr = (uint8_t*)((*itr)->ptr());
            while (bytesLeft > 0)
            {
               printf("CHECK PACKET BL=%i\n", bytesLeft);
               OVPacketHeader* hdr = (OVPacketHeader*)basePtr;

               if (!hdr->isValid())
               {
                  printf("Something wrong with header. bp=%llu\n", basePtr);
                  assert(false);
               }
               if (hdr->getPacketSize() > bytesLeft)
               {
                  assert(false);
               }

               bytesLeft -= hdr->getPacketSize()+6;
               basePtr += hdr->getPacketSize()+6;
            }
#endif

         }

         // Put in write buffer
         uint32_t bytesLeft = ReadBufferSize-writeBufferBytes;
         uint32_t dataToWrite = (*itr)->size();
         if (bytesLeft < dataToWrite)
         {
            // Can't write anything else
            break;
         }
         else
         {
            memcpy(&mWriteBuffer[writeBufferBytes], (*itr)->ptr(), dataToWrite);
            writeBufferBytes += dataToWrite;
            (*itr)->free();
            packetsWritten++;
         }
      }
   }

   if (mQueue.size() != 0 && packetsWritten == 0)
   {
      // Write next packet as large packet
      std::shared_ptr<OVProtocolHandler> clientHandler = shared_from_this();
      mIsWriting = true;
      asio::async_write(mSocket, 
         asio::buffer(mQueue[0]->ptr(), mQueue[0]->size()),
         [clientHandler, this](std::error_code ec, std::size_t sz){
            mStrand.post([clientHandler, ec, sz, this]{
               mIsWriting = false;
               mQueue[0]->free();
               mQueue.erase(mQueue.begin());
               Log::client_printf(*clientHandler.get(), LogEntry::General, "Large packet written");
               clientHandler->onDataWritten(ec, sz);
            });
         }
      );
   }
   else if (packetsWritten > 0)
   {
      
      // DEBUG
      if ((mProtocol & PROTOCOL_BACKEND) == 0)
      {
         uint8_t* dbgPtr = &mWriteBuffer[0];
         uint8_t* endPtr = dbgPtr + writeBufferBytes;
         
         while (dbgPtr < endPtr)
         {
            OVPacketHeader *header = (OVPacketHeader*)dbgPtr;
            if (header->isAuto() && header->getNativeId() == 10001)
            {
               MemStream str(8, dbgPtr + 6);
               uint16_t rpcID;
               str.readUintBE(rpcID);
               if (rpcID > 9000)
               {
                  Log::errorf("WTF!!!!! WRONG RPC PACKET TYPE HERE!!!\n");
               }
            }
            dbgPtr += header->getNativeSize()+6;
         }
      }
      // END DEBUG
      
      mQueue.erase(mQueue.begin(), mQueue.begin() + packetsWritten);
      mIsWriting = true;
      asio::async_write(mSocket, asio::buffer(&mWriteBuffer[0], writeBufferBytes),
                        mStrand.wrap(std::bind(&OVProtocolHandler::onDataWritten, shared_from_this(), std::placeholders::_1, std::placeholders::_2)));
   }
   else if (mCloseAfterWrite)
   {
      onClose();
   }
}


void OVProtocolHandler::start(uint16_t kbpslimit)
{
   StatsController::logConnect();
   
   {
      asio::ip::tcp::endpoint remote_ep = mSocket.remote_endpoint();
      asio::ip::address remote_ad = remote_ep.address();
      std::string s = remote_ad.to_string();
      Log::client_printf(*this, LogEntry::General, "New connection from %s:%u", remote_ad.to_string().c_str(), remote_ep.port());
   }
   
   mLastRequestID = 0;
   mReadLimitKB = kbpslimit;
   mReadProcessor.reset();
   startRead();
   //setAuthCode();
   
   if ((mProtocol & PROTOCOL_CLIENT) == 0)
   {
      // We need to send a packet
      uint8_t staticData[] = {0x4F,0x76,0x00,0x08,0x00,0x65, 0x16,0xCB,0x49,0xE6,0x6E,0x31,0x87,0x81};
      
      //OVPacketHeader* header = (OVPacketHeader*)staticData;
      
      // TODO USE RAND CODE
      memcpy(&mSecurityCode[0], &staticData[6], 8);
      
      if ((mProtocol & PROTOCOL_BACKEND_TOOL) == 0)
      {
         uint8_t* packetData = new uint8_t[sizeof(staticData)];
         memcpy(packetData, staticData, sizeof(staticData));
         
         PacketMallocData* mallocDat = new PacketMallocData();
         mallocDat->mPtr = packetData;
         mallocDat->mSize = sizeof(staticData);
         
         queuePacketData(mallocDat);
      }
      
      // Start ping timer
      mIdleTimer.expires_after(std::chrono::milliseconds(PingTime));
      mIdleTimer.async_wait(mStrand.wrap(std::bind(&OVProtocolHandler::onIdleTimer, shared_from_this(), std::placeholders::_1)));
   }
}

void OVProtocolHandler::startRead()
{
   if (mReadState == ReadBufferProcessor::READ_READING || mClosed || mCloseAfterWrite)
      return;
   
   onDataRead();
}

static uint32_t debugPacketID = 0;

void OVProtocolHandler::doRead(uint32_t offset, uint32_t size)
{
   uint32_t start_buffer = offset;
   mReadBuffer.mReadHead = offset;
   if (size == 0)
      return;
   
   mReadState = ReadBufferProcessor::READ_READING;
   
   std::shared_ptr<OVProtocolHandler> self = shared_from_this();
   mSocket.async_read_some(asio::buffer(mReadBuffer.getReadBufferHeadPtr(), size),
                           mStrand.wrap([this, self](std::error_code ec, std::size_t length)
                                        {
                                       #ifdef EXTRA_PACKET_DEBUG
                                           char buf[512];
                                           snprintf(buf, 512, "P%u", debugPacketID);
                                           debugPacketID++;

                                           FILE* fp = fopen(buf, "wb");
                                           fwrite(mReadBuffer.getReadBufferHeadPtr(), length, 1, fp);
                                           fclose(fp);
                                       #endif

                                           if (!ec)
                                           {
                                              auto now = OVRuntimeTimerType::clock_type::now().time_since_epoch();
                                              auto timeSinceEpoch = std::chrono::duration_cast<std::chrono::milliseconds>(now);
                                              mReadBuffer.mReadHead += length;
                                              mReadProcessor.mLastPacketTime = timeSinceEpoch.count();
                                              mReadProcessor.addBytesRead(length);
                                              if (Log::acceptType(LogEntry::Debug)) Log::client_printf(*this, LogEntry::Debug, ">>>> Read %i bytes, buffer bytes now %i", length, mReadBuffer.mReadHead);
                                              onDataRead();
                                           }
                                           else if (ec != asio::error::operation_aborted)
                                           {
                                              // close?
                                              mReadState = ReadBufferProcessor::READ_IDLE;
                                              Log::client_errorf(*this, LogEntry::Debug, "Error reading (%s)", ec.message().c_str());
                                              onClose();
                                           }
                                           else
                                           {
                                              mReadState = ReadBufferProcessor::READ_IDLE;
                                           }
                                        }));
}

// Read callback. Should always be called within a strand.
void OVProtocolHandler::onDataRead()
{
   auto now = OVRuntimeTimerType::clock_type::now().time_since_epoch();
   auto timeSinceEpoch = std::chrono::duration_cast<std::chrono::milliseconds>(now);
   
   PacketIterator<ReadBufferSize> packetItr(mReadBuffer.getReadBufferPtr(),
                                            0, mReadBuffer.getReadBytes(),
                                            mReadBuffer.getProcessedBytes(),
                                            &mEncryptionKey);
   
   mReadState = ReadBufferProcessor::READ_PROCESSING;
   if (!mReadProcessor.processData(this, packetItr, timeSinceEpoch.count()))
      return;
   
   mReadState = mReadProcessor.enqueueRead(this, packetItr);
   
#ifdef PACKET_ADV_DEBUG
   if (packetItr.processed_offset == 0 && mReadBuffer.mReadHead > 6)
   {
      PacketAlreadyDecrypted((OVPacketHeader*)mReadBuffer.mData, &mEncryptionKey);
   }
#endif
   
   mReadBuffer.mProcessedHead = packetItr.processed_offset;
   
   now = OVRuntimeTimerType::clock_type::now().time_since_epoch();
   timeSinceEpoch = std::chrono::duration_cast<std::chrono::milliseconds>(now);
   
   // Handle new state
   if (mReadState == ReadBufferProcessor::READ_LIMITED)
   {
      // Try reading again at earliest opportunity if we're limited
      uint64_t nextTime = mReadProcessor.getTimeUntilNextWindow(timeSinceEpoch.count());
      Log::client_printf(*this, LogEntry::Debug, "Delaying next read for %u ms", (uint32_t)nextTime);
      mReadRestartTimer.expires_after(std::chrono::milliseconds(nextTime+10));
      mReadRestartTimer.async_wait(mStrand.wrap(std::bind(&OVProtocolHandler::onRestartReadTimer, shared_from_this(), std::placeholders::_1)));
   }
   else if (mReadState == ReadBufferProcessor::READ_BUFFER_FULL)
   {
      Log::client_printf(*this, LogEntry::Debug, "Buffer is full");
   }
   else if (mReadState != ReadBufferProcessor::READ_READING)
   {
      // Not possible, but who knows
      Log::client_printf(*this, LogEntry::Debug, "Unexpected state");
   }
}

void OVProtocolHandler::onRestartReadTimer(const std::error_code& ec)
{
   if (ec || (mReadState == ReadBufferProcessor::READ_READING) || mClosed || mCloseAfterWrite)
   {
      Log::client_printf(*this, LogEntry::Debug, "Ignoring restart timer");
      return;
   }
   
   onDataRead();
}

bool OVProtocolHandler::allowMultipleLogins()
{
    bool isPatcher = mProtocol & PROTOCOL_PATCHER;
    bool isBackend = mProtocol & PROTOCOL_BACKEND;
   
   return isPatcher || isBackend;
}

bool OVProtocolHandler::acceptsPacketType(uint32_t code, bool is_auto)
{
   bool isClient = (mProtocol & PROTOCOL_CLIENT) != 0;
   bool loggedOut = mUid == 0;
   bool isTool = (mProtocol & PROTOCOL_BACKEND_TOOL) != 0;
   return isClient || isTool || (loggedOut ? (code == 102 || code == 3) : true);
}

// Packet callback. Should always be called within a strand.
bool OVProtocolHandler::onPacket(OVPacketHeader* header)
{
   // Wait for next packet
   if (Log::acceptType(LogEntry::Debug))
   {
      Log::client_printf(*this, LogEntry::Debug, "Got %spacket %i of full size %u", header->isAuto() ? "auto " : "", header->getNativeId(), header->getPacketSize());
   }
   
   uint16_t packetId = header->getNativeId();
   
   if (!acceptsPacketType(packetId, header->isAuto()))
   {
      // Only accept packet 102 or 2 in logged out state
      if (Log::acceptType(LogEntry::Debug))
      {
         Log::client_errorf(*this, LogEntry::Debug, "Client sent packet id %i when logged out!", packetId);
      }
      
      onClose();
      return true;
   }
   
   bool inPrelogin = inPreloginPhase();
   
   if (inPrelogin && (header->isEncrypted() || header->isCompressed()))
   {
      // Don't accept compressed or encrypted packets when logged out
      if (Log::acceptType(LogEntry::Debug))
      {
         Log::printf(LogEntry::Debug, "Can't accept advanced packets when logged out");
      }
      
      onClose();
      return true;
   }
   
   if (header->getNativeId() == PacketPong)
   {
      if (mIdleState == IDLE_WAIT_PING_ACK)
      {
         mIdleState = IDLE_WAIT_PING;
      }
      return true;
   }
   
   if ((mProtocol & PROTOCOL_CLIENT) != 0 && header->getNativeId() == PacketPing)
   {
      OVPacketHeader pongPacket;
      pongPacket.setBasicPacket();
      pongPacket.setNativeId(PacketPong);
      pongPacket.setNativeSize(0);
      
      queuePacketData(PacketMallocData::createFromData(sizeof(pongPacket), (uint8_t*)&pongPacket));
      return true;
   }
   
   if (inPrelogin)
   {
      if (onPreloginPacket(header))
         return true;
   }
   
   // Defer to proper handlers
   OVPacketInfo packetInfo;
   
   PacketHandler* handler = PacketHandler::getHandlerForPacket(header);
   if (handler && ((handler->getPacketProtocol() & mProtocol) != 0) )
   {
      // Use handler
      OVFreeableData* allocData = NULL;
      int handleCode = handler->preHandle(*this, header);
      if (handleCode < 0)
      {
         return true; // Invalid format
      }
      else if (handleCode < 1)
      {
         return false; // Don't process yet
      }
      
      packetInfo = DecodePacket(header, &mEncryptionKey, true);
      if (!packetInfo.isValid)
      {
         if (Log::acceptType(LogEntry::Debug))
         {
            Log::client_errorf(*this, LogEntry::Debug, "Invalid packet");
         }
      }
      else
      {
         handler->handle(*this, packetInfo);
      }
   }
   else
   {
      if (Log::acceptType(LogEntry::Debug))
      {
         Log::client_errorf(*this, LogEntry::Debug, "Unknown packet %u", header->getNativeId());
      }
      
      // 0x3e9 (1001) requestCatChildren
      // 0x3eb () requestMoveCatObject
      // 0x3ec requestAddCategory
      // 0x3ed requestDeleteCategory
      // 0x3ee requestRenameCatObject
      // 0x3ef (1007) requestResourceData
      // 0x3f1 requestUpdateResource  (wrapped in RPC)
      // 0x3f2 requestDeleteResource
   }
   
   return true;
}

bool OVProtocolHandler::inPreloginPhase()
{
   return mUid == 0;
}

bool OVServerGameConnectionState::isIgnoringUser(uint32_t uid)
{
   auto itr = std::find_if(mFriendList.begin(), mFriendList.end(), [uid](const FriendStatusRecord& item){
      return item.uid == uid && item.state == FRIENDSTATE_IGNORE;
   });
   
   if (itr == mFriendList.end())
      return false;
   else
      return true;
}

bool OVServerGameConnectionState::isFriend(uint32_t uid)
{
   auto itr = std::find_if(mFriendList.begin(), mFriendList.end(), [uid](const FriendStatusRecord& item){
      return item.uid == uid && item.state <= FRIENDSTATE_ONLINE;
   });
   
   if (itr == mFriendList.end())
      return false;
   else
      return true;
}

bool OVServerGameConnectionState::isFriendOrPending(uint32_t uid)
{
   FriendState state = getFriendState(uid);
   return (state <= FRIENDSTATE_ONLINE || state == FRIENDSTATE_PENDING);
}

bool OVServerGameConnectionState::isPendingFriend(uint32_t uid)
{
   FriendState state = getFriendState(uid);
   return (state == FRIENDSTATE_PENDING);
}

bool OVServerGameConnectionState::isAcceptableFriend(uint32_t uid)
{
   FriendState state = getFriendState(uid);
   return (state == FRIENDSTATE_REQUIRE_ACCEPT);
}

bool OVServerGameConnectionState::isBeingIgnoredBy(uint32_t uid)
{
   FriendState state = getFriendState(uid);
   return (state == FRIENDSTATE_BEING_IGNORED);
}

FriendState OVServerGameConnectionState::getFriendState(uint32_t uid)
{
   auto itr = std::find_if(mFriendList.begin(), mFriendList.end(), [uid](const FriendStatusRecord& item){
      return item.uid == uid;
   });
   
   if (itr == mFriendList.end())
      return FRIENDSTATE_DELETE;
   else
      return itr->state;
}

uint32_t OVServerGameConnectionState::getFriendCount()
{
   return mFriendList.size();
}

void OVProtocolHandler::sendRedirectPacket(uint8_t address[4])
{
   uint8_t outBuffer[6 + 4];
   
   OVPacketHeader header;
   header.setAutoPacket();
   header.setNativeSize(sizeof(outBuffer));
   header.setNativeId(PacketRedirect);
   
   MemStream outStream(6 + 4, &outBuffer);
   
   outStream.write(6, &header);
   outStream.write(4,&address);
   
   queuePacketData(PacketMallocData::createFromData(sizeof(outBuffer), outBuffer));
}



RPCTicketPtr OVProtocolHandler::beginRPCTicket(const RPCTicketInfo &requestInfo)
{
   uint32_t requestID = requestInfo.mRequestID;
   auto itr = std::find_if(mRPCTickets.begin(), mRPCTickets.end(), [requestID](const RPCTicketPtr& item) {
      return (item->mInfo.mRequestID == requestID);
   });
   
   if (itr != mRPCTickets.end())
   {
      Log::errorf("Ticket already started\n");
      return *itr;
   }
   
   RPCTicketPtr ticket(new RPCTicket(requestInfo));
   mRPCTickets.push_back(ticket);
   return ticket;
}

RPCTicketPtr OVProtocolHandler::findRPCTicket(const uint32_t requestId)
{
   auto itr = std::find_if(mRPCTickets.begin(), mRPCTickets.end(), [requestId](const RPCTicketPtr& item) {
      return (item->mInfo.mRequestID == requestId);
   });
   
   if (itr != mRPCTickets.end())
   {
      return *itr;
   }
   else
   {
      return RPCTicketPtr();
   }
}

void OVProtocolHandler::cancelRPCTicket(RPCTicketPtr &ticket)
{
   auto itr = std::find(mRPCTickets.begin(), mRPCTickets.end(), ticket);
   
   // Unlikely case
   if (ticket->mCancelled || ticket->mCompleted)
      return;
   
   // Send status response
   RPCTicketInfo &info = ticket->mInfo;
   ticket->mCancelled = true;
   ticket->mCompleted = true;
   
   info.mRPCHandler->createRPCResponseWithoutTicket(*this, info.mRequestID, info.mFailPacketID, info.mFailResponseCode);
   
   if (itr != mRPCTickets.end())
   {
      mRPCTickets.erase(itr);
   }
   
   StatsController::logRPCTicket(ticket);
}

bool OVProtocolHandler::hasRPCTicket(const uint32_t requestId)
{
   auto itr = std::find_if(mRPCTickets.begin(), mRPCTickets.end(), [requestId](const RPCTicketPtr& item) {
      return (item->mInfo.mRequestID == requestId);
   });
   
   return itr != mRPCTickets.end();
}

void OVProtocolHandler::endRPCTicket(const RPCTicketPtr &ptr)
{
   auto itr = std::find(mRPCTickets.begin(), mRPCTickets.end(), ptr);
   if (itr != mRPCTickets.end())
   {
      mRPCTickets.erase(itr);
   }
   
   ptr->mCompleted = true;
   
   StatsController::logRPCTicket(ptr);
   
   // Schedule a restart if we're not reading (since buffer might have been full)
   if (mReadState != ReadBufferProcessor::READ_READING)
   {
      // No use doing it if we're expecting the limiter to schedule a timer
      if (mReadState == ReadBufferProcessor::READ_SCHEDULED || mReadState == ReadBufferProcessor::READ_LIMITED || mReadState == ReadBufferProcessor::READ_PROCESSING)
         return;
      
      mReadState = ReadBufferProcessor::READ_SCHEDULED;
      
      std::error_code ec;
      mStrand.wrap(std::bind(&OVProtocolHandler::onRestartReadTimer, shared_from_this(), ec));
   }
}

uint32_t OVProtocolHandler::newRPCTicketID()
{
   mLastRequestID++;
   if (mLastRequestID == 0) // handle overflow
      mLastRequestID = 1;
   
   // Servers should store ids in lower 16 bits, clients should store ids in upper 16 bits
   if ((mProtocol & PROTOCOL_CLIENT) != 0)
   {
      return ((uint32_t)mLastRequestID) << 16;
   }
   else
   {
      return mLastRequestID;
   }
}

bool OVProtocolHandler::cantAcceptMoreRPCRequests()
{
   if (mLimitPendingRPCRequests > 0)
   {
      if (mRPCTickets.size() >= mLimitPendingRPCRequests)
      {
         return true;
      }
   }
   return false;
}

void OVProtocolHandler::onJunkPacket()
{
   Log::client_errorf(*this, LogEntry::General, "Client sent junk packet");
   onClose();
}

namespace Log
{
   extern std::vector<ConsumerCallback> gConsumers;
   
   static void _printf(OVProtocolHandler &handler, LogEntry::Level level, LogEntry::Type type, const char* fmt, va_list argptr)
   {
      char buffer[8192];
      uint32_t offset = 0;
      
      snprintf(buffer, 8192, "[C%u.%u]: ", handler.mSocketID, handler.mProtocol);
      offset += strlen(buffer);
      
      vsnprintf(buffer + offset, sizeof(buffer) - offset, fmt, argptr);
      
      LogEntry entry;
      entry.mData = buffer;
      entry.mLevel = level;
      entry.mType = type;
      for(uint32_t i = 0; i < gConsumers.size(); i++)
         gConsumers[i](level, &entry);
   }
   
   void client_printf(OVProtocolHandler &handler, LogEntry::Type type, const char *_format, ...)
   {
      va_list argptr;
      va_start(argptr, _format);
      _printf(handler, LogEntry::Normal, type, _format, argptr);
      va_end(argptr);
   }

   void client_warnf(OVProtocolHandler &handler, LogEntry::Type type, const char *_format, ...)
   {
      va_list argptr;
      va_start(argptr, _format);
      _printf(handler, LogEntry::Warning, type, _format, argptr);
      va_end(argptr);
   }

   void client_errorf(OVProtocolHandler &handler, LogEntry::Type type, const char *_format, ...)
   {
      va_list argptr;
      va_start(argptr, _format);
      _printf(handler, LogEntry::Error, type, _format, argptr);
      va_end(argptr);
   }

}
