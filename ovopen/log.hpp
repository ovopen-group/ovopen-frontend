/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _CORE_LOG_H_
#define _CORE_LOG_H_

#include <stdint.h>
#include <stdarg.h>

/// Represents an entry in the log.
struct LogEntry
{
   enum Level
   {
      Normal = 0,
      Warning,
      Error
   } mLevel;
   
   /// Line
   const char *mData;
   
   /// Type of entry
   typedef unsigned char Type;
   LogEntry::Type mType;
   
   static LogEntry::Type General;
   static LogEntry::Type Debug;
};

typedef void (*ConsumerCallback)(uint32_t level, LogEntry *logEntry);

namespace Log
{
   void init();
   
   void shutdown();
   
   bool isActive();
   
   void addConsumer(ConsumerCallback cb, uint32_t mask=0xFFFFFFFF);
   void removeConsumer(ConsumerCallback cb, uint32_t mask=0xFFFFFFFF);
   
   bool acceptType(LogEntry::Type type);
   
   void printf(const char *_format, ...);
   
   void warnf(const char *_format, ...);
   
   void errorf(const char *_format, ...);
   
   void printf(LogEntry::Type type, const char *_format, ...);
   
   void warnf(LogEntry::Type type, const char *_format, ...);
   
   void errorf(LogEntry::Type type, const char *_format, ...);
}

#endif
