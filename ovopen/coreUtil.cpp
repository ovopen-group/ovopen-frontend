/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "coreUtil.hpp"
#include <cstring>

size_t hexs2bin(const char *hex, unsigned char *out, size_t max_len)
{
   size_t len;
   char   b1;
   char   b2;
   size_t i;
   
   if (hex == NULL || *hex == '\0' || out == NULL)
      return 0;
   
   len = strlen(hex);
   if (len % 2 != 0)
      return 0;
   len /= 2;
   
   memset(out, '\0', len > max_len ? max_len : len);
   for (i=0; i<len && i < max_len; i++) {
      if (!hexchr2bin(hex[i*2], &b1) || !hexchr2bin(hex[i*2+1], &b2)) {
         return 0;
      }
      out[i] = (b1 << 4) | b2;
   }
   return len;
}

size_t bin2hex(const unsigned char *bin, size_t blen, char *hex)
{
   for (size_t i=0; i < blen; ++i)
   {
      hex[i*2] = "0123456789ABCDEF"[bin[i] >> 4];
      hex[i*2+1] = "0123456789ABCDEF"[bin[i] & 0x0F];
   }
   
   hex[blen*2] = '\0';
   return blen*2;
}

