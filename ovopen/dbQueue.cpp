#include <vector>
#include <thread>
#include <system_error>
#include <time.h>
#include <limits.h>
#include "dbQueue.hpp"
#include "packet.hpp"


#ifdef _MSC_VER
__declspec(thread) OVDBConnection* sCurrentConnection;
#else
thread_local OVDBConnection* sCurrentConnection;
#endif

void DBQueryQueue::setConnection(OVDBConnection* ptr)
{
   sCurrentConnection = ptr;
}

OVDBConnectionPtr DBQueryQueue::getConnection()
{
   return sCurrentConnection->shared_from_this();
}

DBQueryQueue::DBQueryQueue(asio::io_service &svc) : mMainQueue(svc)
{
	mPendingOperations = 0;
   mShutttingDown = 0;
}

DBQueryQueue::~DBQueryQueue()
{
   shutdown();
}

void DBQueryQueue::shutdown()
{
   mShutttingDown = 1;
   mWriteQueue.stop();
   mReadQueue.stop();
   mConnections.clear();
   mSpawnConnectionFunc = nullptr;
}

void DBQueryQueue::configurePool(const char* driver, DBQueryQueue::ConfigInfo cfg, int numReaders, int numWriters, int numReadWriters)
{
   if (strcasecmp(driver, "sqlite") == 0)
   {
      mSpawnConnectionFunc = &SQLiteConnectionSpawn;
   }
   else
   {
      Log::errorf("Unknown DB driver %s\n", driver);
      exit(1);
   }
   
   mConnectionConfig = cfg;
   
   // Spawn connections
   
   if (numReadWriters == 0)
   {
      mUseReadQueue = true;
      for (int i=0; i<numReaders; i++)
      {
         OVDBConnectionPtr ptr = mSpawnConnectionFunc(this, 0, &mConnectionConfig);
         if (ptr)
         {
            mConnections.push_back(ptr);
         }
      }
      
      for (int i=0; i<numWriters; i++)
      {
         OVDBConnectionPtr ptr = mSpawnConnectionFunc(this, 1, &mConnectionConfig);
         if (ptr)
         {
            mConnections.push_back(ptr);
         }
      }
   }
   else
   {
      mUseReadQueue = false;
      for (int i=0; i<numReadWriters; i++)
      {
         OVDBConnectionPtr ptr = mSpawnConnectionFunc(this, 1, &mConnectionConfig);
         if (ptr)
         {
            mConnections.push_back(ptr);
         }
      }
   }
      
   if (mConnections.size() == 0)
   {
      Log::errorf("Failed to spawn connections");
      exit(1);
   }
}

void DBQueryQueue::checkConnectionsLater(uint64_t delta_ms)
{
}

void DBQueryQueue::checkConnections()
{
   if (mShutttingDown)
      return;

	// ...
}
