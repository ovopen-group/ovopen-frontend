/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "groupChat.hpp"
#include "protocolHandler.hpp"

static std::unordered_map<uint32_t, GroupChatPtr> smGroupChats;

GroupChatHandler::GroupChatHandler(asio::io_service &svc) : mStrand(svc), mDispatchTimer(svc)
{
}

void GroupChatHandler::startDispatch()
{
   mDispatchTimer.expires_after(std::chrono::milliseconds(1000));
   mDispatchTimer.async_wait(mStrand.wrap(std::bind(&GroupChatHandler::dispatchQueuedMessages, shared_from_this(), std::placeholders::_1)));
}

void GroupChatHandler::queueMessage(OVFreeableData* data)
{
   mMessagesToSend.push_back(data);
}

void GroupChatHandler::dispatchMessageExceptToUser(PacketMultipleUserData* data, uint32_t exceptID)
{
   // We need to send this immediately since we can't optimally filter it otherwise
   for (std::shared_ptr<OVProtocolHandler> &handler : mListeners)
   {
      if (handler->mUid == exceptID)
         continue;
      //printf("]]Message data sent to %u\n",handler->mUid);
      
      if (Log::acceptType(LogEntry::Debug))
      {
         data->debugString();
      }
      
      handler->queuePacketData(data->cloneMultiple());
   }
}

void GroupChatHandler::dispatchQueuedMessages(const std::error_code &ec)
{
   if (ec)
      return;
   
   if (mMessagesToSend.size() > 0)
   {
      // Create a batch message
      uint32_t packetSize = 0;
      uint32_t numToSend = 0;
      
      // Determine size required
      for (OVFreeableData* dat : mMessagesToSend)
      {
         uint32_t msgSize = dat->size();
         packetSize += msgSize;
         
         if (packetSize > 65000)
         {
            packetSize -= msgSize;
            break;
         }
         numToSend++;
      }
      
      // Build packet
      PacketMultipleUserData* data = PacketMultipleUserData::createOfSize(packetSize);
      uint8_t* dest = data->ptr();
      for (uint32_t i=0; i<numToSend; i++)
      {
         uint32_t msgSize = mMessagesToSend[i]->size();
         memcpy(dest, mMessagesToSend[i]->ptr(), msgSize);
         dest += msgSize;
      }
      
      mMessagesToSend.erase(mMessagesToSend.begin(), mMessagesToSend.begin()+numToSend);
      
      for (std::shared_ptr<OVProtocolHandler> &handler : mListeners)
      {
         handler->queuePacketData(data->cloneMultiple());
      }
      
      delete data;
   }
   
   startDispatch();
}

void GroupChatHandler::addListener(OVProtocolHandler &client)
{
   std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
   mStrand.post([this,clientHandler](){
      mListeners.push_back(clientHandler);
   });
}

void GroupChatHandler::removeListener(OVProtocolHandler &client)
{
   std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
   mStrand.post([this,clientHandler](){
      auto itr = std::find(mListeners.begin(), mListeners.end(), clientHandler);
      if (itr != mListeners.end())
      {
         mListeners.erase(itr);
      }
   });
}

GroupChatHandler* GroupChatHandler::getRoom(uint32_t roomID)
{
   std::unordered_map<uint32_t, GroupChatPtr>::iterator itr = smGroupChats.find(roomID);
   if (itr != smGroupChats.end())
   {
      return itr->second.get();
   }
   
   return NULL;
}


void GroupChatHandler::init(asio::io_service &svc)
{
   smGroupChats[ROOM_GUIDE] = std::make_shared<GroupChatHandler>(svc);
   smGroupChats[ROOM_GUIDE]->startDispatch();
}

void GroupChatHandler::clear()
{
   smGroupChats.clear();
}

