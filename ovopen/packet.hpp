/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef packet_hpp
#define packet_hpp

#include <stdio.h>
#include <stdint.h>

#include <asio.hpp>
#include <atomic>
#include <iostream>
#include <unordered_map>
#include "blowfish.h"
#include "sha1.hpp"
#include "log.hpp"
#include "zlib.h"


#ifdef _MSC_VER 
//not #if defined(_WIN32) || defined(_WIN64) because we have strncasecmp in mingw
#define strncasecmp _strnicmp
#define strcasecmp _stricmp
#endif

#ifndef MSVC_STR_OVERRIDE
#if defined(_MSC_VER) && _MSC_VER < 1900

#define MSVC_STR_OVERRIDE
#include <stdio.h>
#include <stdarg.h>

#define snprintf c99_snprintf
#define vsnprintf c99_vsnprintf

__inline int c99_vsnprintf(char *outBuf, size_t size, const char *format, va_list ap)
{
	int count = -1;

	if (size != 0)
		count = _vsnprintf_s(outBuf, size, _TRUNCATE, format, ap);
	if (count == -1)
		count = _vscprintf(format, ap);

	return count;
}

__inline int c99_snprintf(char *outBuf, size_t size, const char *format, ...)
{
	int count;
	va_list ap;

	va_start(ap, format);
	count = c99_vsnprintf(outBuf, size, format, ap);
	va_end(ap);

	return count;
}

#endif

#define MSVC_STR_OVERRIDE
#endif

#if defined(_MSC_VER)
#include <stdlib.h>
#define OV_BYTESWAP64(value) _byteswap_uint64(value)
#define OV_BYTESWAP32(value) _byteswap_ulong(value)
#define OV_BYTESWAP16(value) _byteswap_ushort(value)
#else
#define OV_BYTESWAP64(value) __builtin_bswap64(value)
#define OV_BYTESWAP32(value) __builtin_bswap32(value)
#define OV_BYTESWAP16(value) __builtin_bswap16(value)
#endif

class OVPacketHeader
{
public:
   
   enum Flags
   {
      COMPRESSED=(1<<0),
      //
      ENCRYPTED=(1<<3),
      AUTO_HANDLER=(1<<5)
   };
   
   enum
   {
      NORMAL_CODE = 0x4f56,
      PROCESSED_CODE = 0x0056
   };
   
   enum
   {
      MaxPacketSize = 32000-6,
      MaxCompressedSize = MaxPacketSize*2,
   };
   
   uint16_t mCode;
   uint16_t mSize;
   uint16_t mId;
   
   inline uint16_t getNativeCode() const { return  OV_BYTESWAP16(mCode); }
   inline uint16_t getNativeSize() const  { return  OV_BYTESWAP16(mSize); }
   inline uint16_t getNativeId() const  { return  OV_BYTESWAP16(mId); }
   
   inline uint32_t getPacketHash() const
   {
      return ((uint32_t)getNativeId()) | ((uint32_t)isAuto() << 16) | 0x8000000;
   }
   
   
   inline void setBasicPacket() { mCode = OV_BYTESWAP16(0x4f56); };
   inline void setAutoPacket() { mCode = OV_BYTESWAP16(0x4f56 | AUTO_HANDLER); };
   inline void setNativeCode(uint16_t val) { mCode = OV_BYTESWAP16(val); }
   inline void setNativeSize(uint16_t val) { mSize = OV_BYTESWAP16(val);  }
   inline void setNativeId(uint16_t val) { mId = OV_BYTESWAP16(val);  }
   
   
   inline bool isValidUnprocessed() const  { return (getNativeCode() & 0xffd6) == 0x4f56; }
   inline bool isValid() const  { return (getNativeCode() & 0x00d6) == 0x0056; }
   inline bool isProcessed() const  { return (getNativeCode() & 0xff00) == 0x0000; }
   inline bool isEncrypted() const  { return (getNativeCode() & ENCRYPTED) != 0; }
   inline bool isEncryptedSize() const  { return (isEncrypted() || isDecrypted()); }
   inline bool isDecrypted() const  { return isProcessed() && ((getNativeCode() & ENCRYPTED) != 0); }
   inline bool isCompressed() const  { return (getNativeCode() & COMPRESSED) != 0; }
   inline bool isAuto() const  { return (getNativeCode() & AUTO_HANDLER) != 0; }
   inline uint32_t getPacketSize() const  { return isEncryptedSize() ? ((((uint32_t)getNativeSize()+4) + 0x7) & ~0x7) : getNativeSize(); }
   
   void setProcessed()
   {
      mCode &= OV_BYTESWAP16(0x00ff);
   }
   
   inline void setDecompressed()
   {
      mCode &= ~OV_BYTESWAP16(COMPRESSED);
   }
   
   inline void setEncrypted()
   {
      mCode |= OV_BYTESWAP16(ENCRYPTED);
   }
   
   inline void setCompressed()
   {
      mCode |= OV_BYTESWAP16(COMPRESSED);
   }
   
   uint8_t* getData() const  { return ((uint8_t*)this) + 6; }
};


class OVFreeableData
{
public:
   virtual ~OVFreeableData() {;}
   virtual void free() = 0;
   virtual uint8_t* ptr() = 0;
   virtual uint32_t size() = 0;
   
   // Returns a clone, usually using multiple user class
   virtual OVFreeableData* cloneMultiple() = 0;
   
   inline void debugString(const char *ctx="") {
      OVPacketHeader *header = (OVPacketHeader*)ptr();
      Log::printf(LogEntry::Debug, "%s: packet %u type %s", ctx, header->getNativeId(), header->isAuto() ? "auto" : "basic");
   }
};

class MemStream
{
public:

   uint32_t mPos;
   uint32_t mSize;
   uint32_t mAllocSize;
   uint8_t* mPtr;
   
   bool mGrow;
   bool mOwnPtr;
   
   MemStream(uint32_t sz, void* ptr, bool ownPtr=false) : mPos(0), mSize(sz), mAllocSize(sz), mPtr((uint8_t*)ptr), mGrow(false), mOwnPtr(ownPtr) {;}
   MemStream(MemStream &&other)
   {
      mPtr = other.mPtr;
      mPos = other.mPos;
      mSize = other.mSize;
      mAllocSize = other.mAllocSize;
      other.mOwnPtr = false;
      mOwnPtr = true;
      mGrow = other.mGrow;
   }
   MemStream(uint32_t sz)
   {
      mPtr = (uint8_t*)malloc(sz);
      mPos = 0;
      mSize = 0;
      mAllocSize = sz;
      mOwnPtr = true;
      mGrow = true;
   }
   MemStream(MemStream &other)
   {
      mPtr = other.mPtr;
      mPos = other.mPos;
      mSize = other.mSize;
      mAllocSize = other.mAllocSize;
      other.mOwnPtr = false;
      mOwnPtr = true;
   }
   MemStream& operator=(MemStream other)
   {
      mPtr = other.mPtr;
      mPos = other.mPos;
      mSize = other.mSize;
      mAllocSize = other.mAllocSize;
      other.mOwnPtr = false;
      mOwnPtr = true;
      return *this;
   }
   ~MemStream()
   {
      if(mOwnPtr)
         free(mPtr);
   }
   
   bool _read(uint32_t bytes, void* outPtr)
   {
      if (mPos+bytes > mSize)
         return false;
      
      memcpy(outPtr, mPtr+mPos, bytes);
      mPos += bytes;
      return true;
   }
   
   bool resize(uint32_t newSize)
   {
      uint32_t allocSize = newSize;
      if (allocSize % 0x4000 != 0)
         allocSize += (0x4000 - (allocSize % 0x4000));
      
      if (mGrow)
      {
         if (mOwnPtr)
         {
            mPtr = mAllocSize < allocSize ? (uint8_t*)realloc(mPtr, allocSize) : mPtr;
            mSize = newSize;
         }
         else
         {
            uint8_t* newPtr = (uint8_t*)malloc(allocSize);
            memcpy(newPtr, mPtr, mSize);
            mSize = newSize;
            mPtr = newPtr;
            mOwnPtr = true;
         }
         return true;
      }
      else if (mSize+newSize <= mAllocSize)
      {
         mSize += newSize;
         return true;
      }
      else
      {
         return false;
      }
   }
   
   bool _write(uint32_t bytes, const void* inPtr)
   {
      if (mPos+bytes > mSize)
      {
         uint32_t newSize = mPos+bytes;
         if (!resize(newSize))
            return false;
      }
      
      memcpy(mPtr+mPos, inPtr, bytes);
      mPos += bytes;
      return true;
   }
   
   bool isEOF()
   {
      return mPos >= mSize;
   }
   
   // For array types
   template<class T, int N> inline bool read( T (&value)[N] )
   {
      return _read(sizeof(T)*N, &value);
   }
   
   // For normal scalar types
   template<typename T> inline bool read(T &value)
   {
      return _read(sizeof(T), &value);
   }
   
   inline bool read(uint32_t size, void* data)
   {
      return _read(size, data);
   }
   
   // For array types
   template<class T, int N> inline bool write(const T (&value)[N] )
   {
      return _write(sizeof(T)*N, &value);
   }
   
   // For normal scalar types
   template<typename T> inline bool write(const T &value)
   {
      return _write(sizeof(T), &value);
   }
   
   inline bool write(uint32_t size, const void* data)
   {
      return _write(size, data);
   }
   
   inline void setPosition(uint32_t pos)
   {
      if (pos > mSize)
      {
         pos = mSize;
      }
      mPos = pos;
   }
   
   inline void growPosition(uint32_t pos)
   {
      if (pos > mSize)
      {
         resize(pos);
         if (pos > mSize)
            pos = mSize;
      }
      mPos = pos;
   }
   
   inline bool readString(char* out, uint16_t maxSize)
   {
      uint8_t code = 0;
      uint16_t len = 0;
      read(code);
      if (code != 0xEA)
      {
         out[0] = '\0';
         Log::printf(LogEntry::Debug, "Malformed string");
         return false;
      }
      
      readUintBE(len);
      uint16_t toRead = len > maxSize ? maxSize : len;
      read(toRead, out);
      if (len > maxSize)
      {
         setPosition(mPos + (len - maxSize));
      }
      out[toRead] = '\0';
      
      return true;
   }
   
   inline bool readString(std::string &outs)
   {
      uint8_t code = 0;
      uint16_t len = 0;
      read(code);
      if (code != 0xEA)
      {
         outs = "";
         Log::printf(LogEntry::Debug, "Malformed string");
         return false;
      }
      readUintBE(len);
      
      outs = std::string(len, 'x');
      return read(len, &outs[0]);
   }
   
   inline bool writeString(const char* out, uint16_t maxSize=0xFFFF)
   {
      uint16_t len = strlen(out);
      uint8_t code = 0xEA;
      write(code);
      writeUintBE(len);
      
      uint16_t toWrite = len > maxSize ? maxSize : len;
      return write(toWrite, out);
   }
   
   inline bool writeRawString(const char* str)
   {
      int len = strlen(str);
      return write(len, str);
   }
   
   inline bool skipString()
   {
      uint8_t code = 0;
      uint16_t len = 0;
      read(code);
      if (code != 0xEA)
      {
         Log::printf(LogEntry::Debug, "Malformed string");
         return false;
      }
      readUintBE(len);
      
      setPosition(mPos + len);
      return true;
   }
   
   inline bool readUintBE(uint16_t &num)
   {
      uint16_t outn = 0;
      read(outn);
      num = OV_BYTESWAP16(outn);
      return true;
   }
   
   inline bool readUintBE(uint32_t &num)
   {
      uint32_t outn = 0;
      read(outn);
      num = OV_BYTESWAP32(outn);
      return true;
   }
   
   inline bool readUintBE(uint64_t &num)
   {
      uint64_t outn = 0;
      read(outn);
	  num = OV_BYTESWAP64(outn);
      return true;
   }
   
   inline bool readIntBE(int16_t &num)
   {
      int16_t outn = 0;
      read(outn);
      num = OV_BYTESWAP16(outn);
      return true;
   }
   
   inline bool readIntBE(int32_t &num)
   {
      int32_t outn = 0;
      read(outn);
      num = OV_BYTESWAP32(outn);
      return true;
   }
   
   inline bool writeUintBE(uint16_t num)
   {
      uint16_t outn = OV_BYTESWAP16(num);
      return write(outn);
   }
   
   inline bool writeUintBE(uint32_t num)
   {
      uint32_t outn = OV_BYTESWAP32(num);
      return write(outn);
   }
   
   inline bool writeUintBE(uint64_t num)
   {
	  uint64_t outn = OV_BYTESWAP64(num);
      return write(outn);
   }
   
   inline bool writeIntBE(int16_t num)
   {
      int16_t outn = OV_BYTESWAP16(num);
      return write(outn);
   }
   
   inline bool writeIntBE(int32_t num)
   {
      int32_t outn = OV_BYTESWAP32(num);
      return write(outn);
   }
   
   inline bool writeIntBE(int64_t num)
   {
	  int64_t outn = OV_BYTESWAP64(num);
      return write(outn);
   }
   
   inline void writeHex(uint32_t size, const void* data)
   {
      const uint8_t* bytes = (uint8_t*)data;
      for (uint32_t i=0; i<size; i++)
      {
         char buf[2];
         buf[0] = "0123456789ABCDEF"[bytes[i] >> 4];
         buf[1] = "0123456789ABCDEF"[bytes[i] & 0x0F];
         _write(2, buf);
      }
   }
   
   bool readCPPString(std::string &outs)
   {
      uint16_t len = 0;
      readUintBE(len);
      
      outs = std::string(len, 'x');
      return read(len, &outs[0]);
   }
   
   bool writeCPPString(std::string &outs)
   {
      uint16_t len = outs.size();
      writeUintBE(len);
      return write(len, outs.c_str());
   }
   
   inline uint32_t getPosition() { return mPos; }
   inline uint8_t* getPositionPtr() { return mPtr + mPos; }
   inline uint32_t getStreamSize() { return mSize; }
};

class PacketMallocData : public OVFreeableData
{
public:
   uint8_t* mPtr;
   uint32_t mSize;
   
   PacketMallocData() : mPtr(NULL), mSize(0) {;}
   
   inline static PacketMallocData* createOfSize(uint32_t sz)
   {
      PacketMallocData* dat = new PacketMallocData();
      dat->mPtr = (uint8_t*)malloc(sz);
      dat->mSize = sz;
      return dat;
   }
   
   inline static PacketMallocData* createFromData(uint32_t sz, uint8_t* data)
   {
      PacketMallocData* dat = new PacketMallocData();
      dat->mPtr = (uint8_t*)malloc(sz);
      dat->mSize = sz;
      memcpy(dat->mPtr, data, sz);
      return dat;
   }
   
   OVFreeableData* cloneMultiple();
   
   void free()
   {
      if (mPtr) ::free(mPtr);
      delete this;
   }
   
   uint8_t* ptr()
   {
      return mPtr;
   }
   
   uint32_t size()
   {
      return mSize;
   }
};

class SharedMallocData : public std::enable_shared_from_this<SharedMallocData>
{
public:
   uint8_t* mPtr;
   uint32_t mSize;
   
   virtual ~SharedMallocData()
   {
      if (mPtr) ::free(mPtr);
   }
   
   inline static SharedMallocData* createOfSize(uint32_t sz)
   {
      SharedMallocData* dat = new SharedMallocData();
      dat->mPtr = (uint8_t*)malloc(sz);
      dat->mSize = sz;
      return dat;
   }
   
   inline static SharedMallocData* createFromData(uint32_t sz, uint8_t* data)
   {
      SharedMallocData* dat = new SharedMallocData();
      dat->mPtr = (uint8_t*)malloc(sz);
      dat->mSize = sz;
      memcpy(dat->mPtr, data, sz);
      return dat;
   }
   
   uint8_t* ptr()
   {
      return mPtr;
   }
   
   uint32_t size()
   {
      return mSize;
   }
   
   virtual void free()
   {
   }
};

template<uint32_t size> class StackMemStream : public MemStream
{
public:
   uint8_t data[size];
   
   StackMemStream() : MemStream(size, &data[0], false)
   {
      mSize = size;
      mPtr = &data[0];
      mPos = 0;
      mOwnPtr = false;
      mGrow = false;
   }
};

class PacketMultipleUserData : public OVFreeableData
{
public:
   std::shared_ptr<SharedMallocData> mData;
   
   PacketMultipleUserData() : mData(NULL) {;}
   
   inline static PacketMultipleUserData* createOfSize(uint32_t sz)
   {
      SharedMallocData* dat = SharedMallocData::createOfSize(sz);
      PacketMultipleUserData* dataHandle = new PacketMultipleUserData();
      dataHandle->mData = std::shared_ptr<SharedMallocData>(dat);
      return dataHandle;
   }
   
   inline static PacketMultipleUserData* createFromData(uint32_t sz, uint8_t* data)
   {
      SharedMallocData* dat = SharedMallocData::createFromData(sz, data);
      PacketMultipleUserData* dataHandle = new PacketMultipleUserData();
      dataHandle->mData = std::shared_ptr<SharedMallocData>(dat);
      return dataHandle;
   }
   
   OVFreeableData* cloneMultiple();
   
   void free()
   {
      mData = NULL;
      delete this;
   }
   
   uint8_t* ptr()
   {
      return mData->ptr();
   }
   
   uint32_t size()
   {
      return mData->size();
   }
   
};

// This is the abstracted version - handles cases of large compressed packets
struct OVPacketInfo
{
   uint16_t code;
   bool isAuto;
   bool isValid;
   
   uint32_t size;
   void* data;
   std::shared_ptr<SharedMallocData> freeData;
   
   OVPacketInfo() : code(0), isAuto(false), isValid(false), size(0), data(0), freeData(0) {;}
   ~OVPacketInfo() { freeData = NULL; }
   
   uint32_t getHashCode() { return ((uint32_t)code) | (((uint32_t)isAuto) << 16); }
   
   void ensureFreeable(); // Makes sure the data is freeable later (for long-running handlers)
   void cleanup();
};

// Decodes a packet, returning a new block of allocated data if required
// Returns false if the packet was in some way invalid
OVPacketInfo DecodePacket(OVPacketHeader* rawPacket, BLOWFISH_CONTEXT* encKey, bool allowDecompress);

bool PacketAlreadyDecrypted(OVPacketHeader* rawPacket, BLOWFISH_CONTEXT* encKey);
void EncryptPacketInPlace(OVPacketHeader* rawPacket, BLOWFISH_CONTEXT* encKey);
bool DecryptPacketInPlace(OVPacketHeader* rawPacket, BLOWFISH_CONTEXT* encKey);

// Encodes a packet according to the header requirements, creating a freeable block of data in the process.
// Returns NULL if it wasn't possible to create a packet.
OVFreeableData* EncodePacket(OVPacketHeader* rawPacket, BLOWFISH_CONTEXT* encKey, uint32_t dataSize, uint8_t* data, bool padRPC=false, bool padEnc=false);


// These codes are basic packet codes
enum InternalPacketCodes
{
   PacketError = 1,
   PacketPing = 2,
   PacketPong = 3,
   
   PacketRedirect = 5,
   
   PacketLogin1 = 0x66,
   PacketAuthSuccess = 0x67,
   
   PacketFriendList = 0xca,
   PacketFriendUpdate = 0xcb,
   PacketInstanceReady = 0xcc,
   PacketMasterVersion = 0xcd,
   PacketServerMessage = 0xcf,
   PacketServerAnnouncement = 0xd0,
   PacketPrivateMessage = 0xd1,
   PacketGuideReplyMessage = 0xd2,
   PacketCatChildren = 0x3ea,
   PacketResourceData = 0x3f0,
   
   AutoPacketRPCHandler = 10001,
   AutoPacketInstanceList = 201,
   
   // BACKEND
   AutoPacketBackendUserLoginQueued=500,
   AutoPacketBackendUserShouldBeKicked=501,
   AutoPacketBackendInventoryData=502,
   AutoPacketBackendHousePlot=503,
   AutoPacketUserInventoryId=504,
   AutoPacketBackendProcessManagement=505, // TODO: through ServerManagement?
   AutoPacketBackendServerInfo=506,
   AutoPacketUserBalance=507
};

// Wraps common packet building operations
class PacketBuilder
{
public:
   MemStream *mStream;
   uint16_t mRpcID;
   uint16_t mPacketID;
   bool mIsRPC;
   bool mIsAuto;
   bool mIsCompressed;
   bool mIsEncrypted;
   uint32_t mRequestID;
   uint32_t mOffset;
   
   PacketBuilder(MemStream *stream) : mStream(stream), mOffset(0), mIsCompressed(false), mIsEncrypted(false) {;}
   
   void beginBasicPacket(uint16_t type)
   {
      mPacketID = type;
      mIsAuto = false;
      mIsRPC = false;
      mStream->growPosition(mOffset+6);
   }
   
   void beginAutoPacket(uint16_t type)
   {
      mPacketID = type;
      mIsAuto = true;
      mIsRPC = false;
      mStream->growPosition(mOffset+6);
   }
   
   void beginRPC(uint16_t rpcID, uint32_t requestID, uint16_t packetID, bool isAuto=false)
   {
      mIsAuto = isAuto;
      mIsRPC = true;
      mRpcID = rpcID;
      mPacketID = packetID;
      mRequestID = requestID;
      mStream->growPosition(mOffset+6+6+6);
   }
   
   bool commit()
   {
      OVPacketHeader header;
      uint32_t total_size = mStream->getPosition() - mOffset;
      if (mIsRPC)
      {
         // We have 6+6+6 bytes of data before
         mStream->setPosition(0);
         header.setAutoPacket();
         header.setNativeId(AutoPacketRPCHandler);
         header.setNativeSize(total_size - 6);
         mStream->write(sizeof(header), &header);
         
         mStream->writeUintBE(mRpcID);
         mStream->writeUintBE(mRequestID);
         
         if (mIsAuto)
            header.setAutoPacket();
         else
            header.setBasicPacket();
         header.setNativeId(mPacketID);
         header.setNativeSize(total_size - (6 + 6 + 6));
         mStream->write(sizeof(header), &header);
      }
      else
      {
         // We have 6 bytes of data before
         if (mIsAuto)
            header.setAutoPacket();
         else
            header.setBasicPacket();
         header.setNativeId(mPacketID);
         header.setNativeSize(total_size - 6);
         mStream->setPosition(mOffset);
         mStream->write(sizeof(header), &header);
      }
      mStream->setPosition(total_size + mOffset);
      
#ifdef DEBUG
      assert(mStream->mPos == (total_size+mOffset));
#endif
      
      return (mStream->mPos == (total_size+mOffset));
   }

   OVFreeableData* commitToEncodedPacket(BLOWFISH_CONTEXT* encKey)
   {
      if (!commit())
         return NULL;

      OVPacketHeader* rootPacket = NULL;
      uint32_t baseSize = 0;
      
      if (mIsRPC)
      {
         rootPacket = (OVPacketHeader*)(mStream->mPtr + mOffset + 6 + 6);
         baseSize = rootPacket->getPacketSize();

         if (mIsCompressed || baseSize > (OVPacketHeader::MaxPacketSize/2))
         {
            // Make sure we compress it
            rootPacket->setCompressed();
         }
      }
      else
      {
         rootPacket = (OVPacketHeader*)(mStream->mPtr + mOffset);
         baseSize = rootPacket->getPacketSize();

         if (mIsCompressed || baseSize > (OVPacketHeader::MaxPacketSize/2))
         {
            // Make sure we compress it
            rootPacket->setCompressed();
         }
         
         if (mIsEncrypted)
         {
            rootPacket->setEncrypted();
         }
      }
      
      OVFreeableData* outData = EncodePacket(rootPacket, 
                                             encKey,
                                             rootPacket->getNativeSize(),
                                             ((uint8_t*)rootPacket) + 6,
                                             mIsRPC,
                                             mIsEncrypted);
      
      // Copy RPC data over encoded packet
      // Encrypt the whole RPC data packet if required.
      if (mIsRPC)
      {
         OVPacketHeader* innerPacket = (OVPacketHeader*)(outData->ptr() + 6 + 6);
         baseSize = innerPacket->getPacketSize();
         memcpy(outData->ptr(), mStream->mPtr + mOffset, 6+6);
         rootPacket = (OVPacketHeader*)(outData->ptr());
         rootPacket->setNativeSize(baseSize + 6 + 6);
         
         //printf("Inner packet size pre-enc=%u\n", baseSize);
         
         if (mIsEncrypted)
         {
            rootPacket->setEncrypted();
            EncryptPacketInPlace(rootPacket, encKey);
         }
      }
      
      return outData;
   }

   void setCompressed(bool comp)
   {
      mIsCompressed = comp;
   }

   void setEncrypted(bool enc)
   {
      mIsEncrypted = enc;
   }
};


#endif /* packet_hpp */
