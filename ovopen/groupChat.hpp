/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef groupChat_hpp
#define groupChat_hpp

#include "packet.hpp"
#include "packetHandlers.hpp"
#include "protocolHandler.hpp"


class GroupChatHandler : public std::enable_shared_from_this<GroupChatHandler>
{
public:
   
   enum
   {
      ROOM_GUIDE = 1
   };
   
   asio::io_service::strand mStrand;
   OVRuntimeTimerType mDispatchTimer;
   
   std::atomic<int32_t> mPendingOperations;
   std::vector<OVFreeableData*> mMessagesToSend;
   std::vector<std::shared_ptr<OVProtocolHandler>> mListeners;
   
   GroupChatHandler(asio::io_service &svc);
   
   void startDispatch();
   
   void queueMessage(OVFreeableData* data);
   void dispatchMessageExceptToUser(PacketMultipleUserData* data, uint32_t exceptID);
   
   void dispatchQueuedMessages(const std::error_code &ec);
   
   void addListener(OVProtocolHandler &handler);
   void removeListener(OVProtocolHandler &handler);
   
   static GroupChatHandler* getRoom(uint32_t roomID);
   static void init(asio::io_service &svc);

   static void clear();
};

typedef std::shared_ptr<GroupChatHandler> GroupChatPtr;

#endif /* groupChat_hpp */
