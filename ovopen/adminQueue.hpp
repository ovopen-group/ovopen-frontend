/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef adminQueue_hpp
#define adminQueue_hpp

#include <asio.hpp>
#include <atomic>
#include <iostream>
#include <unordered_map>
#include "log.hpp"

#include "packetHandlers.hpp"

// Queue to serialize admin operations
class AdminQueryQueue
{
public:
   asio::io_service mIOService;
   asio::io_service::strand mStrand;
   asio::io_service::strand mMainStrand;
   
   std::atomic<int32_t> mPendingOperations;
   
   AdminQueryQueue(asio::io_service &svc) : mStrand(mIOService), mMainStrand(svc)
   {
      mPendingOperations = 0;
   }
   
   void onAdvanceQueue()
   {
      //Log::printf("AdminQueryQueue: advance");
      if (mPendingOperations == 0)
      {
         //Log::printf("  run_one");
         if (mIOService.stopped())
         {
            mIOService.restart();
         }
         mIOService.run_one();
      }
      else
      {
         Log::printf(LogEntry::Debug, "AdminQueryQueue: busy");
      }
   }
   
   template<typename T> void addToQueue(PacketHandler* handler, T resultHandler)
   {
      mStrand.post([this, resultHandler]() {
         Log::printf(LogEntry::Debug, "AdminQueryQueue: executing operation");
         
         mPendingOperations++;
         resultHandler();
         mPendingOperations--;
         
         // Handle next op
         if (mPendingOperations == 0)
         {
            Log::printf(LogEntry::Debug, "AdminQueryQueue: ready to handle next op (resultHandler should have been called)");
            mMainStrand.post(std::bind(&AdminQueryQueue::onAdvanceQueue, this));
         }
      });
      
      if (mPendingOperations == 0)
      {
         Log::printf(LogEntry::Debug, "No pending ops at the moment, posting advance");
         mMainStrand.post(std::bind(&AdminQueryQueue::onAdvanceQueue, this));
      }
   }
   
   template<typename T> void addToQueue(RPCPacketHandler* handler, T resultHandler)
   {
      mStrand.post([this, resultHandler]() {
         Log::printf(LogEntry::Debug, "AdminQueryQueue: executing operation");
         
         mPendingOperations++;
         resultHandler();
         mPendingOperations--;
         
         // Handle next op
         if (mPendingOperations == 0)
         {
            Log::printf(LogEntry::Debug, "AdminQueryQueue: ready to handle next op (resultHandler should have been called)");
            mMainStrand.post(std::bind(&AdminQueryQueue::onAdvanceQueue, this));
         }
      });
      
      if (mPendingOperations == 0)
      {
         Log::printf(LogEntry::Debug, "No pending ops at the moment, posting advance");
         mMainStrand.post(std::bind(&AdminQueryQueue::onAdvanceQueue, this));
      }
   }
   
   
   static AdminQueryQueue* sAdminQueryQueue;
   
   static AdminQueryQueue& getQueue()
   {
      return *sAdminQueryQueue;
   }
};


#endif /* adminQueue_hpp */
