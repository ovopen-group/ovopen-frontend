/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "packetHandlers.hpp"
#include "packet.hpp"
#include "protocolHandler.hpp"
#include "friendController.hpp"
#include "adminQueue.hpp"
#include "groupChat.hpp"
#include <unordered_map>
#include "userConnectionController.hpp"
#include "backendConnectionController.hpp"
#include "LUrlParser.hpp"

class RPC_ClientRequestMirrors : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(421, OVProtocolHandler::PROTOCOL_PATCHER);
   
   enum
   {
      ReturnCode_Error = -1,
      ReturnCode_OK = 0
   };
   
   enum
   {
      Packet_RequestMirrors=0x1,
      Packet_UpdateMirror=0x2
   };
   
   void doSendMirror(OVProtocolHandler& client, const RPCTicket* requestTicket, uint16_t ident, std::string& name, std::string& hostname, std::string& path)
   {
      uint32_t packet_len = 6 + 6 + 6 + (4 + 2 + 1 + (3*3) + name.size() + hostname.size() + path.size());
      
      MemStream packetStream(packet_len);
      PacketBuilder packet(&packetStream);
      packet.beginRPC(getRPCID(), requestTicket->getRequestID(), Packet_UpdateMirror, false);

      packetStream.writeUintBE((uint32_t)0xDEADB33F);  // doesn't appear to be used
      packetStream.writeUintBE(ident); // index of mirror?
      packetStream.write((char)0); // unused
      packetStream.writeString(name.c_str());
      packetStream.writeString(hostname.c_str()); // hostname
      packetStream.writeString(path.c_str()); // root path
      
      packet.commit();
      
      PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
      client.queuePacketData(responseData);
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      // Basically, the server should keep sending Packet_UpdateMirror packets wrapped up, then to finish
      // packet 0.
      if (info.code == Packet_RequestMirrors)
      {
         PacketMallocData* responseData = NULL;
         std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
         
         // NOTE: should be only 1 patch listed
         DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT id, name, hostname, path FROM patch_mirrors ORDER BY id ASC;"), false,  [this, clientHandler, requestTicket](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
            if (ec)
            {
               endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, ReturnCode_Error);
            }
            else
            {
               uint16_t count = 0;
               
               for (auto row : results)
               {
                  std::string name = row.value[1];
                  std::string hostname = row.value[2];
                  std::string path = row.value[3];
                  doSendMirror(*(clientHandler.get()), requestTicket.get(), count++, name, hostname, path);
               }
               
               endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, ReturnCode_OK);
            }
         });
      }
      else
      {
         endRPCResponseWithTicket(client, requestTicket, 0, ReturnCode_Error);
      }
      
      info.cleanup();
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_ClientRequestMirrors);


class RPC_ClientRequestExecutable : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(422, OVProtocolHandler::PROTOCOL_PATCHER);
   
   enum
   {
      ReturnCode_Error = -1,
      ReturnCode_OK = 0,
      ReturnCode_NoUpdate = 1
   };
   
   enum
   {
      Packet_RequestInfo=0x1,
      Packet_ExecutableInfo=0x2
   };
   
   void doSendPatchExecutableInfo(OVProtocolHandler& client, const RPCTicket* requestTicket, uint32_t crc, uint32_t size, std::string& path, std::string& exe)
   {
      uint32_t packet_len = 6 + 6 + 6 + (8 + 6 + path.size() + exe.size());
      
      MemStream packetStream(packet_len);
      PacketBuilder packet(&packetStream);
      packet.beginRPC(getRPCID(), requestTicket->getRequestID(), Packet_ExecutableInfo, false);
      
      packetStream.writeUintBE(size); // size?
      packetStream.writeUintBE(crc); // CRC
      packetStream.writeString(path.c_str()); // path on server
      packetStream.writeString(exe.c_str()); /// EXE name
      
      packet.commit();
      
      PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
      client.queuePacketData(responseData);
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      if (info.code == Packet_RequestInfo)
      {
         char bundleName[65];
         char bundleVersion[65];
         stream.readString(&bundleName[0], 64);     // os (osx, win32)
         stream.readString(&bundleVersion[0], 64);  // version (stored internally in patcher)
         
         Log::client_printf(client, LogEntry::General, "Client requested patcher for %s > version %s\n", bundleName, bundleVersion);

         // NOTE: should be only 1 patch listed
         DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT crc, size, path, filename FROM patches WHERE bundle_name=$1 AND source_bundle_version = $2 LIMIT 1;", bundleName, bundleVersion), false, [this, clientHandler, requestTicket](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
            if (ec)
            {
               endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, ReturnCode_NoUpdate);
            }
            else
            {
               for (auto row : results)
               {
                  std::string path = row.value[2];
                  std::string exe = row.value[3];
                  doSendPatchExecutableInfo(*(clientHandler.get()), requestTicket.get(), DBToU32(row.value[0]),  DBToU32(row.value[1]), path, exe);
               }
               
               endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, results.size() == 0 ? ReturnCode_NoUpdate : ReturnCode_OK);
            }
         });
      }
      else
      {
         endRPCResponseWithTicket(client, requestTicket, 0, ReturnCode_Error);
      }
      
      info.cleanup();
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_ClientRequestExecutable);


class RPC_ClientRequestUpdate : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(420, OVProtocolHandler::PROTOCOL_PATCHER);
   
   enum
   {
      ReturnCode_Error = -1,
      ReturnCode_OK = 0
   };
   
   enum
   {
      Packet_RequestInfo=0x1,
      Packet_PatchList=0x2
   };
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      
      // Basically, the server should keep sending Packet_PatchList packets wrapped up, then to finish
      // packet 0.
      if (info.code == Packet_RequestInfo)
      {
          std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
          std::array<char, 65> bundleName;
         char bundleVersion[65];
         stream.readString(&bundleName[0], 64);
         stream.readString(&bundleVersion[0], 64);
         
         /*
          Basically: get a set of bundles which will take us from the supplied bundle to the current version.
          
          bundle_version=NEW VERSION
          source_bundle_version=CURRENT VERSION ON CLIENT or '' if full patch
          stage_bundle_version=BASIS VERSION or '' if full patch
          
          Example scenarios:
          
          CLIENT: source_bundle_version=""
          SERVER: list full patch [only got "" in reply]
          
          CLIENT: source_bundle_version="known"
          SERVER: list delta patches [only got "" in reply]
          
          CLIENT: source_bundle_version="unknown"
          SERVER: list full patch [only got "" in reply]
          
          */
         
         Log::client_printf(client, LogEntry::General, "Client requested update for %s version %s\n", bundleName, bundleVersion);
     
         DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT crc, size, path, filename, bundle_name, bundle_version, stage_bundle_version, source_bundle_version FROM patches WHERE bundle_name = $1 AND (source_bundle_version=$2 OR source_bundle_version='') ORDER BY source_bundle_version,order_id ASC;", &bundleName[0], bundleVersion), false, [this, clientHandler, requestTicket, bundleName](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
         
            // Remove source_bundle_version='' from results if we have more than 1 result
            if (results.size() > 1)
            {
               for (uint32_t i=0; i<results.size(); i++)
               {
                  if (results[i].value[7][0] == '\0') // source_bundle_version
                  {
                     results.erase(results.begin()+i);
                     break;
                  }
               }
            }
            
            uint16_t chain_count = (uint16_t)results.size();
            
            MemStream packetStream(8192);
            PacketBuilder packet(&packetStream);
            packet.beginRPC(getRPCID(), requestTicket->getRequestID(), Packet_PatchList, false);
            
            packetStream.writeUintBE(chain_count);
            packetStream.writeString(&bundleName[0]); // bundle name
            
            // NOTE: Files are stored to "patcherTemp"
            for (auto rowv : results)
            {
               auto row = rowv.value;
               
               std::string path = row[2];
               std::string exe = row[3];
               std::string bundle_version = row[5];
               std::string stage_bundle_version = row[6];
               uint32_t crc = DBToU32(row[0]);
               uint32_t size = DBToU32(row[1]);
               
               packetStream.writeString(exe.c_str()); // last part of URL. If this is blank, the patch will not download. Presumably the filename of the patch.
               packetStream.writeString(path.c_str());         // Extra path appended onto rootPath. If this is blank, a "/" will be placed in the URL instead
               packetStream.writeString(stage_bundle_version.c_str()); // Bundle we are updating from
               packetStream.writeString(bundle_version.c_str());     // This gets placed in the URL if the blankable value is not blank NOTE: this is also used as the new bundle version in patch.dat!!
               
               // To sum up: two possible urls:
               // http://<server>/<rootPath><string4>/<string1> (if string2 is blank)
               // http://<server>/<rootPath><string2><string1>  (if string2 is not blank)
               
               packetStream.writeUintBE(size); //
               packetStream.writeUintBE(crc); //
            }
            
            packet.commit();
            
            PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
            clientHandler->queuePacketData(responseData);
            
            endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, ReturnCode_OK);
         });
      }
      
      info.cleanup();
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_ClientRequestUpdate);


class RPC_GetChannelPatchBundleSet : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(423, OVProtocolHandler::PROTOCOL_PATCHER);
   
   enum
   {
      RESPONSE_OK = 0,
      RESPONSE_ERR = -2,
      RESPONSE_INVALID_CHANNEL = -3
   };
   
   enum
   {
      Request_ChannelIdentifier=1,
      Response_ChannelPatch=2
   };
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      if (info.code != Request_ChannelIdentifier)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
         return;
      }
      
      char buf2[1024];
      char channelName[65];
      
      stream.readString(&channelName[0], 64);     // e.g. linux-server
      channelName[64] = '\0';
      
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT name,data FROM patch_channels WHERE name = $1 LIMIT 1;\n",
      channelName), false, [this, clientHandler, requestTicket](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
         
         if (ec || results.size() == 0)
         {
            endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_INVALID_CHANNEL);
         }
         else
         {
            auto row = results[0];
            MemStream packetStream(4096);
            PacketBuilder packet(&packetStream);
            packet.beginRPC(getRPCID(), requestTicket->getRequestID(), Response_ChannelPatch, false);
            
            packetStream.writeString(row.value[0]);
            packetStream.write(row.lengths[1], row.value[1]);
            
            packet.commit();
            
            PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
            clientHandler->queuePacketData(responseData);
            
            endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_OK);
         }
      });
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_GetChannelPatchBundleSet);
