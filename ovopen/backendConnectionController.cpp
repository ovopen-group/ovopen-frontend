/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "backendConnectionController.hpp"
#include "dbQueue.hpp"
#include <algorithm>

// This is simply a QUICK lookup which skips the DB
std::unordered_map<uint64_t, std::shared_ptr<OVProtocolHandler>> gLocationRegister;
std::unordered_map<uint32_t, std::shared_ptr<OVProtocolHandler>> gLocationSocketLookup;
std::mutex gLocationLock;

static uint64_t calcLocationHash(uint32_t locationID, uint32_t instanceID)
{
   return locationID | (instanceID << 16);
}

std::shared_ptr<OVProtocolHandler> BackendConnectionController::getInstance(uint32_t locationID, uint32_t instanceID)
{
   std::lock_guard<std::mutex> lock(gLocationLock);
   uint64_t key = calcLocationHash(locationID, instanceID);
   
   auto value = gLocationRegister.find(key);
   if (value == gLocationRegister.end())
   {
      return std::shared_ptr<OVProtocolHandler>();
   }
   else
   {
      return value->second;
   }
}

std::shared_ptr<OVProtocolHandler> BackendConnectionController::getBySocketID(uint32_t socketID)
{
   std::lock_guard<std::mutex> lock(gLocationLock);
   
   auto value = gLocationSocketLookup.find(socketID);
   if (value == gLocationSocketLookup.end())
   {
      return std::shared_ptr<OVProtocolHandler>();
   }
   else
   {
      return value->second;
   }
}

void BackendConnectionController::registerConnection(std::shared_ptr<OVProtocolHandler> connection)
{
   std::lock_guard<std::mutex> lock(gLocationLock);
   
   OVBackendGameConnectionState* state = dynamic_cast<OVBackendGameConnectionState*>(connection->mState);
   if (!state)
      return;
   
   gLocationSocketLookup[connection->mSocketID] = connection;
}

void BackendConnectionController::registerLocation(std::shared_ptr<OVProtocolHandler> connection)
{
   std::lock_guard<std::mutex> lock(gLocationLock);
   
   OVBackendGameConnectionState* state = dynamic_cast<OVBackendGameConnectionState*>(connection->mState);
   if (!state || state->mLocationID < 0)
      return;
   
   uint64_t key = calcLocationHash(state->mLocationID, state->mInstanceID);
   gLocationRegister[key] = connection;
   
   // Update current socket ID in DB
   DBQueryQueue::getQueue().addToQueueAffected(connection->getStrandRef(), SQLBindPack("UPDATE location_instances SET socket_id = $1, permissions = $2 WHERE id = $3 AND owner_id = $4", (int64_t)connection->mSocketID, (int64_t)0, (int64_t)state->mInternalID, (int64_t)connection->mUid), &DBQueryQueue::defaultBasicHandler);
}

void BackendConnectionController::markLocationReady(std::shared_ptr<OVProtocolHandler> connection)
{
   OVBackendGameConnectionState* state = dynamic_cast<OVBackendGameConnectionState*>(connection->mState);
   if (!state)
      return;
   
   uint64_t key = calcLocationHash(state->mLocationID, state->mInstanceID);
   gLocationRegister[key] = connection;
   gLocationSocketLookup[connection->mSocketID] = connection;
   
   // Update current socket ID in DB
   DBQueryQueue::getQueue().addToQueueAffected(connection->getStrandRef(), SQLBindPack("UPDATE location_instances SET permissions = $1 WHERE id = $2 AND owner_id = $3",
   (int64_t)state->mAcceptPermissions, (int64_t)state->mInternalID, (int64_t)connection->mUid), &DBQueryQueue::defaultBasicHandler);
}

void BackendConnectionController::unregisterConnection(std::shared_ptr<OVProtocolHandler> connection)
{
   std::lock_guard<std::mutex> lock(gLocationLock);
   
   OVBackendGameConnectionState* state = dynamic_cast<OVBackendGameConnectionState*>(connection->mState);
   if (!state || state->mLocationID < 0)
      return;
   
   uint64_t key = calcLocationHash(state->mLocationID, state->mInstanceID);
   gLocationRegister.erase(key);
   gLocationSocketLookup.erase(connection->mSocketID);
   
   DBQueryQueue::getQueue().addToQueueAffected(connection->getStrandRef(), SQLBindPack("UPDATE location_instances SET socket_id = NULL, permissions = 0 WHERE id = $1 AND owner_id = $2",
   (int64_t)state->mInternalID, (int64_t)connection->mUid), &DBQueryQueue::defaultBasicHandler);
}

void BackendConnectionController::notifyLogout(std::shared_ptr<OVProtocolHandler> connection)
{
   OVServerGameConnectionState* clientState = dynamic_cast<OVServerGameConnectionState*>(connection->mState);
   if (clientState)
   {
      uint32_t uid = connection->mUid;
      
      // Kick client from previous location
      // TODO: consolidate with method in RPC handler?
      if (clientState->mLastInstanceID != -1)
      {
         std::shared_ptr<OVProtocolHandler> prevBackend = BackendConnectionController::getInstance(clientState->mLastLocationID, clientState->mLastInstanceID);
         
         if (prevBackend)
         {
            OVBackendGameConnectionState* backendState = dynamic_cast<OVBackendGameConnectionState*>(prevBackend->mState);
            
            StackMemStream<6+4> packetStream;
            PacketBuilder packet(&packetStream);
            packet.beginAutoPacket(AutoPacketBackendUserShouldBeKicked);
            
            packetStream.writeUintBE(uid);
            
            packet.commit();
            
            prevBackend->queuePacketData(PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr));
            prevBackend->mStrand.post([backendState, uid]{
               backendState->eraseUID(uid);
            });
         }
         
         DBQueryQueue::getQueue().addToQueueAffected(connection->getStrandRef(), SQLBindPack("UPDATE users SET last_instance_id = -1 WHERE id = $1;", (int64_t)uid), &DBQueryQueue::defaultBasicHandler);
      }
   }
}

// NOTE: this should be executed within the connections strand
void BackendConnectionController::expendReadyQueue(std::shared_ptr<OVProtocolHandler> handler, uint8_t acceptState)
{
   // Expend queue
   OVBackendGameConnectionState* state = dynamic_cast<OVBackendGameConnectionState*>(handler->mState);
   if (!state)
      return;
   
   state->mLastAcceptState = acceptState;
   state->onAdvanceLocationReadyQueue();
}

//

void OVBackendGameConnectionState::onAdvanceLocationReadyQueue()
{
   if (mPendingLocationReadyRequests != 0)
   {
      if (mLocationReadyQueueService.stopped())
      {
         mLocationReadyQueueService.restart();
      }
      mLocationReadyQueueService.run_one();
   }
}

void OVBackendGameConnectionState::insertUID(uint32_t uid)
{
   std::vector<uint32_t>::iterator position = std::find(mConnectedUIDs.begin(), mConnectedUIDs.end(), uid);
   if (position == mConnectedUIDs.end())
   {
      mConnectedUIDs.push_back(uid);
   }
}

void OVBackendGameConnectionState::eraseUID(uint32_t uid)
{
   std::vector<uint32_t>::iterator position = std::find(mConnectedUIDs.begin(), mConnectedUIDs.end(), uid);
   if (position != mConnectedUIDs.end())
      mConnectedUIDs.erase(position);
}

