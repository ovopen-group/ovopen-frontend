/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "packetHandlers.hpp"
#include "packet.hpp"
#include "protocolHandler.hpp"
#include "friendController.hpp"
#include "adminQueue.hpp"
#include "groupChat.hpp"
#include <unordered_map>
#include "userConnectionController.hpp"
#include "backendConnectionController.hpp"
#include "LUrlParser.hpp"
#include "log.hpp"
#include "coreUtil.hpp"
#include <time.h>
#include "jansson.h"

class RPC_ResolveURLHandler : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(1, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   enum
   {
      ReturnCode_TeleportDenied=0,
      ReturnCode_UserNoHouse=1,
      ReturnCode_UserNoLoc=2,
      ReturnCode_UserOffline=3,
      ReturnCode_InvalidUsername=4,
      ReturnCode_URLError=5,
      ReturnCode_UnknownInstance=6,
      ReturnCode_UnknownLocation=7,
      ReturnCode_BadURL=8,
      ReturnCode_InvalidURL=9,
      ReturnCode_OK=10,
      RESPONSE_ERR = -1
   };
   
   enum
   {
      Query_ResolveURL=0x1,
      Query_GetInstances=0x2,
      Response_ResolvedLocation=0x3
   };
   
   /*
    Paths are usually of the form:
    
    onverse://location/hub/Hub 2/tutorial"
    
    Where "hub" is the location name, "Hub 2" is the instance name, and "tutorial" is the poi name
    */
   void splitLocationPath(std::vector<std::string> &components, std::string &location_name, std::string &instance_name, std::string &poi_name)
   {
      location_name = "trials_of_the_ancients";
      
      location_name = components.size() > 0 ? components[0] : "";
      instance_name = components.size() > 1 ? components[1] : "";
      poi_name = components.size() > 2 ? components[2] : "";
   }
   
   void dispatchLocationResolvedPacket(OVProtocolHandler& client, RPCTicketPtr requestTicket, int32_t location_id, int32_t instance_id, const std::string &location_name, const std::string &instance_name, const std::string &poi_name)
   {
      MemStream packetStream(4096);
      PacketBuilder packet(&packetStream);
      packet.beginRPC(getRPCID(), requestTicket->getRequestID(), Response_ResolvedLocation, false);
      packetStream.writeIntBE(location_id);
      packetStream.writeIntBE(instance_id); // -1 = show map. Otherwise load instance
      
      packetStream.writeString(location_name.c_str());
      packetStream.writeString(instance_name.c_str());
      packetStream.writeString(poi_name.c_str());
      packet.commit();
      
      PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
      client.queuePacketData(responseData);
      
      endRPCResponseWithTicket(client, requestTicket, 0, ReturnCode_OK);
   }
   
   void handleLocationURL(OVProtocolHandler& client, std::vector<std::string> &components, RPCTicketPtr requestTicket)
   {
      MemStream packetStream(4096);
      OVPacketHeader header;
      uint32_t save_pos = 0;
      
      std::string location_name, instance_name, poi_name;
      
      splitLocationPath(components, location_name, instance_name, poi_name);
      
      if (location_name.size() == 0)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, ReturnCode_BadURL);
      }
      else
      {
         // Lookup location in DB
         // Two variants: <location>, <location> <instance id>
         bool wantsInstance = false;
         std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();

         SQLPack* query = NULL;
         
         if (instance_name.size() > 0)
         {
            wantsInstance = true;
            
            query = SQLBindPack(
                     "SELECT location_id, instance_id, socket_id, locations.name, location_instances.name "
                     "FROM location_instances "
                     "LEFT JOIN locations ON locations.id = location_instances.location_id "
                     "WHERE (permissions & $1 != 0) AND locations.name = $2 AND location_instances.name = $3 LIMIT 1;", (int64_t)client.mPermissionsMask.code, location_name.c_str(), instance_name.c_str());
         }
         else
         {
            query = SQLBindPack(
                     "SELECT location_id, instance_id, socket_id, locations.name, location_instances.name "
                     "FROM location_instances "
                     "LEFT JOIN locations ON locations.id = location_instances.location_id "
                     "WHERE (permissions & $1 != 0) AND locations.name = $2 LIMIT 1;", (int64_t)client.mPermissionsMask.code, location_name.c_str());
         }
         DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), query, false, [this, clientHandler, requestTicket, poi_name, wantsInstance](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
            
            if (results.size() > 0)
            {
               auto row = results[0].value;
               int32_t location_id = DBToS32(row[0]);
               int32_t instance_id = !wantsInstance ? -1 : DBToS32(row[1]);
               int64_t socket_id = row[2] == NULL ? -1 : DBToS64(row[2]);
               std::string location_name = row[3];
               std::string instance_name = !wantsInstance ? "" : row[4];
               
               if (socket_id >= 0)
               {
                  dispatchLocationResolvedPacket(*(clientHandler.get()), requestTicket, location_id, instance_id, location_name, instance_name, poi_name);
               }
               else
               {
                  endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, ReturnCode_UnknownInstance);
               }
            }
            else
            {
               endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, ReturnCode_UnknownInstance);
            }
         });
      }
      
      return;
   }
   
   void handlePlayerURL(OVProtocolHandler& client, std::vector<std::string> &components, RPCTicketPtr requestTicket)
   {
      // Can alternatively be grabbed via: `SELECT * FROM location_instances LEFT JOIN users ON users.last_instance_id = location_instances.id  WHERE users.id = 1 LIMIT 1;`
      const char* name = components.size() > 1 ? components[1].c_str() : "";
      std::shared_ptr<OVProtocolHandler> playerConnection = UserConnectionController::resolveUsernameConnection(name);
      if (!playerConnection)
      {
         endRPCResponseWithTicket(client, requestTicket, 0, ReturnCode_OK);
      }
      else
      {
         OVServerGameConnectionState* state = dynamic_cast<OVServerGameConnectionState*>(playerConnection->mState);
         if (state && state->mLastLocationID >= 0 && state->mLastInstanceID >= -1)
         {
            dispatchLocationResolvedPacket(client, requestTicket, state->mLastLocationID, state->mLastInstanceID, "", "", "");
            return;
         }
         
         endRPCResponseWithTicket(client, requestTicket, 0, ReturnCode_UserNoLoc);
         return;
      }
      
      endRPCResponseWithTicket(client, requestTicket, 0, ReturnCode_UserOffline);
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      // There are two types of query:
      // 0x1: resolveURL (string url)
      // 0x2: getInstances (uint locationId)
      // The packet wrapped in the query will be of id 1 or 2.
      
      // Should return either packet 0x3 or autopacket 201 or normal return code
      // autopacket(rpc)
      // uint16 1
      // uint32 requestId
      // response packet 201
      
      // returns error code -2 if aborted
      
      {
         uint16_t queryCode = info.code;
         
         if (queryCode == Query_ResolveURL) // get instances
         {
            std::string query_name;
            stream.readString(query_name);
            Log::client_printf(client, LogEntry::General, "Querying location %s\n", query_name.c_str());
            
            LUrlParser::clParseURL URL = LUrlParser::clParseURL::ParseURL( query_name );
            
            // Make sure we have the right URL type
            if (strcasecmp(URL.m_Scheme.c_str(), "onverse") == 0)
            {
               std::vector<std::string> components;

               StringUtil::StringSplitter split;
               split.scan(URL.m_Path.c_str(), "/");
               components = split.toList<1024>();

               if (strcasecmp(URL.m_Host.c_str(), "location") == 0)
               {
                  handleLocationURL(client, components, requestTicket);
                  return;
               }
               else if (strcasecmp(URL.m_Host.c_str(), "player") == 0)
               {
                  handlePlayerURL(client, components, requestTicket);
                  return;
               }
               else
               {
                  endRPCResponseWithTicket(client, requestTicket, 0, ReturnCode_BadURL);
                  return;
               }
            }
            
            endRPCResponseWithTicket(client, requestTicket, 0, ReturnCode_URLError);
            return;
         }
         else if (queryCode == Query_GetInstances)
         {
            // response should be
            // auto packet 202
            //   uint32 locationId
            //   uint32 numInstances
            //   for each numInstances while packet has more than 0xf bytes:
            //     uint32 id
            //     uint32 numUsers
            //     uint32 numHomes
            //     uint32 numFriends
            //     string name
            
            int32_t request_locationid = 0;
            MemStream inputStream(info.size, info.data);
            inputStream.readIntBE(request_locationid);
            
            // NOW we can grab the instances
            std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
            
            DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT socket_id FROM location_instances WHERE location_id = $1 AND (permissions & $2) != 0 AND socket_id IS NOT NULL", (int64_t)request_locationid, (int64_t)client.mPermissionsMask.code), false, [this, clientHandler, requestTicket, request_locationid](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
               
               
               if (ec)
               {
                  endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, ReturnCode_URLError);
                  return;
               }
               
               MemStream packetStream(4096);
               PacketBuilder packet(&packetStream);
               packet.beginRPC(getRPCID(), requestTicket->getRequestID(), AutoPacketInstanceList, true);
               
               packetStream.writeIntBE((int32_t)request_locationid);
               packetStream.writeIntBE((int32_t)results.size()); // num instances
               
               for (auto &rowv : results)
               {
                  auto row = rowv.value;
                  int64_t socket_id = DBToS64(row[0]);
                  std::shared_ptr<OVProtocolHandler> hdlr = BackendConnectionController::getBySocketID((uint32_t)socket_id);
                  OVBackendGameConnectionState* state = hdlr ? dynamic_cast<OVBackendGameConnectionState*>(hdlr->mState) : NULL;
                  if (state)
                  {
                     // This should be thread-safe in this case
                     packetStream.writeIntBE((int32_t)state->mInstanceID);
                     packetStream.writeUintBE((uint32_t)state->mConnectedUIDs.size()); // users present
                     packetStream.writeUintBE((uint32_t)state->mNumHomes); // homes
                     packetStream.writeUintBE((uint32_t)0); // friends present TODO
                     packetStream.writeString(state->mInstanceName.c_str());
                  }
                  else
                  {
                     packetStream.writeIntBE((int32_t)-1);
                     packetStream.writeUintBE((uint32_t)0); // users present
                     packetStream.writeUintBE((uint32_t)0); // homes
                     packetStream.writeUintBE((uint32_t)0); // friends present
                     packetStream.writeString("");
                     
                  }
               }
               
               OVFreeableData* responseData = packet.commitToEncodedPacket(&clientHandler->mEncryptionKey);
               if (responseData)
                  clientHandler->queuePacketData(responseData);
               endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, responseData ? ReturnCode_OK : RESPONSE_ERR);
            });
         }
         
      }
      
      info.cleanup();
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_ResolveURLHandler);

void BackendConnectionController::postUserLoginQueued(std::shared_ptr<OVProtocolHandler> handler, const UserInfo &info)
{
   MemStream packetStream(4096);
   PacketBuilder packet(&packetStream);
   packet.beginAutoPacket(AutoPacketBackendUserLoginQueued);
   
   packetStream.writeUintBE(info.uid);
   packetStream.writeUintBE(info.permissions);
   packetStream.writeUintBE(info.invID); // NEW
   packetStream.write((uint8_t)info.isVIP);
   packetStream.write((uint8_t)info.avatarSex);
   packetStream.write(20, info.pwdHash);
   packetStream.writeString(info.username);
   
   packet.commit();
   
   PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
   handler->onQueueWriteData(responseData);
}

class RPC_RequestInstance : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(2, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   enum Response
   {
      RESPONSE_OK=0,
      RESPONSE_CANNOT_CONNECT=0xfffffffb,
      RESPONSE_UNKNOWN_LOCATION=0xfffffffc,
      RESPONSE_UNKNOWN_INSTANCE=0xfffffffd,
   };
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      // There are two types of query:
      // 0x1: resolveURL (string url)
      // 0x2: getInstances (uint locationId)
      // The packet wrapped in the query will be of id 1 or 2.
      
      // Should return
      // autopacket(rpc)
      // uint16 1
      // uint32 requestId
      // response packet 201
      
      std::shared_ptr<OVProtocolHandler> clientHandler(client.shared_from_this());
      
      if (info.code == 1)
      {
         MemStream stream(info.size, ((uint8_t*)info.data));
         
         uint16_t queryCode = info.code;
         PacketMallocData* responseData = NULL;
         
         int32_t location_id=0;
         int32_t instance_id=0;
         
         stream.readIntBE(location_id);
         stream.readIntBE(instance_id); // this will be same value as returned by resolve
         
         if (location_id < 0)
         {
            endRPCResponseWithTicket(*clientHandler.get(), requestTicket, 0, RESPONSE_UNKNOWN_LOCATION);
            return;
         }
         
         if (location_id < 0)
         {
            endRPCResponseWithTicket(*clientHandler.get(), requestTicket, 0, RESPONSE_UNKNOWN_INSTANCE);
            return;
         }
         
         Log::client_printf(client, LogEntry::General, "Trying to connect to location %u instance %u\n", location_id, instance_id);
         
         
         // Request instance
         
         BackendConnectionController::waitLocationReady(clientHandler, location_id, instance_id,
                                                        [this, location_id, instance_id, clientHandler, requestTicket](std::error_code ec, OVBackendGameConnectionState* state) {
                                                           
                                                           if (ec || (state == NULL) || (state->mLastAcceptState == OVBackendGameConnectionState::ACCEPT_ABORT))
                                                           {
                                                              endRPCResponseWithTicket(*clientHandler.get(), requestTicket, 0, RESPONSE_CANNOT_CONNECT); // TODO: check ec
                                                           }
                                                           else
                                                           {
                                                              // Just send an InstanceReady packet with our test server
                                                              StackMemStream<6+4+4+4+2> packetStream;
                                                              PacketBuilder packet(&packetStream);
                                                              packet.beginBasicPacket(PacketInstanceReady);
                                                              
                                                              packetStream.writeIntBE(location_id);
                                                              packetStream.writeIntBE(instance_id);
                                                              if (clientHandler->mProtocol & OVProtocolHandler::PROTOCOL_09)
                                                              {
                                                                  if (gForceLocationIp)
                                                                  {
                                                                    state->mLocationAddress[0] = gForceLocationIpAddress[0];
                                                                    state->mLocationAddress[1] = gForceLocationIpAddress[1];
                                                                    state->mLocationAddress[2] = gForceLocationIpAddress[2];
                                                                    state->mLocationAddress[3] = gForceLocationIpAddress[3];
                                                                  }
                                                                 
                                                                 packetStream.write(4, &state->mLocationAddress[0]);
                                                              }
                                                              packetStream.writeUintBE((uint16_t)state->mLocationPort);
                                                              
                                                              packet.commit();
                                                              
                                                              clientHandler->queuePacketData(PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr));
                                                              endRPCResponseWithTicket(*clientHandler.get(), requestTicket, 0, RESPONSE_OK);
                                                           }
                                                           
                                                           
                                                        });
      }
      //createRPCResponse(clientHandler, request_id, 0, RESPONSE_CANNOT_CONNECT);
      
      // packet should be:
      // uint32 value
      // uint32 value
      
      
      // Return code in the packet is uint32, can be:
      // 0 = ok
      // 0xfffffffb = cannot connect
      // 0xfffffffc = unknown instance
      // 0xfffffffd = unknown location
      // The request id needs to match the code otherwise the client will barf up.
      
      info.cleanup();
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_RequestInstance);

class RPC_FriendRequest : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(3, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   enum
   {
      Query_Request=1
   };
   
   enum RequestCode
   {
      REQUEST_ADD=0,
      REQUEST_ACCCEPT=1,
      REQUEST_DELETE=2,
      REQUEST_IGNORE=3
   };
   
   enum ResponseCodes
   {
      RESPONSE_OK = 0,
      RESPONSE_IGNORING = 0xfffffffb,
      RESPONSE_TOO_MANY_FRIENDS = 0xfffffffc,
      RESPONSE_INVALID_USERNAME = 0xfffffffd
   };
   
   virtual void handleRequest(OVProtocolHandler& clientHandler, const OVPacketInfo info, RPCTicketPtr requestTicket, RequestCode requestCode, uint32_t friend_uid, const char* friend_name)
   {
      OVServerGameConnectionState& state = *((OVServerGameConnectionState*)clientHandler.mState);
      if (requestCode == REQUEST_ADD)
      {
         if (state.getFriendCount() > FriendController::FriendLimit)
         {
            // Too many friends!
            endRPCResponseWithTicket(clientHandler, requestTicket, 0, RESPONSE_TOO_MANY_FRIENDS);
         }
         else if (state.isFriendOrPending(friend_uid))
         {
            // Already pending or friend!
            endRPCResponseWithTicket(clientHandler, requestTicket, 0, RESPONSE_OK);
         }
         else if (state.isIgnoringUser(friend_uid))
         {
            endRPCResponseWithTicket(clientHandler, requestTicket, 0, RESPONSE_IGNORING);
         }
         else
         {
            // New request!
            if (!state.isBeingIgnoredBy(friend_uid))
            {
               FriendController::startFriendRequest(friend_uid, clientHandler.mUid, friend_name, clientHandler.mUsername.c_str());
               endRPCResponseWithTicket(clientHandler, requestTicket, 0, RESPONSE_OK);
            }
            else
            {
               // They're ignoring us, but silently accept anyway
               endRPCResponseWithTicket(clientHandler, requestTicket, 0, RESPONSE_OK);
            }
         }
      }
      else if (requestCode == REQUEST_ACCCEPT)
      {
         if (state.isAcceptableFriend(friend_uid))
         {
            // Now we're friends
            FriendController::setFriendRecord(friend_uid, clientHandler.mUid, FRIENDSTATE_OFFLINE, clientHandler.mUsername.c_str());
            FriendController::setFriendRecord(clientHandler.mUid, friend_uid, FRIENDSTATE_OFFLINE, friend_name);
            endRPCResponseWithTicket(clientHandler, requestTicket, 0, RESPONSE_OK);
         }
         else
         {
            endRPCResponseWithTicket(clientHandler, requestTicket, 0, RESPONSE_INVALID_USERNAME);
         }
      }
      else if (requestCode == REQUEST_DELETE)
      {
         FriendController::setFriendRecord(clientHandler.mUid, friend_uid, FRIENDSTATE_DELETE, friend_name);
         FriendController::setFriendRecord(friend_uid, clientHandler.mUid, FRIENDSTATE_DELETE, clientHandler.mUsername.c_str());
         endRPCResponseWithTicket(clientHandler, requestTicket, 0, RESPONSE_OK);
      }
      else if (requestCode == REQUEST_IGNORE)
      {
         FriendController::setFriendRecord(clientHandler.mUid, friend_uid, FRIENDSTATE_IGNORE, friend_name);
         FriendController::setFriendRecord(friend_uid, clientHandler.mUid, FRIENDSTATE_DELETE, clientHandler.mUsername.c_str()); // apparently this is what should happen
         endRPCResponseWithTicket(clientHandler, requestTicket, 0, RESPONSE_OK);
      }
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      // request packet should be ??
      // uint8 code
      // string user
      
      // code can be:
      // add
      // accept
      // delete
      // ignore
      
      
      // Return code in the packet is uint32, can be:
      // 0 = ok
      // 0xfffffffb = request ignored? or user already ignored?
      // 0xfffffffc = too many friends
      // 0xfffffffd = invalid username
      // The request id needs to match the code otherwise the client will barf up.
      
      if (info.code != Query_Request)
         return;
      
      
      char username[33];
      uint8_t update_code;
      stream.read(update_code);
      stream.readString(&username[0], 32);
      
      Log::client_printf(client, LogEntry::Debug, "FriendRequest update_code is %u, packet id is %u\n", update_code, info.code);
      
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      info.ensureFreeable(); // needed so we can still read the info in the callback
      
      // First resolve the darn uid for the target user
      UserConnectionController::resolveUIDAndName(username, [this, clientHandler, info, requestTicket, update_code](uint32_t uid, const char* query_username){
         if (uid == 0)
         {
            // Error!
            endRPCResponseWithTicket(*clientHandler, requestTicket, 0, RESPONSE_INVALID_USERNAME);
            info.freeData->free();
         }
         else
         {
            // Now onto the meat
            handleRequest(*clientHandler, info, requestTicket, (RequestCode)update_code, uid, query_username);
            info.freeData->free();
         }
      });
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_FriendRequest);

class RPC_CustomerService : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(4, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   enum CommandCode : uint8_t
   {
      INVALID_CMD,
      COMMAND_KICK,              // <user>
      COMMAND_KICKALL,           //
      COMMAND_SRV_MSG,           // <user> <message> / <message>
      COMMAND_SRV_ANNOUNCE,      // <message>
      COMMAND_SUSPEND,           // <user> <hours>
      COMMAND_BAN,               // <user> <ban flag>
      COMMAND_ADD_CURRENCY       // <user> <CC> <PP>
   };
   
   enum ResponseCodes
   {
      RESPONSE_INVALID_NUMARGS=0,
      RESPONSE_INVALID_COMMAND=1,
      RESPONSE_USER_OFFLINE=2,
      RESPONSE_INVALID_USERNAME=3,
      RESPONSE_NO_PERMISSION=4,
      RESPONSE_OK = 5
   };
   
   ResponseCodes kickUser(const char* username)
   {
      auto destConnection = UserConnectionController::resolveUsernameConnection(username);
      if (destConnection)
      {
         destConnection->kick(1);
         return RESPONSE_OK;
      }
      else
      {
         return RESPONSE_USER_OFFLINE;
      }
   }
   
   virtual void doBanUser(const char* username, uint64_t time)
   {
      std::string suser = username;
      DBQueryQueue::getQueue().addToQueueAffected(DBQueryQueue::getStrandRef(), SQLBindPack("UPDATE users SET banned_until_time = $1 WHERE username = $2;", (int64_t)time, username),
      [this, suser](OVDBConnection* a, const std::error_code &ec, uint64_t affected_rows){
         // Kick after since they will see the update only after query is changed
         kickUser(suser.c_str());
      });
   }
   
   virtual void doUnBanUser(const char* username)
   {
      DBQueryQueue::getQueue().addToQueueAffected(DBQueryQueue::getStrandRef(), SQLBindPack("UPDATE users SET banned_until_time = 0 WHERE username = $1;", username),
      [](OVDBConnection* a, const std::error_code &ec, uint64_t affected_rows){
         // TODO: send callback later
      });
   }
   
   virtual void doModifyCurrency(const char* username, int32_t CC_Diff,  int32_t PP_Diff)
   {
      DBQueryQueue::getQueue().addToQueueMultipleRows(DBQueryQueue::getStrandRef(), SQLBindPack("BEGIN TRANSACTION;\n"
      "INSERT INTO transactions(item_id, user_id, action_id, created_at, cc, pp, obj_server_id) "
      "VALUES (0, (SELECT id FROM users WHERE username=$1 LIMIT 1), 0, NOW(), $2, $3, 0);"
      "UPDATE users SET cc=users.cc+$2, pp=users.pp+$3 WHERE username = $1 LIMIT 1;\n"
      "COMMIT;\n", username, (int64_t)CC_Diff, (int64_t)PP_Diff), true,
      [](OVDBConnection* a, const std::error_code &ec, uint64_t affected_rows, std::vector<DBResult> &results){
         // TODO: send callback later
      });
   }
   
   virtual void doServerMessage(const std::vector<std::string> &words, bool announcement)
   {
      std::string str = StringUtil::joinStrings(words, " ");
      
      MemStream packetStream(4096);
      PacketBuilder packet(&packetStream);
      packet.beginBasicPacket(announcement ? PacketServerAnnouncement : PacketServerMessage);
      
      packetStream.writeString(str.c_str());
      
      packet.commit();
      
      PacketMultipleUserData* responseData = PacketMultipleUserData::createFromData(packetStream.getPosition(), packetStream.mPtr);
      
      // We need to send a message to every connected, logged in client...
      
      UserConnectionController::eachLoggedInConnection([responseData](OVProtocolHandler *connection){
         connection->queuePacketData(responseData->cloneMultiple());
      });
      
      responseData->free();
   }
   
   virtual void handle(OVProtocolHandler& clientHandler, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);

      uint8_t command = 0;
      uint8_t numArgs = 0;
      uint32_t responseCode = RESPONSE_OK;
      
      stream.read(command);
      stream.read(numArgs);
      std::vector<std::string> args;
      std::string tmp;
      
      if (!clientHandler.mPermissionsMask.hasGuideLevel())
      {
         endRPCResponseWithTicket(clientHandler, requestTicket, 0, RESPONSE_NO_PERMISSION);
         return;
      }
      
      for (int i=0; i<numArgs; i++)
      {
         std::string tmp;
         stream.readString(tmp);
         args.push_back(std::move(tmp));
      }
      
      std::shared_ptr<OVProtocolHandler> destConnection;
      
      switch (command)
      {
         case COMMAND_KICK:
            if (numArgs < 1)
            {
               responseCode = RESPONSE_INVALID_NUMARGS;
               break;
            }
            
            responseCode = kickUser(args[0].c_str());
            break;
         case COMMAND_KICKALL:
            if (!clientHandler.mPermissionsMask.hasAdminLevel())
            {
               responseCode = RESPONSE_NO_PERMISSION;
               break;
            }
            
            UserConnectionController::eachLoggedInConnection([](OVProtocolHandler *connection){
               connection->kick(1);
            });
            
            break;
         case COMMAND_SRV_MSG:
            if (!clientHandler.mPermissionsMask.hasAdminLevel())
            {
               responseCode = RESPONSE_NO_PERMISSION;
               break;
            }
            
            doServerMessage(args, false);
            break;
         case COMMAND_SRV_ANNOUNCE:
            doServerMessage(args, true);
            break;
         case COMMAND_SUSPEND:
            if (args.size() < 2)
            {
               responseCode = RESPONSE_INVALID_NUMARGS;
               break;
            }
            
            {
               auto unix_timestamp = std::chrono::seconds(time(NULL)).count();
               doBanUser(args[0].c_str(), unix_timestamp + (std::atoi(args[1].c_str()) * 3600));
            }
            
            break;
         case COMMAND_BAN:
            if (!clientHandler.mPermissionsMask.hasCommManagerLevel())
            {
               responseCode = RESPONSE_NO_PERMISSION;
               break;
            }
            
            if (args.size() > 1 && (std::atoi(args[1].c_str()) != 0))
            {
               doBanUser(args[0].c_str(), 1);
            }
            else
            {
               doUnBanUser(args[0].c_str());
            }
            
            break;
         case COMMAND_ADD_CURRENCY:
            if (args.size() < 3)
            {
               responseCode = RESPONSE_INVALID_NUMARGS;
               break;
            }
            
            if (!clientHandler.mPermissionsMask.hasCommManagerLevel())
            {
               endRPCResponseWithTicket(clientHandler, requestTicket, 0, RESPONSE_NO_PERMISSION);
               return;
            }
            
            doModifyCurrency(args[0].c_str(), std::atoi(args[1].c_str()), std::atoi(args[2].c_str()) );
            
            break;
         default:
            responseCode = RESPONSE_INVALID_COMMAND;
            break;
      }
      
      endRPCResponseWithTicket(clientHandler, requestTicket, 0, responseCode);
      
      info.cleanup();
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_CustomerService);

class RPC_ServerManagement : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(5, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   enum ManagementPacketType : uint8_t
   {
      Fallback,
      Management_AutoPacket,
      Management_MasterInfo,
      Management_LocationList,
      Management_InstanceList,
      Management_InstanceInfo,
      Management_UsersList,
      Management_UsersFriends,
      Management_UserInfo,
      Management_LauncherList,
      Management_LauncherInfo
   };
   
   enum CommandCode : uint8_t
   {
      INVALID_CMD,
      COMMAND_GET_MASTER_INFO,
      COMMAND_GET_USER_LIST,
      COMMAND_GET_FRIEND_LIST,
      COMMAND_GET_USER_INFO,
      COMMAND_GET_FRIEND_INFO,
      COMMAND_GET_LOCATION_LIST,
      COMMAND_GET_INSTANCE_LIST,
      COMMAND_GET_INSTANCE_INFO,
      COMMAND_GET_LAUNCHER_LIST,
      COMMAND_GET_LAUNCHER_INFO,
      COMMAND_CRASH_MASTER,
      COMMAND_CRASH_CURRENT,
      COMMAND_UPDATE_SERVERS,
      COMMAND_COPY_LOCATION,
      COMMAND_DELETE_LOCATION,
      COMMAND_CREATE_INSTANCE,
      COMMAND_CLOSE_INSTANCE,
      COMMAND_EMPTY_INSTANCE,
      COMMAND_START_INSTANCE,
      COMMAND_SET_PERMISSION,
      COMMAND_SET_LOC_PERMISSION
   };
   
   enum ResponseCode : uint32_t
   {
      Response_Ok = 0,
      Response_Invalid_Argument = 0xfffffffa,
      Response_Number_Args = 0xfffffffb,
      Response_Invalid_Command = 0xfffffffc,
      Response_Noe_Permission = 0xfffffffd
   };
   
   enum
   {
      Request_Get = 0x1
   };
   
   virtual void handle(OVProtocolHandler& clientHandler, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      // TODO
      // Seems to have several different sub-packets
      
      // Uses same basic return code mechanism
      
      // Likes sending MasterInfo packets

      
      MemStream stream(info.size, info.data);
      
      // Check permissions
      if (!clientHandler.mPermissionsMask.hasNetworkAdminLevel())
      {
         endRPCResponseWithTicket(clientHandler, requestTicket, 0, Response_Noe_Permission);
         return;
      }
      
      if (info.code == Request_Get) // get
      {
         // query should be of form
         // response should be:
         // string data "ident1\tkeys\nident2\tkeys\n"...
         const char* serverData = "server1\t1\t5050\tAUTH_LEVEL_DEVELOPER\nserver2\t0\t5051\tAUTH_LEVEL_USER\n";
         
         CommandCode commandNum = INVALID_CMD;
         uint8_t secondKey = 0;
         
         stream.read(commandNum);
         stream.read(secondKey);
         
         ManagementPacketType returnPacketType = Fallback;
         
         MemStream packetStream(0x4000);
         PacketBuilder packet(&packetStream);
         packet.beginRPC(getRPCID(), requestTicket->getRequestID(), 0, false);
         
         /*
          Management_MasterInfo,
          Management_LocationList,
          Management_InstanceList,
          Management_InstanceInfo,
          Management_UsersList,
          Management_UsersFriends,
          Management_UserInfo,
          Management_LauncherList,
          Management_LauncherInfo*/
         
         if (commandNum == COMMAND_GET_MASTER_INFO)
         {
            packetStream.writeString(serverData);
            returnPacketType = Management_MasterInfo;
         }
         else if (commandNum == COMMAND_GET_LAUNCHER_LIST)
         {
            packetStream.writeString("launcher1\t1\t5050\tAUTH_LEVEL_DEVELOPER\nlauncher2\t0\t5051\tAUTH_LEVEL_USER\n");
            returnPacketType = Management_LauncherList;
         }
         else if (commandNum == COMMAND_GET_LOCATION_LIST)
         {
            packetStream.writeString("location1\t1\t5050\tAUTH_LEVEL_DEVELOPER\nlocation2\t0\t5051\tAUTH_LEVEL_USER\n");
            returnPacketType = Management_LocationList;
         }
         else if (commandNum == COMMAND_GET_USER_LIST)
         {
            packetStream.writeString("user1\t1\t5050\tAUTH_LEVEL_DEVELOPER\nuser2\t0\t5051\tAUTH_LEVEL_USER\n");
            returnPacketType = Management_UsersList;
         }
         else if (commandNum == COMMAND_GET_INSTANCE_LIST)
         {
            packetStream.writeString("inst11\t1\t5050\tAUTH_LEVEL_DEVELOPER\ninst22\t0\t5051\tAUTH_LEVEL_USER\n");
            returnPacketType = Management_InstanceList;
         }
         else if (commandNum == COMMAND_GET_FRIEND_LIST)
         {
            packetStream.writeString("friend1\t1\t5050\tAUTH_LEVEL_DEVELOPER\nfriend2\t0\t5051\tAUTH_LEVEL_USER\n");
            returnPacketType = Management_UsersFriends;
         }
         else if (commandNum == COMMAND_GET_INSTANCE_INFO)
         {
            packetStream.writeString("inst_info1\t1\t5050\tAUTH_LEVEL_DEVELOPER\ninst_info2\twoo\t5051\tAUTH_LEVEL_USER\n");
            returnPacketType = Management_InstanceInfo;
         }
         else if (commandNum == COMMAND_GET_LAUNCHER_INFO)
         {
            packetStream.writeString("launch_info1\t1\t5050\tAUTH_LEVEL_DEVELOPER\nlaunch_info2\twoo\t5051\tAUTH_LEVEL_USER\n");
            returnPacketType = Management_LauncherInfo;
         }
         else if (commandNum == COMMAND_GET_FRIEND_INFO)
         {
            packetStream.writeString("friend1\t1\t5050\tAUTH_LEVEL_DEVELOPER\nfriend2\twoo\t5051\tAUTH_LEVEL_USER\n");
            returnPacketType = Management_UserInfo;
         }
         else if (commandNum == COMMAND_GET_USER_INFO)
         {
            packetStream.writeString("friend1\t1\t5050\tAUTH_LEVEL_DEVELOPER\nfriend2\twoo\t5051\tAUTH_LEVEL_USER\n");
            returnPacketType = Management_UserInfo;
         }
         else
         {
            packetStream.write((uint32_t)Response_Invalid_Command);
            packet.commit();
            clientHandler.queuePacketData(PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr));
            info.cleanup();
            return;
         }
         
         packet.mPacketID = returnPacketType; // switch to correct type
         packet.commit();
         
         clientHandler.queuePacketData(PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr));
      }
      else
      {
         Log::client_errorf(clientHandler, LogEntry::General, "Unknown server management packet %u\n", info.code);
         endRPCResponseWithTicket(clientHandler, requestTicket, 0, Response_Invalid_Command);
      }
      
      Log::printf("TODO: RPC_ServerManagement");
      
      info.cleanup();
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_ServerManagement);

// TODO: check
class RPC_StoreFolders : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(6, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   enum CommandCode : uint8_t
   {
      INVALID_CMD,
      COMMAND_ADD_FOLDER,
      COMMAND_DELETE_FOLDER,
      COMMAND_RENAME_FOLDER,
      COMMAND_MOVE_FOLDER,
      COMMAND_INSERT_ITEM,
      COMMAND_DELETE_ITEM,
      COMMAND_MOVE_ITEM,
      COMMAND_FOLDER_ITEMS
   };
   
   enum ResponseCode
   {
      Response_NumberArguments=0,
      Response_InvalidCommand=1,
      Response_ReturnInvalidItem=2,
      Response_ReturnInvalidFolder=3,
      Response_ReturnNoePermission=4,
      Response_Ok = 0x5 // >= ok
   };
   
   // NOTE: for most responses we should return the destination folder again
   
   void doAddItem(std::shared_ptr<OVProtocolHandler> clientHandler, RPCTicketPtr requestTicket, uint32_t dest_folder_id, uint32_t resource_id)
   {
      DBQueryQueue::getQueue().addToQueueInsert(clientHandler->getStrandRef(), SQLBindPack("INSERT INTO store_items(folder_id,resource_data_id) VALUES ($1,$2)", (int64_t)dest_folder_id, (int64_t)resource_id),
                                          [this, clientHandler, requestTicket, dest_folder_id](OVDBConnection* a, const std::error_code &ec, uint64_t insert_id){
         
         doListFolder(clientHandler, requestTicket, (uint32_t)dest_folder_id);
      });
   }
   
   void doDeleteItem(std::shared_ptr<OVProtocolHandler> clientHandler, RPCTicketPtr requestTicket, uint32_t item_id)
   {
      DBQueryQueue::getQueue().addToQueueMultipleRows(clientHandler->getStrandRef(), SQLBindPack("BEGIN TRANSACTION;SELECT folder_id FROM store_items WHERE id = $1 LIMIT 1;DELETE FROM store_items WHERE id = $2; COMMIT;", (int64_t)item_id, (int64_t)item_id), true, [this, clientHandler, requestTicket](OVDBConnection* async, const std::error_code &ec, uint32_t queryNum, std::vector<DBResult> &results){
            if (ec)
            {
               endRPCResponseWithTicket(*clientHandler.get(), requestTicket, 0, Response_ReturnInvalidItem);
            }
         
            if (queryNum == 1 && results.size() > 0)
            {
               uint32_t folder_id = DBToU32(results[0].value[0]);
               doListFolder(clientHandler, requestTicket, folder_id);
            }
         }
      );
   }
   
   void doMoveItem(std::shared_ptr<OVProtocolHandler> clientHandler, RPCTicketPtr requestTicket, uint32_t dest_folder_id, uint32_t item_id)
   {
      DBQueryQueue::getQueue().addToQueueAffected(clientHandler->getStrandRef(), SQLBindPack("UPDATE store_items SET folder_id = $1 WHERE id = $2", (int64_t)dest_folder_id, (int64_t)item_id),
      [this, clientHandler, requestTicket, dest_folder_id](OVDBConnection* a, const std::error_code &ec, uint64_t affected_rows){
         doListFolder(clientHandler, requestTicket, (uint32_t)dest_folder_id);
      });
   }
   
   void doAddFolder(std::shared_ptr<OVProtocolHandler> clientHandler, RPCTicketPtr requestTicket, uint32_t folder_id, std::string name)
   {
      DBQueryQueue::getQueue().addToQueueInsert(clientHandler->getStrandRef(), SQLBindPack("INSERT INTO store_folders(name, parent_id) VALUES($1, $2)", name.c_str(), (int64_t)folder_id),
      [this, clientHandler, requestTicket, folder_id](OVDBConnection* a, const std::error_code &ec, uint64_t insert_id){
         doListFolder(clientHandler, requestTicket, (uint32_t)folder_id);
      });
   }
   
   void doMoveFolder(std::shared_ptr<OVProtocolHandler> clientHandler, RPCTicketPtr requestTicket, uint32_t folder_id, uint32_t dest_folder_id)
   {
      DBQueryQueue::getQueue().addToQueueAffected(clientHandler->getStrandRef(), SQLBindPack("UPDATE store_folders SET parent_id = $1 WHERE id = $2", (int64_t)dest_folder_id, (int64_t)folder_id),
      [this, clientHandler, requestTicket, dest_folder_id](OVDBConnection* a, const std::error_code &ec, uint64_t affected_rows){
         doListFolder(clientHandler, requestTicket, (uint32_t)dest_folder_id);
      });
   }
   
   void doDeleteFolder(std::shared_ptr<OVProtocolHandler> clientHandler, RPCTicketPtr requestTicket, uint32_t folder_id)
   {
      DBQueryQueue::getQueue().addToQueueAffected(clientHandler->getStrandRef(), SQLBindPack("DELETE FROM store_folders WHERE id = $1", (int64_t)folder_id),
      [this, clientHandler, requestTicket](OVDBConnection* a, const std::error_code &ec, uint64_t affected_rows){
         endRPCResponseWithTicket(*clientHandler.get(), requestTicket, 0, Response_Ok);
      });
      
   }
   
   void doRenameFolder(std::shared_ptr<OVProtocolHandler> clientHandler, RPCTicketPtr requestTicket, uint32_t folder_id, std::string newName)
   {
      DBQueryQueue::getQueue().addToQueueAffected(clientHandler->getStrandRef(), SQLBindPack("UPDATE store_folders SET name = $1 WHERE id = $2", newName.c_str(), (int64_t)folder_id),
      [this, clientHandler, requestTicket](OVDBConnection* a, const std::error_code &ec, uint64_t affected_rows){
         endRPCResponseWithTicket(*clientHandler.get(), requestTicket, 0, Response_Ok);
      });
   }
   
   void doListFolder(std::shared_ptr<OVProtocolHandler> clientHandler, RPCTicketPtr requestTicket, uint32_t folder_id)
   {
      DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT id,1,name,NULL,NULL FROM store_folders WHERE parent_id = $1 UNION ALL SELECT store_items.id,2,resource_datas.id,resource_datas.name,resource_datas.object_id FROM store_items INNER JOIN resource_datas ON resource_datas.id = store_items.resource_data_id WHERE folder_id = $1", (int64_t)folder_id), false, [this, clientHandler, requestTicket](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
         
         if (!ec)
         {
            uint32_t packet_len = 0;
            
            MemStream packetStream(0x4000);
            PacketBuilder packet(&packetStream);
            packet.beginRPC(getRPCID(), requestTicket->getRequestID(), 0x2, false);
            
            // Begin string
            uint32_t string_len_pos = packetStream.getPosition() + 1;
            packetStream.writeString("");
            
            // Write folders
            for (auto &rowv : results)
            {
               auto row = rowv.value;
               uint32_t ident = DBToU32(row[1]);
               if (ident == 1)
               {
                  char buf[256];
                  snprintf(buf, sizeof(buf), "%s\t%s\n", row[0], row[2]);
                  packetStream.write(strlen(buf), buf);
               }
            }
            
            // Correct size of folder string
            packet_len = packetStream.getPosition();
            packetStream.setPosition(string_len_pos);
            packetStream.writeUintBE((uint16_t)(packet_len - string_len_pos - 2));
            packetStream.setPosition(packet_len);
            
            // Begin string
            string_len_pos = packetStream.getPosition() + 1;
            packetStream.writeString("");
            
            // Write objects
            for (auto &rowv : results)
            {
               auto row = rowv.value;
               uint32_t ident = DBToU32(row[1]);
               if (ident == 2)
               {
                  // store_items.id,2,resource_datas.id,resource_datas.name,resource_datas.object_id
                  char buf[256];
                  uint32_t entry_id = DBToU32(row[0]);
                  uint32_t object_id = DBToU32(row[1]);
                  uint32_t resource_id = DBToU32(row[2]);
                  snprintf(buf, sizeof(buf), "%u\t%u\t%u\t%s\n", entry_id, object_id, resource_id, row[3]);
                  packetStream.write(strlen(buf), buf);
               }
            }
            
            // Correct size of object string
            packet_len = packetStream.getPosition();
            packetStream.setPosition(string_len_pos);
            packetStream.writeUintBE((uint16_t)(packet_len - string_len_pos - 2));
            packetStream.setPosition(packet_len);

            OVFreeableData* responseData = packet.commitToEncodedPacket(&clientHandler->mEncryptionKey);
            if (responseData)
               clientHandler->queuePacketData(responseData);
         }
      });
   }
   
   void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      // Check permissions
      if (!client.mPermissionsMask.hasSuperAdminLevel())
      {
         endRPCResponseWithTicket(client, requestTicket, 0, Response_ReturnNoePermission);
         return;
      }
      
      // request packet should be:
      // uint8 code
      // uint8 num_params
      // for each num_params:
      //   string value
      
      // response:
      // packet 0x2 = store folders packet
      // uses same response code spackets
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      uint8_t code = 0;
      ResponseCode responseCode = Response_Ok;
      stream.read(code);
      
      if (code == COMMAND_ADD_FOLDER)
      {
         uint8_t num_params = 0;
         stream.read(num_params);
         
         if (num_params == 2)
         {
            uint64_t folder_id = 0;
            char buf[64];
            stream.readString(buf, 45);
            folder_id = DBToU64(buf);
            std::string name = "";
            stream.readString(name);
            AdminQueryQueue::getQueue().addToQueue(this, [this, folder_id, clientHandler, requestTicket, name]() { doAddFolder(clientHandler, requestTicket, (uint32_t)folder_id, name); } );
            return;
         }
         else
         {
            responseCode = Response_NumberArguments;
         }
      }
      else if (code == COMMAND_DELETE_FOLDER)
      {
         uint8_t num_params = 0;
         stream.read(num_params);
         
         if (num_params == 1)
         {
            uint64_t folder_id = 0;
            char buf[64];
            stream.readString(buf, 45);
            folder_id = DBToU64(buf);
            AdminQueryQueue::getQueue().addToQueue(this, [this, clientHandler, requestTicket, folder_id]() { doDeleteFolder(clientHandler, requestTicket, (uint32_t)folder_id); } );
            //return;
         }
         else
         {
            responseCode = Response_NumberArguments;
         }
         
      }
      else if (code == COMMAND_RENAME_FOLDER)
      {
         uint8_t num_params = 0;
         stream.read(num_params);
         
         if (num_params == 2)
         {
            uint64_t folder_id = 0;
            char buf[64];
            stream.readString(buf, 45);
            folder_id = DBToU64(buf);
            std::string name;
            stream.readString(name);
            
            AdminQueryQueue::getQueue().addToQueue(this, [this, folder_id, clientHandler, requestTicket, name]() { doRenameFolder(clientHandler, requestTicket, (uint32_t)folder_id, name); } );
            //return;
         }
         else
         {
            responseCode = Response_NumberArguments;
         }
         
      }
      else if (code == COMMAND_MOVE_FOLDER)
      {
         uint8_t num_params = 0;
         stream.read(num_params);
         
         if (num_params == 2)
         {
            uint64_t folder_id = 0;
            uint64_t dest_folder_id = 0;
            char buf[46];
            stream.readString(buf, 45);
            folder_id = DBToU64(buf);
            stream.readString(buf, 45);
            dest_folder_id = DBToU64(buf);
            
            AdminQueryQueue::getQueue().addToQueue(this, [this, folder_id, clientHandler, requestTicket, dest_folder_id]() { doMoveFolder(clientHandler, requestTicket, (uint32_t)folder_id, (uint32_t)dest_folder_id); } );
            return;
         }
         else
         {
            responseCode = Response_NumberArguments;
         }
         
      }
      else if (code == COMMAND_INSERT_ITEM)
      {
         uint8_t num_params = 0;
         stream.read(num_params);
         
         if (num_params == 3)
         {
            uint64_t item_id = 0;
            uint64_t dest_folder_id = 0;
            uint64_t object_id = 0;
            char buf[46];
            stream.readString(buf, 45);
            item_id = DBToU64(buf);
            stream.readString(buf, 45);
            dest_folder_id = DBToU64(buf);
            stream.readString(buf, 45);
            object_id = DBToU64(buf);
            
            AdminQueryQueue::getQueue().addToQueue(this, [this, item_id, clientHandler, requestTicket, dest_folder_id]() { doAddItem(clientHandler, requestTicket, (uint32_t)item_id, (uint32_t)dest_folder_id); } );
            return;
         }
         else
         {
            responseCode = Response_NumberArguments;
         }
         
      }
      else if (code == COMMAND_DELETE_ITEM)
      {
         uint8_t num_params = 0;
         stream.read(num_params);
         
         if (num_params == 1)
         {
            uint64_t item_id = 0;
            char buf[46];
            stream.readString(buf, 45);
            item_id = DBToU64(buf);
            
            AdminQueryQueue::getQueue().addToQueue(this, [this, clientHandler, requestTicket, item_id]() { doDeleteItem(clientHandler, requestTicket, (uint32_t)item_id); } );
            return;
         }
         else
         {
            responseCode = Response_NumberArguments;
         }
      }
      else if (code == COMMAND_MOVE_ITEM)
      {
         uint8_t num_params = 0;
         stream.read(num_params);
         
         if (num_params == 2)
         {
            uint64_t item_id = 0;
            uint64_t dest_folder_id = 0;
            char buf[46];
            stream.readString(buf, 45);
            dest_folder_id = DBToU64(buf);
            stream.readString(buf, 45);
            item_id = DBToU64(buf);
            
            AdminQueryQueue::getQueue().addToQueue(this, [this, item_id, clientHandler, requestTicket, dest_folder_id]() { doMoveItem(clientHandler, requestTicket, (uint32_t)item_id, (uint32_t)dest_folder_id); } );
            return;
         }
         else
         {
            responseCode = Response_NumberArguments;
         }
      }
      else if (code == COMMAND_FOLDER_ITEMS)
      {
         uint8_t num_params = 0;
         stream.read(num_params);
         
         if (num_params == 1)
         {
            char buf[46];
            stream.readString(buf, 45);
            uint64_t folder_id = DBToU64(buf);
            
            AdminQueryQueue::getQueue().addToQueue(this, [this, clientHandler, requestTicket, folder_id]() { doListFolder(clientHandler, requestTicket, (uint32_t)folder_id); } );
            return;
         }
         else
         {
            responseCode = Response_NumberArguments;
         }
      }
      
      uint32_t packet_len = 6 + 6 + 6 + 4;
      
      MemStream packetStream(packet_len);
      PacketBuilder packet(&packetStream);
      packet.beginRPC(getRPCID(), requestTicket->getRequestID(), 0, false);
      
      packetStream.write(responseCode);
      
      packet.commit();
      
      PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
      client.queuePacketData(responseData);
      
      info.cleanup();
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_StoreFolders);

class RPC_PrivateMessage : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(7, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   // Dispatch type is +1 because who knows
   enum DispatchType
   {
      DISPATCH_NONE=0,
      DISPATCH_PRIVATE=1,
      DISPATCH_FRIENDS=2,
      DISPATCH_GUIDES=3,
      DISPATCH_GUIDE_REPLY=4
   };
   
   enum
   {
      Request_Message=1
   };
   
   // These directly correspond to types in MasterServerObject::onPrivateMessage
   enum MessageSource
   {
      SOURCE_PRIVATE=0,
      SOURCE_FRIENDS=1,
      SOURCE_GUIDES=2,
      SOURCE_USER_TO_GUIDE=3
   };
   
   enum ErrorCode
   {
      RESPONSE_OK=0,
      RESPONSE_USER_OFFLINE=0xfffffffc,
      RESPONSE_INVALID_USERNAME=0xfffffffd
   };
   
   static void queueUserMessage(std::shared_ptr<OVProtocolHandler> toClient, OVFreeableData* packetData, uint32_t fromUserId, bool force)
   {
      toClient->mStrand.post([force, toClient, packetData, fromUserId](){
         OVServerGameConnectionState* connectionState = ((OVServerGameConnectionState*)toClient->mState);
         if (force || !connectionState->isIgnoringUser(fromUserId))
         {
            toClient->onQueueWriteData(packetData);
         }
         else
         {
            packetData->free();
         }
      });
   }
   
   static void queueUserFriendsMessage(std::shared_ptr<OVProtocolHandler> fromClient, OVFreeableData* packetData)
   {
      OVFreeableData *multiplePacketData = packetData->cloneMultiple();
      
      fromClient->mStrand.post([fromClient, multiplePacketData](){
         uint32_t uid = fromClient->mUid;
         OVServerGameConnectionState* connectionState = ((OVServerGameConnectionState*)fromClient->mState);
         for (FriendStatusRecord record : connectionState->mFriendList)
         {
            if (record.state != FRIENDSTATE_BEING_IGNORED)
            {
               std::shared_ptr<OVProtocolHandler> toFriend = UserConnectionController::resolveUIDConnection(record.uid);
               if (toFriend)
               {
                  queueUserMessage(toFriend, multiplePacketData->cloneMultiple(), uid, false);
               }
            }
         }
         
         multiplePacketData->free();
      });
   }
   
   static void queueUserToGuideMessage(std::shared_ptr<OVProtocolHandler> fromClient, OVFreeableData* packetData)
   {
      GroupChatHandler* group = GroupChatHandler::getRoom(GroupChatHandler::ROOM_GUIDE);
      if (!group)
         return;
      
      packetData = packetData->cloneMultiple();
      
      group->mStrand.post([packetData, group](){
         group->queueMessage(packetData);
      });
   }
   
   static void queueGuideMessage(std::shared_ptr<OVProtocolHandler> fromClient, PacketMultipleUserData* packetData)
   {
      GroupChatHandler* group = GroupChatHandler::getRoom(GroupChatHandler::ROOM_GUIDE);
      if (!group)
         return;
      
      uint32_t uid = fromClient->mUid;
      packetData = (PacketMultipleUserData*)packetData->cloneMultiple(); // make our own copy since we are using a strand
      
      group->mStrand.post([uid, packetData, group](){
         group->dispatchMessageExceptToUser(packetData, uid);
         packetData->free();
      });
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      // packet should be:
      // 1 = normal message
      // 2 = friends
      // 3 = guide reply
      
      // if packet == 1:
      //    string user
      // string message
      
      // Return code in the packet is uint32, can be:
      // 0 = ok
      // 0xfffffffc = user offline
      // 0xfffffffd = invalid username
      // The request id needs to match the code otherwise the client will barf up.
      
      bool delivered = false;
      bool lookupRequired = false;
      
      {
         MemStream stream(info.size, (uint8_t*)info.data);
         
         uint16_t queryCode = info.code;
         
         if (queryCode == DISPATCH_PRIVATE)
         {
            // Single message
            PacketMallocData* responseData = NULL;
            char username[33];
            username[0] = '\0';
            username[32] = '\0';
            
            stream.readString(&username[0], 32);
            
            std::shared_ptr<OVProtocolHandler> destConnection = UserConnectionController::resolveUsernameConnection(username);
            if (destConnection)
            {
               delivered = true;
               std::string msg;
               stream.readString(msg);
               
               const uint32_t packet_len = 6 + 1 + 3 + msg.size() + 3 + client.mUsername.size();
               PacketMallocData* responseData = PacketMallocData::createOfSize(packet_len);
               MemStream packetStream(packet_len, responseData->mPtr);
               PacketBuilder packet(&packetStream);
               packet.beginBasicPacket(PacketPrivateMessage);
               
               packetStream.write((uint8_t)SOURCE_PRIVATE);
               packetStream.writeString(client.mUsername.c_str()); // 3+len
               packetStream.writeString(msg.c_str()); // 3+len
               
               packet.commit();
               responseData->mSize = packetStream.getPosition();
               
               queueUserMessage(destConnection, responseData, client.mUid, client.mPermissionsMask.hasGuideLevel());
            }
         }
         else if (queryCode == DISPATCH_GUIDE_REPLY)
         {
            // In this case, we need to send guide reply packets instead both to guides and the user
            char username[33];
            username[0] = '\0';
            username[32] = '\0';
            
            stream.readString(&username[0], 32); // destination user
            
            std::shared_ptr<OVProtocolHandler> destConnection = UserConnectionController::resolveUsernameConnection(username);
            if (destConnection)
            {
               delivered = true;
               std::string msg;
               stream.readString(msg);
               
               // Main user message
               {
                  const uint32_t packet_len = 6 + 3 + msg.size() + 3 + strlen(username) + 3 + 5;
                  PacketMallocData* pmResponseData = PacketMallocData::createOfSize(packet_len);
                  MemStream packetStream(pmResponseData->size(), pmResponseData->ptr());
                  PacketBuilder packet(&packetStream);
                  packet.beginBasicPacket(PacketGuideReplyMessage);
                  
                  packetStream.writeString("Guide");
                  packetStream.writeString(username);
                  packetStream.writeString(msg.c_str()); // 3+len
                  
                  packet.commit();
                  pmResponseData->mSize = packetStream.getPosition();
                  
                  queueUserMessage(destConnection, pmResponseData, client.mUid, true);
               }
               
               // Notification for other guides
               {
                  const uint32_t packet_len = 6 + 3 + msg.size() + 3 + strlen(username) + 3 + client.mUsername.size();
                  PacketMultipleUserData* responseData = PacketMultipleUserData::createOfSize(packet_len);
                  MemStream packetStream(responseData->size(), responseData->ptr());
                  PacketBuilder packet(&packetStream);
                  packet.beginBasicPacket(PacketGuideReplyMessage);
                  
                  packetStream.writeString(client.mUsername.c_str());
                  packetStream.writeString(username);
                  packetStream.writeString(msg.c_str()); // 3+len
                  
                  packet.commit();
                  responseData->mData->mSize = packetStream.getPosition();
                  
                  queueGuideMessage(client.shared_from_this(), responseData);
                  
                  responseData->free();
               }
            }
            else
            {
               endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_USER_OFFLINE);
            }
         }
         else if (queryCode == DISPATCH_FRIENDS || queryCode == DISPATCH_GUIDES)
         {
            // Multi-message
            delivered = true;
            std::string msg;
            stream.readString(msg);
            const uint32_t packet_len = 6 + 1 + 3 + msg.size() + 3 + client.mUsername.size() + 4;
            PacketMultipleUserData* responseData = PacketMultipleUserData::createOfSize(packet_len);
            
            uint8_t msgSource = (uint8_t)SOURCE_FRIENDS;
            if (queryCode == DISPATCH_GUIDES)
            {
               // Select more appropriate message type
               if (!client.mPermissionsMask.hasGuideLevel())
                  msgSource = SOURCE_GUIDES;//SOURCE_USER_TO_GUIDE;
               else
                  msgSource = SOURCE_GUIDES;
            }
            
            MemStream packetStream(responseData->size(), responseData->ptr());
            PacketBuilder packet(&packetStream);
            packet.beginBasicPacket(PacketPrivateMessage);
            
            packetStream.write(msgSource);
            packetStream.writeString(client.mUsername.c_str()); // 3+len
            packetStream.writeString(msg.c_str()); // 3+len
            
            packet.commit();
            responseData->mData->mSize = packetStream.getPosition();
            
            if (queryCode == DISPATCH_FRIENDS)
            {
               // To clients friends
               queueUserFriendsMessage(client.shared_from_this(), responseData);
            }
            else if (queryCode == DISPATCH_GUIDES)
            {
               // To guide group
               queueGuideMessage(client.shared_from_this(), responseData);
            }
            
            responseData->free();
         }
         else
         {
            endRPCResponseWithTicket(client, requestTicket, 0, 0xFFFFFFFF);
         }
      }
      
      if (!lookupRequired)
      {
         // Send appropriate response code
         endRPCResponseWithTicket(client, requestTicket, 0, delivered ? RESPONSE_OK : RESPONSE_USER_OFFLINE);
         info.cleanup();
      }
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_PrivateMessage);

class RPC_InstanceHousing : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(8, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   enum
   {
      RESPONSE_OK = 0,
      RESPONSE_ERR = -1
   };
   
   enum
   {
      Query_HousingRequestId = 0x1,
      HousingListResponseId = 0x2
   };
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      // packet should be:
      // uint ??
      // uint ??
      
      // Return packet should be:
      // 0x2 = housing list
      //   uint8 numHouses
      //   each numHouses:
      //     string name
      //     string dispName
      //     string description
      //     uint CC
      //     uint PP
      //     uint MaxFurn
      //     uint8 IconID
      //     uint AccID
      //     string Username
      //     uint8 isFriend
      // 0 = return value
      //    uint code
      
      
      if (info.code == Query_HousingRequestId)
      {
         char queryBuf[512];
         
         int32_t location_id=0;
         int32_t instance_id=0;
         
         stream.readIntBE(location_id);
         stream.readIntBE(instance_id);

         DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT house_plots.id, owner_id, users.username, house_plots.name, house_plots.cc, house_plots.pp, max_furniture, type_id, description, house_plots.poi_name "
         "FROM house_plots LEFT JOIN users ON users.id = house_plots.owner_id "
         "WHERE house_plots.location_id = $1 AND house_plots.instance_id = $2;", (int64_t)location_id, (int64_t)instance_id), false, [this, requestTicket, clientHandler](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
            
            // Send back new resource packet so client knows the id
            if (!ec)
            {
               // names are as big as 64 in db, so we'll use that as the basis
               uint32_t expectedSize = 6 + 6 + 6 + 1 + ( results.size() * (((3 + 64) * 3) + 18) );
               
               MemStream packetStream(expectedSize);
               PacketBuilder packet(&packetStream);
               packet.beginRPC(getRPCID(), requestTicket->getRequestID(), HousingListResponseId, false);
            
               uint8_t numHouses = results.size();
               packetStream.write(numHouses);
               
               OVServerGameConnectionState *state = static_cast<OVServerGameConnectionState*>(clientHandler->mState);
               
               for (auto rowv : results)
               {
                  auto row = rowv.value;
                  uint32_t owner_id = row[1] == NULL ? 0 : DBToU32(row[1]); // owner_id
                  uint8_t is_friend = state->isFriend(owner_id);
                  packetStream.writeString(row[9]); // poi name
                  packetStream.writeString(DBStr(row[3])); // users name
                  packetStream.writeString(row[8]); // description
                  packetStream.writeUintBE(DBToU32(row[4])); // cc
                  packetStream.writeUintBE(DBToU32(row[5])); // pp
                  packetStream.writeUintBE(DBToU32(row[6])); // max_furniture
                  packetStream.write((uint8_t)DBToU32(row[7])); // type
                  packetStream.write(owner_id); // owner_id
                  packetStream.writeString(DBStr(row[2])); // username
                  packetStream.write(is_friend); // is_friend
               }
               
               OVFreeableData* responseData = packet.commitToEncodedPacket(&clientHandler->mEncryptionKey);
               if (responseData)
                  clientHandler->queuePacketData(responseData);
               endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, responseData ? RESPONSE_OK : RESPONSE_ERR);
            }
            else
            {
               endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_ERR);
            }
         });
      }
      else
      {
         endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_ERR);
      }
      
      info.cleanup();
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_InstanceHousing);


class RPC_GetUserHousing : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(9, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   enum
   {
      Query_HouseList = 0x1
   };
   
   enum
   {
      RESPONSE_OK = 0,
      RESPONSE_ERR = -1
   };
   
   enum
   {
      HosingRequestId = 0x1,
      HousingListResponseId = 0x2
   };
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      // request packet:
      // 0x1
      //   string username
      // Return packet should be:
      // 0x2 = housing list
      //   uint8 numHouses
      //   each numHouses:
      //     uint id
      //     uint CC ?? (guess)
      //     uint PP ?? (guess)
      //     uint8 IconID
      //     string locationName
      //     string instanceName
      //     string poiName
      //     string homeName
      // 0 = return value
      //    uint code
      
      // Return code in the packet is uint32, can be:
      // 0 = ok
      // 0xfffffffb = no house
      // 0xfffffffc = denied
      // 0xfffffffd = invalid username
      // The request id needs to match the code otherwise the client will barf up.
      //((uint32_t*)(responseData->mPtr+6+2))[0] = __builtin_bswap32(code);
      
      // We'll just return 0 houses for now
      
      if (info.code == Query_HouseList)
      {
         std::string username;
         if (!stream.readString(username) || strlen(username.c_str()) > 30)
         {
            endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
            return;
         }
         
         DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT house_plots.id, house_plots.cc, house_plots.pp, type_id, location_name, instance_name, poi_name, name "
         "FROM house_plots INNER JOIN users on users.id = house_plots.owner_id WHERE users.username = $1", username.c_str()), false, [this, requestTicket, clientHandler](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
            
            // Send back new resource packet so client knows the id
            if (!ec)
            {
               // names are as big as 64 in db, so we'll use that as the basis
               uint32_t expectedSize = 6 + 6 + 6 + 1 + ( results.size() * (((3 + 64) * 4) + 12) );
               
               MemStream packetStream(expectedSize);
               PacketBuilder packet(&packetStream);
               packet.beginRPC(getRPCID(), requestTicket->getRequestID(), HousingListResponseId, false);
               
               uint8_t numHouses = results.size();
               packetStream.write(numHouses);
               
               // TODO: sort by default flag
               for (auto rowv : results)
               {
                  auto row = rowv.value;
                  packetStream.writeUintBE(DBToU32(row[0])); // id
                  packetStream.writeUintBE(DBToU32(row[1])); // cc
                  packetStream.writeUintBE(DBToU32(row[2])); // pp
                  packetStream.write((uint8_t)DBToU32(row[3])); // type
                  packetStream.writeString(DBStr(row[4])); // location
                  packetStream.writeString(DBStr(row[5])); // instance
                  packetStream.writeString(DBStr(row[6])); // poi_name
                  packetStream.writeString(DBStr(row[7])); // name (set by user)
               }
               
               OVFreeableData* responseData = packet.commitToEncodedPacket(&clientHandler->mEncryptionKey);
               if (responseData)
                  clientHandler->queuePacketData(responseData);
               endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, responseData ? RESPONSE_OK : RESPONSE_ERR);
            }
            else
            {
               endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_ERR);
            }
         });
      }
      else
      {
         endRPCResponseWithTicket(client, requestTicket, 0, RESPONSE_ERR);
      }
      
      info.cleanup();
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_GetUserHousing);


class RPC_EditUserHouse : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(10, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   enum
   {
      Query_Update = 0x1,
      Query_SetDefault = 0x2
   };
   
   enum
   {
      RESPONSE_OK = 0,
      RESPONSE_ERR = -1,
      RESPONSE_NAME_TOO_LONG = 0xfffffffc,
      RESPONSE_INVALID_NAME = 0xfffffffd
   };
   
   virtual void doUpdateName(OVProtocolHandler& client, RPCTicketPtr requestTicket, uint32_t deed_id, const char* newName)
   {
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      DBQueryQueue::getQueue().addToQueueAffected(clientHandler->getStrandRef(), SQLBindPack("UPDATE house_plots SET name = $1 WHERE id = $2", newName, deed_id),
      [this, clientHandler, requestTicket](OVDBConnection* a, const std::error_code &ec, uint64_t affected_rows){
         endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, ec ? RESPONSE_ERR : RESPONSE_OK);
      });
   }
   
   virtual void doSetDefault(OVProtocolHandler& client, RPCTicketPtr requestTicket, uint32_t deed_id)
   {
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      DBQueryQueue::getQueue().addToQueueAffected(clientHandler->getStrandRef(), SQLBindPack("UPDATE users SET default_house_id = $1 WHERE id = $2", client.mUid, deed_id),  [this, clientHandler, requestTicket](OVDBConnection* a, const std::error_code &ec, uint64_t affected_rows){
         endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, ec ? RESPONSE_ERR : RESPONSE_OK);
      });
   }
   
   virtual void handle(OVProtocolHandler& clientHandler, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      
      // request packet:
      // 0x1
      //   string username
      //   string name
      //   ?? (need to check)
      // 0x2 (set default house)
      //   uint id
      
      // Return packet is standard response.
      
      // Return code in the packet is uint32, can be:
      // 0 = ok
      // 0xfffffffc = name too long
      // 0xfffffffd = invalid name
      // The request id needs to match the code otherwise the client will barf up.
      //((uint32_t*)(responseData->mPtr+6+2))[0] = __builtin_bswap32(code);
      
      uint32_t deed_id = 0;
      char house_name_buf[65];
      
      switch (info.code)
      {
         case Query_Update:
            stream.readUintBE(deed_id);
            stream.readString(house_name_buf, 64);
            doUpdateName(clientHandler, requestTicket, deed_id, house_name_buf);
            break;
         case Query_SetDefault:
            stream.readUintBE(deed_id);
            doSetDefault(clientHandler, requestTicket, deed_id);
            break;
         default:
            endRPCResponseWithTicket(clientHandler, requestTicket, 0, RESPONSE_ERR);
            break;
      }
      
      info.cleanup();
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_EditUserHouse);


class RPC_UserPermissions : public RPCPacketHandler
{
public:
   DECLARE_RPC_PACKET_HANDLER(11, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   enum PermEnum
   {
      PERM_PUBLIC=0,
      PERM_FRIENDS=1,
      PERM_PRIVATE=2
   };
   
   enum
   {
      Query_GetPermissionInfo=1,
      Query_PermissionInfo=2,
      Query_SetPermissionInfo=3
   };
   
   enum
   {
      RESPONSE_OK = 0
   };
   
   void doChangePermission(std::shared_ptr<OVProtocolHandler> clientHandler, RPCTicketPtr requestTicket, uint32_t uid, int32_t teleperm, int32_t homeperm)
   {
      char buf2[2048];
      
      SQLPack* query = NULL;
      
      if (teleperm < 0 && homeperm >= 0)
      {
         query = SQLBindPack("UPDATE users SET travel_permission = $1 WHERE id = $2", (int64_t)homeperm, (int64_t)uid);
      }
      else if (homeperm < 0 && teleperm >= 0)
      {
         query = SQLBindPack("UPDATE users SET teleport_permission = $1 WHERE id = $2", (int64_t)teleperm, (int64_t)uid);
      }
      else if (teleperm >= 0 && homeperm >= 0)
      {
         query = SQLBindPack("UPDATE users SET teleport_permission = $1, travel_permission = $2 WHERE id = $3", (int64_t)teleperm, (int64_t)homeperm, (int64_t)uid);
      }
      else
      {
         return;
      }
      
      DBQueryQueue::getQueue().addToQueueAffected(clientHandler->getStrandRef(), query,
         [this, clientHandler, requestTicket](OVDBConnection *async, std::error_code ec, uint64_t affected){
         endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_OK);
      });
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr requestTicket)
   {
      MemStream stream(info.size, info.data);
      std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
      
      // packet should be:
      // 1 = get permissions
      // 2 = get response (server only)
      // 3 = set permissions
      
      // packet 1:
      // [blank]
      //
      // packet 2/3:
      // uint8 allowTeleports (PermEnum)
      // uint8 allowHome (PermEnum)
      
      // return packet either 0x2 or normal 0x0 return code packet
      
      // NOTE: seems to be a bug in the internal client which prevents "allow travel passengers" from being set correctly
      
      if (info.code == Query_GetPermissionInfo)
      {
         // Query
         // TOFIX: handle 0 rows case?
         DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT teleport_permission, travel_permission FROM users WHERE id = $1 LIMIT 1", (int64_t)client.mUid), false, [this,clientHandler, requestTicket](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
            
            if (!ec && results.size() > 0)
            {
               // Send appropriate response code
               auto row = results[0];
               uint8_t val1=(uint8_t)DBToU32(row.value[0]);
               uint8_t val2=(uint8_t)DBToU32(row.value[1]);
               
               uint32_t packet_len = 6 + 6 + 6 + 2;
               
               MemStream packetStream(packet_len);
               PacketBuilder packet(&packetStream);
               packet.beginRPC(getRPCID(), requestTicket->getRequestID(), Query_PermissionInfo, false);
               
               packetStream.write(val1);
               packetStream.write(val2);
               
               packet.commit();
               
               PacketMallocData* responseData = PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr);
               clientHandler->queuePacketData(responseData);
            }
            
            endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_OK);
         });
      }
      else if (info.code == Query_SetPermissionInfo) // SetPermissionInfo
      {
         MemStream stream(info.size, info.data);
         int8_t perm1=0;
         int8_t perm2=0;
         stream.read(perm1);
         stream.read(perm2);
         doChangePermission(clientHandler, requestTicket, clientHandler->mUid, perm1, perm2); // [expends ticket]
      }
      else
      {
         endRPCResponseWithTicket(*(clientHandler.get()), requestTicket, 0, RESPONSE_OK);
      }
      
      info.cleanup();
   }
};
IMPL_RPC_PACKET_HANDLER(RPC_UserPermissions);




// ADMIN PACKETS
// 0x3e9 (1001) requestCatChildren
// 0x3eb () requestMoveCatObject
// 0x3ec requestAddCategory
// 0x3ed requestDeleteCategory
// 0x3ee requestRenameCatObject
// 0x3ef (1007) requestResourceData
// 0x3f1 requestUpdateResource
// 0x3f2 requestDeleteResource

struct ObjectTableLookup
{
   const char* name;
   uint32_t index;
};

static ObjectTableLookup objectTableNames[] = {
   {"", 0},
   {"textures", 1},
   {"shapes", 2},
   {"interiors", 3},
   {"scene_objects", 4},
   {"interior_objects", 5},
   {"clothing_objects", 6},
   {"particle_objects", 7},
   {"light_objects", 8},
   {"interactive_objects", 9},
   {"tool_objects", 10},
   {"pet_objects", 11},
   {"travelmount_objects", 12},
   {"npc_objects", 13}
};

static ObjectTableLookup lookupObjectTableIndex(const char* name)
{
   for (uint32_t i=0; i<sizeof(objectTableNames) / sizeof(&objectTableNames[0]); i++)
   {
      if (strcasecmp(name, objectTableNames[i].name) == 0)
      {
         return objectTableNames[i];
      }
   }
   
   return objectTableNames[0];
}

class ServerErrorPacket : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(1, false, OVProtocolHandler::PROTOCOL_COMMON);
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info)
   {
      client.onCloseAfterWrite();
   }
};
IMPL_PACKET_HANDLER(ServerErrorPacket);


class Admin_RequestCatChildren : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(1001, false, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   void doReturnCatChildren(std::shared_ptr<OVProtocolHandler> clientHandler, uint32_t parent_id, uint32_t folder_id, uint32_t table_id, uint32_t object_id)
   {
      DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack( "SELECT id,name,1 FROM resource_categories WHERE object_id = $1 AND parent_id = $2 UNION ALL SELECT id,name,2 FROM resource_datas WHERE object_id = $1 AND category_id = $2", (int64_t)object_id, (int64_t)parent_id), false, [clientHandler, table_id, folder_id](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
         
         // Create response packet
         if (!ec)
         {
            uint32_t numChildren = results.size();
            uint32_t packetSize = 0;
            OVPacketHeader header;
            
            MemStream outStream(0x4000);
            
            outStream.growPosition(6);
            
            uint16_t folder_len = 0;
            uint16_t object_len = 0;
            
            // Write data
            outStream.writeUintBE((uint16_t)folder_id);
            outStream.writeUintBE(table_id);
            outStream.writeUintBE((uint16_t)0); // Need to correct this
            outStream.writeUintBE((uint16_t)0); // Need to correct this
            
            uint32_t save_pos = outStream.getPosition();
            
            // Grab folders
            for (auto &rowv : results)
            {
               auto row = rowv.value;
               uint32_t type_id = DBToU32(row[2]);
               if (type_id != 1)
                  continue;
               
               uint32_t id = DBToU32(row[0]);
               
               char buf[2048];
               snprintf(buf, sizeof(buf), "%u\t%s\n", id, row[1]);
               outStream.write(strlen(buf), buf);
            }
            
            folder_len = outStream.getPosition() - save_pos;
            save_pos = outStream.getPosition();
            
            // Grab objects
            for (auto &rowv : results)
            {
               auto row = rowv.value;
               uint32_t type_id = DBToU32(row[2]);
               if (type_id != 2)
                  continue;
               
               uint32_t id = DBToU32(row[0]);
               
               char buf[2048];
               snprintf(buf, sizeof(buf), "%u\t%s\n", id, row[1]);
               outStream.write(strlen(buf), buf);
            }
            
            object_len = outStream.getPosition() - save_pos;
            save_pos = outStream.getPosition();
            outStream.growPosition(6 + 2 + 4);
            outStream.writeUintBE(folder_len);
            outStream.writeUintBE(object_len);
            outStream.growPosition(save_pos);
            
            // Write header
            header.setBasicPacket();
            header.setNativeSize(outStream.getPosition()-6);
            header.setNativeId(PacketCatChildren);
            outStream.growPosition(0);
            outStream.write(6, &header);
            
            PacketMallocData* responseData = PacketMallocData::createFromData(save_pos, outStream.mPtr);
            clientHandler->queuePacketData(responseData);
         }
         
      });
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info)
   {
      MemStream stream(info.size, info.data);
      
      // Check permissions
      if (!client.mPermissionsMask.hasSuperAdminLevel())
      {
         return;
      }
      
      // uint32 ??
      // uint16 len (+1)
      // uint32 parentId
      // char[len] name
      
      // response should be
      // 0x3ea (ReceivedCatChildren)
      //   uint16 id  (1)
      //   uint32 tree_control_id
      //   uint16 folder_len (143)
      //   uint16 object_len (0)
      //   char childString[len]
      
      uint32_t parent_id = 0;
      uint16_t folder_id = 0;
      uint32_t table_id = 0;
      char category[46];
      
      // Check permissions
      if (!client.mPermissionsMask.hasSuperAdminLevel())
      {
         return;
      }
      
      stream.readUintBE(parent_id); // db id (0 if root)
      stream.readUintBE(folder_id);   // folder node to insert to
      stream.readUintBE(table_id);    // destination control in client
      
      uint32_t name_len = info.size - stream.getPosition();
      if (name_len <= 45 && true)
      {
         stream.read(name_len, category);
         category[name_len] = '\0';
         
         ObjectTableLookup object = lookupObjectTableIndex(category);
         if (object.index > 0)
         {
            AdminQueryQueue::getQueue().addToQueue(this, std::bind(&Admin_RequestCatChildren::doReturnCatChildren, this, client.shared_from_this(), parent_id, folder_id, table_id, object.index));
         }
      }
      
      info.cleanup();
   }
};
IMPL_PACKET_HANDLER(Admin_RequestCatChildren);

class Admin_RequestCatMoveObject : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(1003, false, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   void doMoveObject(uint32_t srcId, uint32_t destId, uint32_t category_id, bool is_folder)
   {
      char buf2[2048];
      
      SQLPack* query = NULL;
      
      if (is_folder)
      {
         query = SQLBindPack("UPDATE resource_categories SET parent_id = $1 WHERE id = $2 AND object_id = $3", (int64_t)destId, (int64_t)srcId, (int64_t)category_id);
      }
      else
      {
         query = SQLBindPack("UPDATE resource_datas SET category_id = $1 WHERE id = $2 AND object_id = $3", (int64_t)destId, (int64_t)srcId, (int64_t)category_id);
      }
      
      DBQueryQueue::getQueue().addToQueueAffected(DBQueryQueue::getStrandRef(), query,
         &DBQueryQueue::defaultBasicHandler);
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info)
   {
      MemStream stream(info.size, info.data);
      
      // Check permissions
      if (!client.mPermissionsMask.hasSuperAdminLevel())
      {
         return;
      }
      
      // Request packet should be of the form:
      // uint8 isFolder(bool)
      // uint32 srcDBID
      // uint32 destDBID
      // char str[len]
      
      uint8_t isFolder = 0;
      uint32_t srcID = 0;
      uint32_t destParentID = 0;
      char category[46];
      
      stream.read(isFolder);
      stream.readUintBE(srcID);
      stream.readUintBE(destParentID);
      
      uint32_t cat_len = info.size - stream.getPosition();
      
      if (cat_len == 0 || cat_len > 45)
      {
         // Ignore
         Log::client_warnf(client, LogEntry::General, "Tried to add move object in invalid category");
      }
      else
      {
         stream.read(cat_len, &category[0]);
         
         category[cat_len] = '\0';
         
         ObjectTableLookup object = lookupObjectTableIndex(category);
         if (object.index > 0)
         {
            AdminQueryQueue::getQueue().addToQueue(this, std::bind(&Admin_RequestCatMoveObject::doMoveObject, this, srcID, destParentID, object.index, isFolder));
         }
      }
      
      info.cleanup();
   }
};
IMPL_PACKET_HANDLER(Admin_RequestCatMoveObject);

class Admin_RequestAddCategory : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(1004, false, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   void doAddCategory(uint32_t object_id, uint32_t parent_id, std::string name)
   {
      DBQueryQueue::getQueue().addToQueueAffected(DBQueryQueue::getStrandRef(), SQLBindPack("INSERT INTO resource_categories(object_id, parent_id, name) VALUES ($1, $2, $3)", (int64_t)object_id, (int64_t)parent_id, name.c_str()),
         &DBQueryQueue::defaultBasicHandler);
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info)
   {
      MemStream stream(info.size, info.data);
      
      // Check permissions
      if (!client.mPermissionsMask.hasSuperAdminLevel())
      {
         return;
      }
      
      // uint32 ??
      // uint16 len (+1)
      // uint32 parentId
      // char[len] category_len
      // char[size-len] name
      
      uint8_t cat_len = 0;
      uint32_t parentId = 0;
      char category[46];
      char name[46];
      
      stream.readUintBE(parentId);
      stream.read(cat_len);
      
      int32_t name_len = ((int32_t)info.size) - ((int32_t)cat_len) - 5;
      
      if (cat_len > 45 || cat_len <= 1 || name_len <= 0)
      {
         // Ignore
         Log::client_warnf(client, LogEntry::General, "Tried to add invalid category");
      }
      else
      {
         stream.read(cat_len, &category[0]);
         stream.read(name_len, &name[0]);
         
         category[cat_len] = '\0';
         name[name_len] = '\0';
         
         ObjectTableLookup object = lookupObjectTableIndex(category);
         if (object.index > 0)
         {
            AdminQueryQueue::getQueue().addToQueue(this, std::bind(&Admin_RequestAddCategory::doAddCategory, this, object.index, parentId, std::string(name)));
         }
      }
      
      info.cleanup();
   }
};
IMPL_PACKET_HANDLER(Admin_RequestAddCategory);

class Admin_RequestDeleteCategory : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(1005, false, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   void doDeleteCategory(uint32_t category_id)
   {
      DBQueryQueue::getQueue().addToQueueAffected(DBQueryQueue::getStrandRef(), SQLBindPack("DELETE FROM resource_categories WHERE id = $1", (int64_t)category_id),
         &DBQueryQueue::defaultBasicHandler);
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info)
   {
      MemStream stream(info.size, info.data);
      
      // Check permissions
      if (!client.mPermissionsMask.hasSuperAdminLevel())
      {
         return;
      }
      
      uint32_t category_id = 0;
      stream.readUintBE(category_id);
      
      AdminQueryQueue::getQueue().addToQueue(this, std::bind(&Admin_RequestDeleteCategory::doDeleteCategory, this, category_id));
      
      info.cleanup();
   }
};
IMPL_PACKET_HANDLER(Admin_RequestDeleteCategory);

class Admin_RequestRenameCatObject : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(1006, false, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   void doRenameObject(uint32_t src_id, std::string new_name, uint32_t object_id, bool is_folder)
   {
      SQLPack* query = NULL;
      
      if (is_folder)
      {
         query = SQLBindPack("UPDATE resource_categories SET name=$1 WHERE id = $2 AND object_id = $3", new_name.c_str(), (int64_t)src_id, (int64_t)object_id);
      }
      else
      {
         query = SQLBindPack("UPDATE resource_datas SET name=$1 WHERE id = $2 AND object_id = $3", new_name.c_str(), (int64_t)src_id, (int64_t)object_id);
      }
      
      DBQueryQueue::getQueue().addToQueueAffected(DBQueryQueue::getStrandRef(), query,
         &DBQueryQueue::defaultBasicHandler);
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info)
   {
      MemStream stream(info.size, info.data);
      
      // Check permissions
      if (!client.mPermissionsMask.hasSuperAdminLevel())
      {
         return;
      }
      
      // Packet should be of format:
      // uint32 catId
      // uint8 isFolder
      // uint8 catIdLen
      // char name[len]
      
      uint8_t isFolder = 0;
      uint32_t srcID = 0;
      uint8_t cat_len = 0;
      char category[46];
      char new_name[46];
      
      stream.readUintBE(srcID);
      stream.read(isFolder);
      stream.read(cat_len);
      
      uint32_t new_name_len = info.size - (stream.getPosition() + cat_len);
      
      if (new_name_len == 0 || new_name_len > 45 || cat_len == 0 || cat_len > 45)
      {
         // Ignore
         Log::client_warnf(client, LogEntry::General, "Tried to rename an object badly");
      }
      else
      {
         stream.read(cat_len, &category[0]);
         stream.read(new_name_len, &new_name[0]);
         
         category[cat_len] = '\0';
         new_name[new_name_len] = '\0';
         
         ObjectTableLookup object = lookupObjectTableIndex(category);
         if (object.index > 0)
         {
            AdminQueryQueue::getQueue().addToQueue(this, std::bind(&Admin_RequestRenameCatObject::doRenameObject, this, srcID, std::string(new_name), object.index, isFolder));
         }
      }
      
      info.cleanup();
   }
};
IMPL_PACKET_HANDLER(Admin_RequestRenameCatObject);

class Admin_RequestResourceData : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(1007, false, OVProtocolHandler::PROTOCOL_GAME_COMMON);

   static void json_dump_string_safe(MemStream& out, json_t* value)
   {
      if (json_is_string(value))
      {
         const char* str = json_string_value(value);
         out.write(strlen(str), str);
      }
      else
      {
         char* tmp = json_dumps(value, JSON_ENCODE_ANY);
         if (!tmp)
            return;
         out.write(strlen(tmp),tmp);
         free(tmp);
      }
      
   }
   
   static void doSendResource(std::shared_ptr<OVProtocolHandler> clientHandler, uint32_t table_id, uint32_t resource_id, uint32_t object_id, bool json_format)
   {
      DBQueryQueue::getQueue().addToQueueRows(clientHandler->getStrandRef(), SQLBindPack("SELECT name, data FROM resource_datas WHERE id = $1 AND object_id = $2 LIMIT 1", (int64_t)resource_id, (int64_t)object_id), false, [clientHandler, table_id, resource_id, object_id, json_format](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
         
         if (!ec && results.size() != 0)
         {
            MemStream outStream(0x4000);
            ObjectTableLookup lookup = objectTableNames[object_id];
            auto rowv = results[0];
            
            outStream.growPosition(6);
            outStream.writeUintBE(table_id);
            outStream.write((uint8_t)strlen(lookup.name));
            outStream.write(strlen(lookup.name), lookup.name); // type name
            
            // Write data
            uint32_t act_len = rowv.lengths[1];
            if (act_len > 0)
            {
               if (rowv.value[1][act_len-1] == '\n')
               {
                  act_len--;
               }
            }
            outStream.write(act_len, rowv.value[1]);

            json_error_t error;
            json_t *root = json_loads(rowv.value[1], 0, &error);

            if (!root)
            {
               // NOP
            }
            else if (json_format)
            {
               // We'll need to insert the id
               json_object_set(root, "id", json_integer(resource_id));

               char* outS = json_dumps(root, 0);
               outStream.write(strlen(outS), outS);
               free(outS);
            }
            else
            {
               // We'll need to enumerate the JSON as string records
               // Write id and name
               char buf[2048];
               snprintf(buf, sizeof(buf), "\nid\t%u\n", resource_id);
               outStream.write(strlen(buf), buf);
               const char *key;
               json_t *value;
               const char *skey;
               json_t *svalue;

               if (root)
               {
                  bool fieldWritten = false;

                  json_object_foreach(root, key, value) {
                     if (json_is_object(value)) // KVP
                     {
                        // Start
                        snprintf(buf, sizeof(buf), "\n%s\t", key);
                        outStream.write(strlen(buf), buf);

                        // KVP
                        fieldWritten = false;
                        json_object_foreach(value, skey, svalue) {
                           snprintf(buf, sizeof(buf), "%s=", skey);
                           outStream.write(strlen(buf), buf);
                           json_dump_string_safe(outStream, svalue);
                           outStream.write(',');
                           fieldWritten = true;
                        }

                        // End
                        if (fieldWritten)
                           outStream.mPtr--;
                        outStream.write('\n');
                     }
                     else if (json_is_array(value)) // array of string values
                     {
                        // Start
                        snprintf(buf, sizeof(buf), "\n%s\t", key);
                        outStream.write(strlen(buf), buf);

                        // Value(s)
                        fieldWritten = false;
                        int len = (int)json_array_size(value);
                        for (int i=0; i<len; i++)
                        {
                           json_t* avalue = json_array_get(value, i);
                           json_dump_string_safe(outStream, svalue);
                           outStream.write('\t');
                           fieldWritten = true;
                        }

                        // End
                        if (fieldWritten)
                           outStream.mPtr--;
                        outStream.write('\n');
                     }
                     else // presumed string
                     {
                        snprintf(buf, sizeof(buf), "\n%s\t", key);
                        outStream.write(strlen(buf), buf);
                        json_dump_string_safe(outStream, svalue);
                        outStream.write('\n');
                     }

                     fieldWritten = true;
                  }
                  if (fieldWritten)
                     outStream.mPtr--;
               }
            }

            json_decref(root);
            
            uint32_t save_size = outStream.getPosition();
            outStream.setPosition(0);
            
            OVPacketHeader header;
            header.setBasicPacket();
            header.setNativeId(PacketResourceData);
            header.setNativeSize(save_size-6);
            
            outStream.write(6, &header);
            
            clientHandler->queuePacketData(PacketMallocData::createFromData(save_size, outStream.mPtr));
         }
         
      });
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info)
   {
      MemStream stream(info.size, info.data);
      
      // Packet format should be:
      // uint32 table_id
      // uint32 resource_id
      // char category_name[remaining size]
      
      // Return packet should be:
      // 0x3f0 (ReceivedResourceData)
      // uint32 table_id
      // uint8 category_size
      // char category[category_size]
      // char data[remaining size]
      
      // Check permissions
      if (!client.mPermissionsMask.hasSuperAdminLevel())
      {
         return;
      }
      
      uint8_t cat_len = 0;
      uint32_t table_id = 0;
      uint32_t resource_id = 0;
      char category[46];
      bool is_json = false;
      
      stream.readUintBE(table_id);
      stream.readUintBE(resource_id);
      cat_len = info.size - stream.getPosition();
      
      if (cat_len == 0 || cat_len > 45)
      {
         // Ignore
         Log::client_warnf(client, LogEntry::General, "Tried to rename an object badly");
      }
      else
      {
         stream.read(cat_len, &category[0]);
         category[cat_len] = '\0';

         char* fmt = strrchr(category, '.');
         if (fmt != NULL)
         {
            *fmt = '\0';
            if (strcasecmp(fmt+1, "json") != 0)
            {
               is_json = true;
            }
         }
         
         ObjectTableLookup object = lookupObjectTableIndex(category);
         if (object.index > 0)
         {
            AdminQueryQueue::getQueue().addToQueue(this, std::bind(&Admin_RequestResourceData::doSendResource, client.shared_from_this(), table_id, resource_id, object.index, is_json));
         }
      }
      
      info.cleanup();
   }
};
IMPL_PACKET_HANDLER(Admin_RequestResourceData);

class Admin_RequestUpdateResource : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(1009, false, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   template<typename T> void enumerateFieldValues(const char* data, uint32_t max_len, T func)
   {
      char fieldName[256];
      char* params[64];
      
      const char* fieldStart = data;
      bool fieldData = false;
      
      for (const char* ptr = data; (ptr - data) < max_len; ptr++)
      {
         char c = *ptr;
         if (c == '\t')
         {
            uint32_t fieldLen = ptr - fieldStart;
            fieldLen = fieldLen > 255 ? 255 : fieldLen;
            memcpy(fieldName, ptr - fieldLen, fieldLen);
            fieldName[fieldLen] = '\0';
            fieldStart = ptr+1;
            fieldData = true;
         }
         else if (c == '\n' || c == '\0')
         {
            uint32_t fieldLen = ptr - fieldStart;
            if (fieldData)
            {
               func(fieldName, fieldStart, fieldLen);
            }
            fieldStart = ptr+1;
            fieldData = false;
         }
      }
   }
   
   void doUpdateResource(std::shared_ptr<OVProtocolHandler> clientHandler, uint32_t resource_id, uint8_t object_id, std::string data, bool is_json)
   {
      char buf2[2048];
      char objName[256];
      char *buf = NULL;
      uint32_t category_id = 0;
      
      MemStream queryStream(1024 + data.size());
      objName[0] = '\0';
      
      char *objNamePtr = &objName[0];
      MemStream* queryStreamPtr = &queryStream;
      
      SQLPack* query = NULL;
      
      
      if (!is_json)
      {
         Log::client_warnf(*clientHandler.get(), LogEntry::General, "Non-JSON update no longer supported (ignoring)");
         return;
      }
      else
      {
         json_error_t error;
         json_t *root = json_loads(data.c_str(), 0, &error);
         if (root)
         {
            // Get name
            const char* str = json_string_value(json_object_get(root, "name"));
            if (str)
            {
               strncpy(&objName[0], str, sizeof(objName)-1);
               objName[255] = '\0';
               queryStream.write(data.size(), data.c_str());
            }
            json_decref(root);
         }
      }
      
      queryStream.write((uint8_t)0);
      
      if (resource_id == 0)
      {
         query = SQLBindPack("INSERT INTO resource_datas(name, object_id, data, category_id) VALUES ($1, $2, $3, $4)\n", objName, (int64_t)object_id, SQLBlobInfo(queryStream.mPtr, strlen((const char*)queryStream.mPtr)), (int64_t)category_id);
      }
      else
      {
         query = SQLBindPack("UPDATE resource_datas SET name=$1 data=$2 WHERE id=%3 AND object_id=$4", objName, SQLBlobInfo(queryStream.mPtr, strlen((const char*)queryStream.mPtr)), (int64_t)resource_id, (int64_t)object_id);
      }
      
      DBQueryQueue::getQueue().addToQueueInsert(clientHandler->getStrandRef(), query, [this, clientHandler, resource_id, object_id, is_json](OVDBConnection* async, const std::error_code &ec, uint64_t insert_id){
         
         // Send back new resource packet so client knows the id
         if (!ec && resource_id == 0)
         {
            uint32_t new_id = (uint32_t)insert_id;
            AdminQueryQueue::getQueue().addToQueue(this, std::bind(&Admin_RequestResourceData::doSendResource, clientHandler, 0, new_id, object_id, is_json));
         }
         
      });
      
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info)
   {
      MemStream stream(info.size, info.data);
      
      // Packet format should be:
      // uint8 category_size  (also last bit is update flag)
      // uint32 resource_id
      // char category_name[category_size]
      // char data[remaining size]
      
      uint8_t cat_len=0;
      uint32_t resource_id=0;
      char category[46];
      bool is_json = false;
      
      // Check permissions
      if (!client.mPermissionsMask.hasSuperAdminLevel())
      {
         return;
      }
      
      stream.read(cat_len);
      stream.readUintBE(resource_id);
      bool is_update = (cat_len & (1<<7)) != 0;
      cat_len &= 0x7F;
      
      int32_t data_len = (int32_t)info.size - (int32_t)(stream.getPosition() + cat_len);
      
      if (data_len <= 0 || cat_len == 0 || cat_len > 45)
      {
         // Ignore
         Log::client_warnf(client, LogEntry::General, "Tried to update an object badly");
      }
      else
      {
         stream.read(cat_len, &category[0]);
         category[cat_len] = '\0';

         char* fmt = strrchr(category, '.');
         if (fmt != NULL)
         {
            *fmt = '\0';
            if (strcasecmp(fmt+1, "json") != 0)
            {
               is_json = true;
            }
         }

         std::string data(data_len, 'x');
         stream.read(data_len, &data[0]);
         
         ObjectTableLookup object = lookupObjectTableIndex(category);
         if (object.index > 0)
         {
            AdminQueryQueue::getQueue().addToQueue(this, std::bind(&Admin_RequestUpdateResource::doUpdateResource, this, client.shared_from_this(), resource_id, object.index, data, is_json));
         }
      }
      
      
      info.cleanup();
   }
};
IMPL_PACKET_HANDLER(Admin_RequestUpdateResource);

class Admin_RequestDeleteResource : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(1010, false, OVProtocolHandler::PROTOCOL_GAME_COMMON);
   
   void doDeleteResource(uint32_t resource_id, uint32_t object_id)
   {
      DBQueryQueue::getQueue().addToQueueAffected(DBQueryQueue::getStrandRef(), SQLBindPack("DELETE FROM resource_datas WHERE id = $1 AND object_id = $2", (int64_t)resource_id, (int64_t)object_id), &DBQueryQueue::defaultBasicHandler);
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info)
   {
      MemStream stream(info.size, info.data);
      
      // Check permissions
      if (!client.mPermissionsMask.hasSuperAdminLevel())
      {
         return;
      }
      
      // Packet format should be:
      // uint32 resource_id
      // char category_name[remaining size]
      
      uint32_t resource_id;
      char category[46];
      stream.readUintBE(resource_id);
      
      uint32_t cat_len = info.size - stream.getPosition();
      if (cat_len == 0 || cat_len > 45)
      {
         // Ignore
         Log::client_warnf(client, LogEntry::General, "Tried to delete an object badly");
      }
      else
      {
         stream.read(cat_len, &category[0]);
         category[cat_len] = '\0';
         
         ObjectTableLookup object = lookupObjectTableIndex(category);
         if (object.index > 0)
         {
            AdminQueryQueue::getQueue().addToQueue(this, std::bind(&Admin_RequestDeleteResource::doDeleteResource, this, resource_id, object.index));
         }
      }
      
      info.cleanup();
   }
};
IMPL_PACKET_HANDLER(Admin_RequestDeleteResource);



