/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef packetHandlers_hpp
#define packetHandlers_hpp

#include <stdio.h>
#include <stdint.h>

#include <asio.hpp>
#include <atomic>
#include <iostream>
#include <unordered_map>

#include "packet.hpp"
#include "RPCTicket.hpp"

#define DECLARE_PACKET_HANDLER(id_code, is_auto, protocol) \
virtual uint32_t getPacketHash() { return ((uint32_t)id_code) | (((uint32_t)is_auto) << 16) | 0x8000000; } \
virtual uint32_t getPacketCode() { return id_code; } \
virtual uint32_t getPacketAuto() { return is_auto; } \
virtual uint8_t getPacketProtocol() { return protocol; }

#define DECLARE_RPC_PACKET_HANDLER(id_code, protocol) \
virtual uint16_t getRPCID() { return id_code; } \
virtual uint8_t getRPCProtocol() { return protocol; } \
virtual const char* getName() { return typeid(this).name(); }

#define IMPL_PACKET_HANDLER(HandlerType) HandlerType g##HandlerType; \
PacketHandlerRegister<HandlerType> g##HandlerType##Register(&g##HandlerType);

#define IMPL_RPC_PACKET_HANDLER(HandlerType) HandlerType g##HandlerType; \
RPCPacketHandlerRegister<HandlerType> g##HandlerType##Register(&g##HandlerType);

class OVProtocolHandler;
class OVPacketHeader;

class PacketHandler
{
public:
   PacketHandler() {;}
   virtual ~PacketHandler() {;}
   
   virtual int preHandle(OVProtocolHandler& client, OVPacketHeader* header) { return 1; }
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info) = 0;
   virtual uint32_t getPacketHash() = 0;
   virtual uint32_t getPacketCode() = 0;
   virtual uint8_t getPacketProtocol() = 0;
   
   static PacketHandler* getHandlerForPacket(OVPacketHeader* header);
   
   static void registerKlass(PacketHandler* klass);
   static void initKlasses();
   
   static PacketHandler* gRoot;
   PacketHandler* mListNext;
};

class RPCPacketHandler
{
public:
   RPCPacketHandler() {;}
   virtual ~RPCPacketHandler() {;}
   
   virtual RPCTicketPtr fire(OVProtocolHandler& client) { return RPCTicketPtr(); }
   
   RPCTicketPtr createFireTicket(OVProtocolHandler& client);
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info, RPCTicketPtr ticket) = 0;
   virtual uint16_t getRPCID() { return (uint16_t)-1; }
   virtual uint8_t getRPCProtocol() = 0;
   
   virtual void onCancel(OVProtocolHandler& client, RPCTicketPtr &ticket) {;}
   
   void endRPCResponseWithTicket(OVProtocolHandler& client, RPCTicketPtr ticket, uint16_t packet_code, uint32_t response_code, bool is_auto=false);
   void createRPCResponseWithoutTicket(OVProtocolHandler& client, uint32_t request_id, uint16_t packet_code, uint32_t response_code, bool is_auto=false);
   
   static RPCPacketHandler* getHandlerForRPCCommand(uint16_t code);
   
   static void registerKlass(RPCPacketHandler* klass);
   static void initKlasses();
   
   virtual const char* getName() = 0;
   
   static RPCPacketHandler* gRoot;
   RPCPacketHandler* mListNext;
};

template<class T> struct PacketHandlerRegister
{
   PacketHandlerRegister(T* klass)
   {
      PacketHandler::registerKlass(klass);
   }
};

template<class T> struct RPCPacketHandlerRegister
{
   RPCPacketHandlerRegister(T* klass)
   {
      RPCPacketHandler::registerKlass(klass);
   }
};

// Hack
extern int gForceLocationIp;
extern uint8_t gForceLocationIpAddress[4];

#endif /* packetHandlers_hpp */
