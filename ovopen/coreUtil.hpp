/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef coreUtil_hpp
#define coreUtil_hpp

#include <stdlib.h>
#include <stdint.h>
#include <string>
#include <vector>
#include <cstring>

inline int hexchr2bin(const char hex, char *out)
{
   if (out == NULL)
      return 0;
   
   if (hex >= '0' && hex <= '9') {
      *out = hex - '0';
   } else if (hex >= 'A' && hex <= 'F') {
      *out = hex - 'A' + 10;
   } else if (hex >= 'a' && hex <= 'f') {
      *out = hex - 'a' + 10;
   } else {
      return 0;
   }
   
   return 1;
}

size_t hexs2bin(const char *hex, unsigned char *out, size_t max_len);
size_t bin2hex(const unsigned char *bin, size_t blen, char *hex);

namespace StringUtil
{
   inline std::string joinStrings(std::vector<std::string> list, std::string sep)
   {
      int sepLen = sep.size();
      int totalLen = 0;
      int count = 0;
      for (auto itr : list)
      {
         totalLen += itr.size();
         totalLen += sepLen;
         count++;
      }

      if (totalLen != 0)
      {
         totalLen -= sepLen;
      }

      std::string outStr;
      outStr.reserve(totalLen);

      for (auto itr : list)
      {
         count--;
         outStr.append(itr);

         if (count > 0)
         {
            outStr.append(sep);
         }
      }

      return outStr;
   }
   
   // Info class to optimize string retrieval
   class StringSplitter
   {
   public:
      StringSplitter() : scanStr(NULL) {;}
      ~StringSplitter() {;}
      
   protected:
      typedef struct StringInfo
      {
         uint32_t ptr;
         uint32_t size;
         uint8_t sep;
      } StringInfo;
      
      std::vector<StringInfo> infos;
      const char* scanStr;
      
   public:

      int scan(const char *string, const char *sep)
      {
         infos.clear();
         
         scanStr = string;
         
         uint8_t last = 0;
         const char* start = string;
         const char *prev = string;
         StringInfo info;
         while(*string)
         {
            last = *string++;
            
            for(uint32_t i =0; sep[i]; i++)
            {
               if(last == sep[i])
               {
                  info.size = (string-1) - prev;
                  info.ptr = prev - start;
                  info.sep = last;
                  infos.push_back(info);
                  prev = string;
                  last = 0;
                  break;
               }
            }
         }
         if(last) {
            info.size = string - prev;
            info.ptr = prev - start;
            info.sep = 0;
            infos.push_back(info);
         }

         return infos.size();
      }
      
      int getUnitCount() const { return infos.size(); }

      char getUnit(uint32_t index, char *buffer, uint32_t bufferSize) const
      {
         if (index >= 0 && index < infos.size()) {
            uint32_t sz = infos[index].size > bufferSize-1 ? bufferSize-1 : infos[index].size;
            strncpy(buffer, scanStr + infos[index].ptr, sz);
            buffer[sz] = '\0';
            return infos[index].sep;
         } else {
            buffer[0] = '\0';
         }
         return 0;
      }

      template<uint32_t bufSize> std::vector<std::string> toList()
      {
         std::vector<std::string> outList;
         for (uint32_t i=0; i<infos.size(); i++)
         {
            char buffer[bufSize];
            getUnit(i, buffer, sizeof(buffer));
            outList.push_back(buffer);
         }
         return outList;
      }
   };

};


#endif /* coreUtil_hpp */
