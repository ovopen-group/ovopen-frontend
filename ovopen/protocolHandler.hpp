/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <asio.hpp>
#include <atomic>
#include <iostream>
#include <unordered_map>
#include <zlib.h>
#include "log.hpp"
#include <functional>

#include "packet.hpp"
#include "coreTypes.hpp"
#include "RPCTicket.hpp"

class OVProtocolHandler;
class OVPacketHeader;

using asio::ip::tcp;

// Base class for protocol handler

template<int BufferSize>
class PacketIterator
{
public:
   uint8_t* buffer;
   uint32_t pos;
   uint32_t end;
   uint32_t processed_offset;
   BLOWFISH_CONTEXT* encKey;
   
   bool invalid_stream;
   
   PacketIterator(uint8_t* _buffer, uint32_t _start, uint32_t _end, uint32_t _proc_bytes, BLOWFISH_CONTEXT* _encKey) : buffer(_buffer), pos(_start), end(_end), processed_offset(_proc_bytes), encKey(_encKey), invalid_stream(false) {;}
   
   void reset(uint32_t size, uint32_t procofs=0)
   {
      pos = 0;
      end = size;
      processed_offset = procofs;
      invalid_stream = false;
   }
   
   void delta_pos(uint32_t value)
   {
      pos += value;
      pos = std::min(pos, end);
   }
   
   OVPacketHeader* next_packet()
   {
      if (eof())
         return NULL;
      
      OVPacketHeader* header = (OVPacketHeader*)ptr();
      
      uint32_t requiredBytes=0;
      if (header->isValid())
      {
         // If the packet hasn't been fully processed yet, do extra verification
         requiredBytes = header->getPacketSize()+6;
         if ((pos+requiredBytes) > processed_offset)
         {
            if (!header->isValidUnprocessed())
            {
               invalid_stream = true;
               return NULL;
            }
         }
      }
      else
      {
         // Skip this garbage
         invalid_stream = true;
         return NULL;
      }
      
      requiredBytes = header->getPacketSize()+6;
      if (requiredBytes > OVPacketHeader::MaxPacketSize || requiredBytes > BufferSize)
      {
         // Skip this garbage
         invalid_stream = true;
         return NULL;
      }
      
      if (bytes_left() >= requiredBytes)
      {
         // If we haven't processed this packet yet, decrypt it if its encrypted
         if ((pos+requiredBytes) > processed_offset)
         {
            header->setProcessed();
            if (header->isEncrypted())
            {
               DecryptPacketInPlace(header, encKey);
            }
            processed_offset = pos + header->getPacketSize() + 6;
         }
      }
      else
      {
         return NULL;
      }
      
      pos += header->getPacketSize() + 6;
      return header;
   }
   
   // Dispose packet in stream
   void dispose_packet(OVPacketHeader* header)
   {
      uint32_t packet_size = header->getPacketSize()+6;
      uint32_t start_pos = ((uint8_t*)header) - buffer;
      uint32_t end_pos = start_pos + packet_size;
      
      dispose_buffer(start_pos, end_pos);
   }
   
   // Dispose buffer range
   void dispose_buffer(uint32_t _start, uint32_t _end)
   {
      // Verify range
      _end = std::min(_end, end);
      _start = std::min(_start, _end);
      if (_start == _end)
         return;
      
      // Copy bytes after end to start
      uint8_t* start_ptr = buffer + _start;
      uint8_t* end_ptr = buffer + _end;
      if (end-_end > 0)
      {
         memcpy(start_ptr, end_ptr, end-_end);
      }
      
      // Clip out end
      end -= (_end-_start);
      
      // Clip out processed range
      if (processed_offset > _start)
      {
         int new_pos = processed_offset;
         new_pos -= (_end-_start);
         processed_offset = (uint32_t)(std::max(new_pos, (int)_start));
      }
      
      // Clip out pos
      if (pos > _start)
      {
         int new_pos = pos;
         new_pos -= (_end-_start);
         pos = (uint32_t)(std::max(new_pos, (int)_start));
      }
   }
   
   void* ptr() { return buffer+pos; }
   bool eof() { return ((!invalid_stream) && ((end-pos) == 0)); }
   bool valid() { return !invalid_stream; }
   uint32_t bytes_left() { return end-pos; }
   uint32_t bytes_writable() { return BufferSize-end; }
};

// Does basic packet handling in read buffer. Split into struct for tests.
struct ReadBufferProcessor
{
public:
   int32_t mBytesReadDuringWindow;
   uint64_t mLastWindowTime;
   uint64_t mLastPacketTime;
   
   ReadBufferProcessor() : mBytesReadDuringWindow(0), mLastWindowTime(0), mLastPacketTime(0) {;}
   
   void reset()
   {
      mBytesReadDuringWindow = 0;
      mLastWindowTime = 0;
      mLastPacketTime = 0;
   }
   
   inline void addBytesRead(uint32_t bytes)
   {
      mBytesReadDuringWindow += bytes;
   }
   
   inline void updateLimiter(uint64_t now)
   {
      if (mLastWindowTime - now < 1000)
         return;
      
      mBytesReadDuringWindow = 0;
      mLastWindowTime = now;
   }
   
   uint32_t getTimeUntilNextWindow(uint64_t now)
   {
      uint64_t nextTime = mLastWindowTime+1000;
      if (now > nextTime)
         return (uint32_t)now;
      else
         return (uint32_t)(nextTime - now);
   }
   
   // Whats currently happening in read buffer land
   enum ReadState : uint8_t
   {
      READ_IDLE=0,        // Doing nothing
      READ_READING=1,     // Reading data
      READ_LIMITED=2,     // Need to wait for timing window
      READ_BUFFER_FULL=3, // Need to wait for packets to finish,
      READ_SCHEDULED=4,   // We scheduled a re-read
      READ_PROCESSING=5   // We are processing packet data
   };
   
   template<class T, class T2> bool processData(T* handler, T2& packetItr, uint64_t now)
   {
      if (handler->isClosing())
         return false;
      
      OVPacketHeader* packet = NULL;
      updateLimiter(now);
      
      /*
       NOTE: We iterate through the packets in the buffer. If a packet doesn't want to be handled yet,
       the handler will return "false" and it will be left in the buffer for the next read. It will be
       read during subsequent callbacks until it has been marked as finished with.
       */
      
      while (!packetItr.eof())
      {
         packet = packetItr.next_packet();
         if (!packetItr.valid())
         {
            handler->onJunkPacket();
            handler->onClose();
            return false;
         }
         
         // No packet? abort!
         if (!packet)
            break;
         
         // Perform handler
         bool handled = handler->onPacket(packet);
         
         if (handler->isClosing())
            return false;
         
         // Clear out anything which has been dealt with (which will move end)
         if (handled)
         {
            packetItr.dispose_packet(packet);
         }
      }
      
      return true;
   }
   
   template<class T, class T2> ReadState enqueueRead(T* handler, T2& packetItr)
   {
      const uint32_t minBytes = 6;
      if (handler->isReading())
         return READ_READING;
      
      uint32_t bytesCanRead = packetItr.bytes_writable();
      
      // Also restrict to maximum per window
      uint32_t windowBytesLeft = std::max((int)handler->getReadWindowLimit()-(int)mBytesReadDuringWindow, 0);
      uint32_t bytesWillRead = std::min(windowBytesLeft, bytesCanRead);
      
      ReadState ret = READ_READING;
      
      if (bytesWillRead == 0)
      {
         if (bytesCanRead == 0)
            ret = READ_BUFFER_FULL;
         else
            ret = READ_LIMITED;
      }
      else
      {
         handler->doRead(packetItr.end, bytesWillRead);
      }
      
      return ret;
   }
};

template<int BufferSize>
class PacketReadBuffer
{
public:
   uint32_t mReadHead;
   uint32_t mProcessedHead; // Need to keep track of this for idle restart
   uint8_t mData[BufferSize];
   
   inline uint32_t getReadBytes() { return mReadHead; }
   inline uint32_t getProcessedBytes() { return mProcessedHead; }
   inline uint32_t getBytesLeft() { return BufferSize - mReadHead; }
   inline uint8_t* getReadBufferHeadPtr() { return &mData[mReadHead]; }
   inline uint8_t* getReadBufferPtr() { return &mData[0]; }
   
   PacketReadBuffer() : mReadHead(0), mProcessedHead(0) {;}
};

enum FriendState : uint8_t
{
   FRIENDSTATE_OFFLINE=0,
   FRIENDSTATE_ONLINE=1,
   FRIENDSTATE_REQUIRE_ACCEPT=2,
   FRIENDSTATE_PENDING=3,
   FRIENDSTATE_IGNORE=4,   // NOTE: should prevent teleports and messages
   // -- States before this require listing --
   FRIENDSTATE_BEING_IGNORED=0xFE, // special code for being ignored by user
   FRIENDSTATE_DELETE=0xFF
};

// Friend status record
struct FriendStatusRecord
{
   uint32_t uid;
   FriendState state;
   
   FriendStatusRecord() : uid(0), state(FRIENDSTATE_OFFLINE) {;}
};

struct UserPermissions
{
   enum
   {
      LEVEL_USER=(1<<0),
      LEVEL_GUIDE=(1<<1),
      LEVEL_SUPERGUIDE=(1<<2),
      LEVEL_COMM_MANAGER=(1<<3),
      LEVEL_ADMIN=(1<<4),
      LEVEL_SUPER_ADMIN=(1<<5),
      LEVEL_NETWORK_ADMIN=(1<<6),
      LEVEL_ROOT_ADMIN=(1<<7),
      LEVEL_DEVELOPER=(1<<8),
      LEVEL_SUPER_DEV=(1<<31),

	  LEVEL_VALUE_MASK = 0xFFFFFFFF
   };
   uint32_t code;
   
   bool hasUserLevel() { return (code & LEVEL_USER) != 0; }
   bool hasGuideLevel() { return (code & (LEVEL_GUIDE | LEVEL_SUPERGUIDE)) != 0; }
   bool hasSuperGuideLevel() { return (code & (LEVEL_SUPERGUIDE)) != 0; }
   bool hasCommManagerLevel() { return (code & LEVEL_COMM_MANAGER) != 0; }
   bool hasAdminLevel() { return (code & LEVEL_ADMIN) != 0; }
   bool hasSuperAdminLevel() { return (code & LEVEL_SUPER_ADMIN) != 0; }
   bool hasNetworkAdminLevel() { return (code & LEVEL_NETWORK_ADMIN) != 0; }
   bool hasRootAdminLevel() { return (code & LEVEL_ROOT_ADMIN) != 0; }
   bool hasDeveloperLevel() { return (code & LEVEL_DEVELOPER) != 0; }
   bool checkMask(uint32_t mask) { return (code & mask) != 0; }
};

// Stores common state used by packet and rpc handlers
class OVConnectionState
{
public:
   virtual ~OVConnectionState() {;}
};

class OVServerGameConnectionState : public OVConnectionState
{
public:
   OVServerGameConnectionState(asio::io_service &svc) :
     mPreparingFriendsList(false),
     mSubmittedFriendsList(false),
   mLastLocationID(-1),
   mLastInstanceID(-1){;}
   
   ~OVServerGameConnectionState() {;}
   
   // Friend flags
   bool mPreparingFriendsList; // Friends list is being prepared (used in conjunction with mPreparingFriendsList)
   bool mSubmittedFriendsList; // Friends list has been submitted to client (any friend updates need to be delayed)
   
   int32_t mLastLocationID;
   int32_t mLastInstanceID;
   
   std::vector<FriendStatusRecord> mFriendList;    // Current friend list (used to dispatch friend group messages)
   
   bool isIgnoringUser(uint32_t uid);
   bool isFriend(uint32_t uid);
   bool isFriendOrPending(uint32_t uid);
   bool isPendingFriend(uint32_t uid);
   bool isAcceptableFriend(uint32_t uid);
   bool isBeingIgnoredBy(uint32_t uid);
   FriendState getFriendState(uint32_t uid);
   uint32_t getFriendCount();
};

class OVBackendGameConnectionState : public OVConnectionState
{
public:
   OVBackendGameConnectionState(asio::io_service &svc) : mAcceptPermissions(0), mPendingLocationReadyRequests(0), mLocationReadyQueue(mLocationReadyQueueService), mReady(false) {;}
   ~OVBackendGameConnectionState() {;}
   
   enum
   {
      ACCEPT_ABORT=0,
      ACCEPT_CONNECT=1
   };
   
   std::vector<uint32_t> mConnectedUIDs;
   std::string mLocationName;
   std::string mInstanceName;
   
   // NOTE: house info is stored in the db
   
   uint32_t mAcceptPermissions;
   bool mVIPOnly;
   
   int64_t mInternalID; // API id
   int32_t mLocationID;
   int32_t mInstanceID;
   uint32_t mNumHomes;
   
   uint8_t mLastAcceptState;
   
   // Simpler reps of address and port
   uint8_t mLocationAddress[4];
   uint16_t mLocationPort;
   
   std::atomic<int32_t> mPendingLocationReadyRequests;
   asio::io_service mLocationReadyQueueService;
   asio::io_service::strand mLocationReadyQueue;
   
   bool mReady;
   
   void onAdvanceLocationReadyQueue();
   
   void insertUID(uint32_t uid);
   void eraseUID(uint32_t uid);
};


class OVProtocolHandler : public std::enable_shared_from_this<OVProtocolHandler>
{
public:
   
   enum
   {
      ReadBufferSize = 32000,
      PingTime = 30 * 1000,
      ReserveRPCRequests = 10
   };
   
   enum
   {
      PROTOCOL_PATCHER=1,
      PROTOCOL_BACKEND=2,
      PROTOCOL_2012=4,
      PROTOCOL_09=8,
      PROTOCOL_CLIENT = 16,
      PROTOCOL_BACKEND_TOOL = 32,
      
      PROTOCOL_COMMON = 0xFF,
      PROTOCOL_GAME_COMMON = PROTOCOL_2012 | PROTOCOL_09
   };
   
   // What happens when the idle timer expires
   enum IdleState : uint8_t
   {
      IDLE_WAIT_PING=0,
      IDLE_WAIT_PING_ACK=1,
      IDLE_CLOSING=2
   };
   
   OVConnectionState *mState;
   
   IdleState mIdleState;
   bool mCloseAfterWrite;
   bool mClosed;
   ReadBufferProcessor::ReadState mReadState; // used for rate limiting
   
   bool mIsWriting;
   bool mIsVIP;
   uint8_t mAvatarSex;
   uint8_t mLimitPendingRPCRequests;
   
   uint16_t mReadLimitKB; // max=67mb
   uint8_t mProtocol;
   
   
   uint32_t mSocketID;
   uint16_t mLastRequestID;
   
   asio::io_service::strand mStrand;
   OVRuntimeTimerType mIdleTimer;
   OVRuntimeTimerType mReadRestartTimer;
   ReadBufferProcessor mReadProcessor;
   
   tcp::socket mSocket;
   PacketReadBuffer<ReadBufferSize> mReadBuffer;
   uint8_t mWriteBuffer[ReadBufferSize];
   
   std::vector<OVFreeableData*> mQueue;
   std::vector<RPCTicketPtr> mRPCTickets;
   
   // User state
   std::string mUsername;
   uint8_t mLocationPWDHash[20]; // this is needed for location servers
   uint32_t mUid;
   UserPermissions mPermissionsMask;
   uint32_t mLastInventoryID;
   
   uint8_t mSecurityCode[20];
   BLOWFISH_CONTEXT mEncryptionKey;
   
   //
   
   OVProtocolHandler(asio::io_service &svc, tcp::socket socket, uint8_t protocol, OVConnectionState* state, uint32_t socketID);
   virtual ~OVProtocolHandler();
   
   inline bool isClosing() { return mClosed || mCloseAfterWrite; }
   
   void setAuthCode();
   
   void onClose();
   
   void onCloseAfterWrite();
   
   void kick(int delayMs);
   void onKick(int delayMs);
   
   void onIdleTimer(const std::error_code& error);
   
   void queuePacketData(OVFreeableData* data);
   
   void queueCloseAfterWrite();
   
   // Queue freeable data. This should always be called within a strand handler.
   void onQueueWriteData(OVFreeableData* data);
   
   // Write callback. This should always be called within a strand handler.
   void onDataWritten(std::error_code ec, std::size_t sz);
   
   // Send next data packets. This should always be called within a strand handler.
   void onSendNextDataPackets();
   
   void start(uint16_t kbpslimit);
   
   void startRead();
   
   // Read callback. Should always be called within a strand.
   void onDataRead();
   
   // Packet callback. Should always be called within a strand.
   bool onPacket(OVPacketHeader* header);
   
   virtual void onJunkPacket();
   
   inline uint32_t getReadWindowLimit() { return ((uint32_t)mReadLimitKB)*1024; }
   void doRead(uint32_t offset, uint32_t size);
   
   void onRestartReadTimer(const std::error_code& error);
   
   bool inPreloginPhase();
   bool onPreloginPacket(OVPacketHeader* header);
   
   void sendRedirectPacket(uint8_t address[4]);
   
   RPCTicketPtr beginRPCTicket(const RPCTicketInfo &requestInfo);
   RPCTicketPtr findRPCTicket(const uint32_t requestId);
   void cancelRPCTicket(RPCTicketPtr &ticket);
   bool hasRPCTicket(const uint32_t requestId);
   void endRPCTicket(const RPCTicketPtr &ticket);
   uint32_t newRPCTicketID();
   bool cantAcceptMoreRPCRequests();
   
   bool allowMultipleLogins();
   bool acceptsPacketType(uint32_t code, bool is_auto);
   
   
   void onLogout();
   
   inline asio::io_service::strand& getStrand() { return mStrand; }
   
   inline bool isClient() { return (mProtocol & PROTOCOL_CLIENT) != 0; }
   inline bool isReading() { return mReadState == ReadBufferProcessor::READ_READING; }
   
   
   struct DBStrandRef
   {
      std::shared_ptr<OVProtocolHandler> handlerPtr;
      
      inline bool isValid() const
      {
         return true;
      }
      
      inline asio::io_service::strand& getQueue() const
      {
         return handlerPtr->mStrand;
      }
   };
   
   inline DBStrandRef getStrandRef()
   {
      return DBStrandRef{shared_from_this()};
   }
};


namespace Log
{
   void client_printf(OVProtocolHandler &handler, LogEntry::Type type, const char *_format, ...);
   
   void client_warnf(OVProtocolHandler &handler, LogEntry::Type type, const char *_format, ...);
   
   void client_errorf(OVProtocolHandler &handler, LogEntry::Type type, const char *_format, ...);
};

class OVBaseServer
{
public:
   
   virtual void close()=0;
   
   virtual ~OVBaseServer()
   {
      
   }
};

template<typename T> class OVServer : public OVBaseServer
{
public:
   OVServer(const char* name, asio::io_service& io_service, uint16_t port, uint8_t protocol, uint16_t kbps_limit)
   : name_(name),
   protocol_(protocol), acceptor_(io_service, tcp::endpoint(tcp::v4(), port)), socket_(io_service), lastSocketID_(0)
   {
      iosvcptr_ = &io_service;
      kbpslimit_ = kbps_limit;
      Log::printf(LogEntry::General, "Listening on port %i", port);
      do_accept();
   }
   
   void close()
   {
      acceptor_.close();
   }
   
private:
   void do_accept()
   {
      acceptor_.async_accept(socket_,
                             [this](std::error_code ec)
                             {
                                if (!ec)
                                {
                                   asio::io_service &io_service = *iosvcptr_;
                                   std::make_shared<OVProtocolHandler>(io_service, std::move(socket_), protocol_, new T(io_service), ++lastSocketID_)->start(kbpslimit_);
                                }
                                
                                do_accept();
                             });
   }
   
   asio::io_service* iosvcptr_;
   const char* name_;
   uint8_t protocol_;
   tcp::acceptor acceptor_;
   tcp::socket socket_;
   uint16_t kbpslimit_;
   
   uint32_t lastSocketID_;
};
 
