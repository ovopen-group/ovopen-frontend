/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <asio.hpp>
#include <atomic>
#include <iostream>
#include <unordered_map>
#include "zlib.h"

#include "packet.hpp"
#include "packetHandlers.hpp"
#include "protocolHandler.hpp"
#include "coreUtil.hpp"
#include "adminQueue.hpp"
#include "dbQueue.hpp"
#include "serverLogger.hpp"
#include "groupChat.hpp"

#include "INIReader.h"
#include "log.hpp"
#include "serverLogger.hpp"
#include "statsController.hpp"

using asio::ip::tcp;
extern std::string gBannerString;

DBQueryQueue* DBQueryQueue::sDBQueryQueue;
AdminQueryQueue* AdminQueryQueue::sAdminQueryQueue;
static asio::io_service gIoService;
static std::vector<OVBaseServer*> gServers;

ServerLogger* gFileLogger;

void LogToFile(uint32_t level, LogEntry *logEntry)
{
   gFileLogger->addLogEntry(level, logEntry);
}

void LogToStdout(uint32_t level, LogEntry *logEntry)
{
   printf("%s\n", logEntry->mData);
}


static int gDebugMultiVal = 0;

#ifndef OVOPEN_TEST

int main(int argc, char* argv[])
{
   try
   {
      
      INIReader config ("config.ini");
      if (config.ParseError())
      {
         std::cerr << "Invalid config\n";
         return 1;
      }
      
      std::string logger = config.GetString("general", "log", "stdout");
      if (strcmp(logger.c_str(), "stdout") == 0)
      {
         Log::addConsumer(LogToStdout);
      }
      else
      {
         gFileLogger = new ServerLogger(logger.c_str());
         Log::addConsumer(LogToFile);
      }
      
      asio::signal_set signals(gIoService);
      signals.add(SIGINT);
      signals.add(SIGTERM);
#if defined(SIGQUIT)
      signals.add(SIGQUIT);
#endif // defined(SIGQUIT)
      
      signals.async_wait([](std::error_code /*ec*/, int /*signo*/)
                         {
                            Log::printf(LogEntry::General, "Stopping server\n");
                            
                            if (DBQueryQueue::sDBQueryQueue)
                               DBQueryQueue::sDBQueryQueue->shutdown();
         
                            for (auto server : gServers)
                            {
                               server->close();
                            }
                            
                            gIoService.stop();
                            //connection_manager_.stop_all();
                         });
      
      PacketHandler::initKlasses();
      RPCPacketHandler::initKlasses();
      TestTimerController::init(gIoService);
      
      std::string forceIp = config.GetString("general", "forceLocationIpAddress", "");
      if (!forceIp.empty())
      {
         int vals[4];
         sscanf(forceIp.c_str(), "%d.%d.%d.%d", &vals[0], &vals[1], &vals[2], &vals[3]);
         for (int i=0; i<4; i++) { gForceLocationIpAddress[i] = vals[i]; }
         gForceLocationIp = false;
      }

      gBannerString = config.GetString("general", "banner", "Beta ovopen 0.01 (00000000000000) ");
      

      DBQueryQueue::ConfigInfo cfg;

      cfg.mHostname = config.GetString("database", "host", "127.0.0.1");
      cfg.mPort = config.GetInteger("database", "port", 3306);
      cfg.mUsername = config.GetString("database", "username", "root");
      cfg.mPassword = config.GetString("database", "password", "");
      cfg.mDatabase = config.GetString("database", "database", "ovopen");
      cfg.mRetryCount = config.GetInteger("database", "retry", 3);

      DBQueryQueue::sDBQueryQueue = new DBQueryQueue(gIoService);
      DBQueryQueue::sDBQueryQueue->configurePool(config.GetString("database", "driver", "sqlite").c_str(), 
        cfg,
        config.GetInteger("database", "readers", 0),
        config.GetInteger("database", "writers", 0),
        config.GetInteger("database", "read_writers", 1));
      
#ifdef TEST_DB
      
      const char *blobData = "1\n2\n3\n\n\nFOOOO ";
      
      DBQueryQueue::sDBQueryQueue->addToQueueAffected(DBQueryQueue::getStrandRef(), SQLBindPack("INSERT INTO resource_datas(name, data) VALUES ($1,$2)", "test", SQLBlobInfo(blobData, strlen(blobData)), (int64_t)1000, (int64_t)2), [](OVDBConnection* async, const std::error_code &ec, uint64_t affRows){
         Log::printf("\t\tq1 error=%i, rows=%u", ec.value(), affRows);
      });

         
         
      DBQueryQueue::sDBQueryQueue->addToQueueAffected(DBQueryQueue::getStrandRef(), SQLBindPack("UPDATE users SET cc=$1 WHERE id=$2", (int64_t)1000, (int64_t)2), [](OVDBConnection* async, const std::error_code &ec, uint64_t affRows){
         Log::printf("\t\tq1 error=%i, rows=%u", ec.value(), affRows);
      });
   
      DBQueryQueue::sDBQueryQueue->addToQueueAffected(DBQueryQueue::getStrandRef(), SQLBindPack("UPDATE users SET cc=$1 WHERE id=$2", (int64_t)2000, (int64_t)2), [](OVDBConnection* async, const std::error_code &ec, uint64_t affRows){
         Log::printf("\t\tq2 error=%i, rows=%u", ec.value(), affRows);
      });

      DBQueryQueue::sDBQueryQueue->addToQueueAffected(DBQueryQueue::getStrandRef(), SQLBindPack("UPDATE users SET cc=$1 WHERE id=$2", (int64_t)3000, (int64_t)2), [](OVDBConnection* async, const std::error_code &ec, uint64_t affRows){
         Log::printf("\t\tq2 error=%i, rows=%u", ec.value(), affRows);
      });
      DBQueryQueue::sDBQueryQueue->addToQueueRows(DBQueryQueue::getStrandRef(), SQLBindPack("SELECT username,id FROM users"), false, [](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
         
         for (DBResult &res : results)
         {
            printf("\t\tname=%s\nUID=%s\tidl=%lu\nnamel=%lu\n", res.value[0], res.value[1], res.lengths[0], res.lengths[1]);
         }
      });
      printf("--\n");
      DBQueryQueue::sDBQueryQueue->addToQueueRows(DBQueryQueue::getStrandRef(), SQLBindPack("SELECT username,id FROM users"), false, [](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
         
         for (DBResult &res : results)
         {
            printf("\t\tname=%s UID=%s\tidl=%lu namel=%lu\n", res.value[0], res.value[1], res.lengths[0], res.lengths[1]);
         }
      });
      printf("--\n");
      DBQueryQueue::sDBQueryQueue->addToQueueMultipleRows(DBQueryQueue::getStrandRef(), SQLBindPack("SELECT username,id FROM users; UPDATE users set cc=$1 WHERE id=$2; SELECT id,name FROM locations", (int64_t)999, (int64_t)2), false, [](OVDBConnection* async, const std::error_code &ec, uint32_t resultSet, std::vector<DBResult> &results){
         
         printf("\t\tRESULTSET %u\n", resultSet);
         for (DBResult &res : results)
         {
            printf("\t\tf1=%s\nf2=%s\tidl=%lu\nnamel=%lu\n", res.value[0], res.value[1], res.lengths[0], res.lengths[1]);
         }
         printf("-----\n");
      });
      
      DBQueryQueue::sDBQueryQueue->addToQueueMassQuery(DBQueryQueue::getStrandRef(), SQLBindPack("INSERT INTO patch_mirrors(name, hostname, path) VALUES ($1, $2, $3)"), [](OVDBConnection* conn, std::error_code ec, SQLPack** outPacks, int numPacks){
         gDebugMultiVal += numPacks;
         if (gDebugMultiVal > 100)
            return 0;
         
         for (int i=0; i<numPacks; i++)
         {
            char buf1[256];
            char buf2[256];
            snprintf(buf1, 256, "DupMirror%i", gDebugMultiVal-numPacks+i);
            snprintf(buf2, 256, "dupmirror%i.hostname.com", gDebugMultiVal-numPacks+i);
            outPacks[i] = SQLBindPack(NULL, buf1, buf2, "ovpatch/");
         }
         
         return numPacks;
      });
      #endif
      
      AdminQueryQueue::sAdminQueryQueue = new AdminQueryQueue(gIoService);
      
      GroupChatHandler::init(gIoService);
      bool fail = false;
      
      try {
         uint16_t listen_port = config.GetInteger("patcher", "listen_port", 0);
         if (listen_port != 0)
         {
            gServers.push_back(new OVServer<OVServerGameConnectionState>("patcher",
                                                                        gIoService,
                                                                        config.GetInteger("patcher", "listen_port", 0),
                                                                        OVProtocolHandler::PROTOCOL_PATCHER,
                                                                        config.GetInteger("patcher", "packet_limit_kbps", 0)));
         }
         
         listen_port = config.GetInteger("game", "listen_port", 0);
         if (listen_port != 0)
         {
            gServers.push_back(new OVServer<OVServerGameConnectionState>("game",
                                                                        gIoService,
                                                                        config.GetInteger("game", "listen_port", 0),
                                                                        config.GetInteger("game", "protocol", OVProtocolHandler::PROTOCOL_2012),
                                                                        config.GetInteger("game", "packet_limit_kbps", 0)));
         }
         
         listen_port = config.GetInteger("backend", "listen_port", 0);
         if (listen_port != 0)
         {
            gServers.push_back(new OVServer<OVBackendGameConnectionState>("backend",
                                                                         gIoService,
                                                                         config.GetInteger("backend", "listen_port", 0),
                                                                         OVProtocolHandler::PROTOCOL_BACKEND,
                                                                         config.GetInteger("backend", "packet_limit_kbps", 0)));
         }
      }
      catch (std::exception &e)
      {
         std::cerr << "Exception: " << e.what() << "\n";
         fail = true;
      }
      
      OVRuntimeTimerType statPrinter(gIoService);
      
      std::function<void(const std::error_code&)> statPrinterFunc;
      statPrinterFunc = [&statPrinter, &statPrinterFunc](const std::error_code &ec){
         if (ec)
            return;
         
         StatsController::dump();
         
         statPrinter.expires_after(std::chrono::milliseconds(10000));
         statPrinter.async_wait(statPrinterFunc);
      };

      statPrinter.expires_after(std::chrono::milliseconds(10000));
      statPrinter.async_wait(statPrinterFunc);
      
      
      if (fail)
      {
         gIoService.stop();
      }
      else
      {
         if (!fail) gIoService.run();
      }

      GroupChatHandler::clear();
      
      // TODO: better shutdown logic
      DBQueryQueue* qu = DBQueryQueue::sDBQueryQueue;
      qu->shutdown();
      std::this_thread::sleep_for(std::chrono::milliseconds(10));

      DBQueryQueue::sDBQueryQueue = NULL;
      delete qu;
      
      for (auto server : gServers)
      {
         delete server;
      }
      
      if (gFileLogger)
         delete gFileLogger;
   }
   catch (std::exception& e)
   {
      std::cerr << "Exception: " << e.what() << "\n";
   }
   
   return 0;
}

#endif
