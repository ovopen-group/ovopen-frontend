/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef dbQueue_hpp
#define dbQueue_hpp

#include <asio.hpp>
#include <asio/deadline_timer.hpp>
#include <asio/ip/address.hpp>
#include <atomic>
#include <iostream>
#include <unordered_map>
#include <memory>
#include <vector>

#include "coreTypes.hpp"
#include "coreUtil.hpp"

#include "log.hpp"


class DBQueryQueue;

inline uint64_t DBToU64(const char* value)
{
   return value == NULL ? 0 : strtoull(value, NULL, 10);
}
                               
inline int64_t DBToS64(const char* value)
{
   return value == NULL ? 0 : std::atoll(value);
}

inline uint32_t DBToU32(const char* value)
{
   return value == NULL ? 0 : (uint32_t)strtoull(value, NULL, 10);
}
                               
inline int32_t DBToS32(const char* value)
{
   return value == NULL ? 0 : std::atoi(value);
}

inline const char* DBStr(const char* value)
{
   return value == NULL ? "" : value;
}

inline unsigned long ClampedHexString(char *to, const char *from, unsigned long to_length)
{
   uint32_t max_chars = (to_length-1)/2;
   max_chars = std::min<uint32_t>(strlen(from),max_chars);
   return bin2hex((const unsigned char*)from, max_chars, to);
}

inline unsigned long ClampedHexBin(char *to, const char *from, unsigned long length, unsigned long to_length)
{
   uint32_t max_chars = (to_length-1)/2;
   max_chars = std::min<uint32_t>(length, max_chars);
   return bin2hex((const unsigned char*)from, max_chars, to);
}

// Result row. To keep things simple and serializable, for the moment we just use strings.
struct DBResult
{
   enum
   {
      MaxFields=12
   };
   const char** value;
   unsigned long lengths[MaxFields];
};

struct SQLBlobInfo
{
   const void* ptr;
   uint32_t size;
   
   SQLBlobInfo() : ptr(NULL), size(0) {;}
   SQLBlobInfo(const void* _ptr, uint32_t _sz) : ptr(_ptr), size(_sz) {;}
};

inline uint32_t SQLBindSize(const char* str)
{
   return strlen(str)+1;
}

inline uint32_t SQLBindSize(uint32_t val)
{
   return 8;
}

inline uint32_t SQLBindSize(int64_t val)
{
   return 8;
}

inline uint32_t SQLBindSize(SQLBlobInfo val)
{
   return val.size+4;
}

struct SQLPackInfo
{
   enum
   {
      BIND_DONE,
      BIND_STR_QUERY,   // Create new stmt for each query
      BIND_CACHE_QUERY, // Query can be cached from a particular query type
      BIND_INT,
      BIND_UINT,
      BIND_STR,
      BIND_BLOB
   };
   
   uint32_t code;
   uint32_t ptr;
};

inline SQLPackInfo SQLBindPack(uint8_t* base, uint8_t** ptr, int64_t val)
{
   uint8_t* realPtr = *ptr;
   *((int64_t*)realPtr) = val;
   
   SQLPackInfo outI;
   outI.code = SQLPackInfo::BIND_INT;
   outI.ptr = realPtr - base;
   
   *ptr = realPtr+8;
   
   return outI;
}

inline SQLPackInfo SQLBindPack(uint8_t* base, uint8_t** ptr, uint32_t val)
{
   uint8_t* realPtr = *ptr;
   *((uint32_t*)realPtr) = val;
   
   SQLPackInfo outI;
   outI.code = SQLPackInfo::BIND_UINT;
   outI.ptr = realPtr - base;
   
   *ptr = realPtr+4;
   
   return outI;
}

inline SQLPackInfo SQLBindPack(uint8_t* base, uint8_t** ptr, SQLBlobInfo val)
{
   uint8_t* realPtr = *ptr;
   *((uint32_t*)realPtr) = val.size;
   
   SQLPackInfo outI;
   outI.code = SQLPackInfo::BIND_BLOB;
   outI.ptr = realPtr - base;
   
   memcpy(realPtr+4, val.ptr, val.size);
   
   *ptr = realPtr+val.size+4;
   
   return outI;
}

inline SQLPackInfo SQLBindPack(uint8_t* base, uint8_t** ptr, const char* val)
{
   uint32_t len = strlen(val)+1;
   uint8_t* realPtr = *ptr;
   
   SQLPackInfo outI;
   outI.code = SQLPackInfo::BIND_STR;
   outI.ptr = realPtr - base;
   
   memcpy(realPtr, val, len);
   *ptr = realPtr+len;
   
   return outI;
}

// Data for query and instructions for bind params
struct SQLPack
{
public:
   enum
   {
      MAX_ARGS=8
   };
   
   SQLPackInfo packs[MAX_ARGS];
   
   const char* query() const
   {
      return (const char*)data();
   }
   
   uint8_t* data() const
   {
      return (uint8_t*)&packs[MAX_ARGS];
   }
   
};

class OVDBConnection;

// Func to emit at most N SQLPacks
typedef std::function<int(OVDBConnection*,const std::error_code,SQLPack**,int)> DBMassQueryFunc;

inline void* SQLPackAllocator(uint32_t size)
{
   return malloc(sizeof(SQLPack)+size);
}

inline SQLPack* SQLBindPack(const char* query)
{
   if (!query)
      return NULL;
   
	uint32_t queryLen = strlen(query) + 1;
	uint32_t totSz = queryLen;

	SQLPack* pack = (SQLPack*)SQLPackAllocator(totSz);
	uint8_t* ptr = pack->data();

	memcpy(ptr, query, queryLen);
	ptr += queryLen;
	pack->packs[0] = { SQLPackInfo::BIND_STR_QUERY, 0 };
	pack->packs[1] = { SQLPackInfo::BIND_DONE, 0 };

	return pack;
}

template<typename... Targs> inline SQLPack* SQLBindPack(const char* query, Targs... args)
{
   uint32_t xSz[] = {SQLBindSize(args)...};
   
   uint32_t queryLen = query == NULL ? 0 : strlen(query)+1;
   uint32_t totSz = queryLen;
   for (int i=0; i<sizeof(xSz)/sizeof(uint32_t); i++)
   {
      totSz += xSz[i];
   }
   
   SQLPack* pack = (SQLPack*)SQLPackAllocator(totSz);
   uint8_t* ptr = pack->data();
   
   if (queryLen > 0) memcpy(ptr, query, queryLen);
   ptr += queryLen;
   pack->packs[0] = {SQLPackInfo::BIND_STR_QUERY, 0};
   
   SQLPackInfo xInfos[] = {SQLBindPack(pack->data(), &ptr, args)...};
   
   int i=0;
   for (i=0; i<sizeof(xInfos)/sizeof(xInfos[0]); i++)
   {
      pack->packs[i+1] = xInfos[i];
   }
   pack->packs[i+1] = {SQLPackInfo::BIND_DONE, 0};
   
   return pack;
}

class SQLPackQueueRef : public std::enable_shared_from_this<SQLPackQueueRef>
{
public:
   SQLPack* mPack;
   uint64_t mStartTime;
   
   SQLPackQueueRef() : mPack(NULL), mStartTime(0) {;}
   
   SQLPackQueueRef(SQLPack* ref, uint64_t startTime)
   {
      mPack = ref;
      mStartTime = startTime;
   }
   
   ~SQLPackQueueRef()
   {
      delete mPack;
   }
};

class OVDBConnection : public std::enable_shared_from_this<OVDBConnection>
{
public:
   virtual ~OVDBConnection(){;}
   virtual bool connect()=0;
   virtual void disconnect()=0;
   
   virtual uint64_t insert_id() = 0;
   virtual uint64_t affected_rows() = 0;
   virtual bool more_results() = 0;
   
   virtual bool isFreeForQuery() = 0;
   
   virtual void finishQuery() = 0;
   virtual std::error_code doBasicQuery(std::shared_ptr<SQLPackQueueRef> query) = 0;
   virtual std::error_code doRowQuery(std::shared_ptr<SQLPackQueueRef> query, bool multi, std::vector<DBResult> &results, std::function<void(std::error_code&,uint32_t,std::vector<DBResult>&)>&& callback) = 0;
   virtual std::error_code doMassQuery(std::shared_ptr<SQLPackQueueRef> baseQuery, DBMassQueryFunc&& func) = 0;
};

typedef std::shared_ptr<OVDBConnection> OVDBConnectionPtr;

class DBQueryQueue
{
public:
   struct ConfigInfo
   {
      std::string mHostname;
      int mPort;
      std::string mUsername;
      std::string mPassword;
      std::string mDatabase;

      int mRetryCount;
   };
   
   // Queue for result handler which runs in main processing loop
   asio::io_service::strand mMainQueue;
   
   // Queues for each core type of connection (if we only have readwrite, only write is used)
   asio::io_service mWriteQueue;
   asio::io_service mReadQueue;
   
   std::atomic<int32_t> mPendingOperations;
   
   std::vector<OVDBConnectionPtr> mConnections;
   std::atomic<uint32_t> mFreeConnections;
   
   // Credentials
   ConfigInfo mConnectionConfig;
   bool mUseReadQueue;
   
   // Main query queue
   static DBQueryQueue* sDBQueryQueue;
   
   std::function<OVDBConnectionPtr(DBQueryQueue*,uint32_t,ConfigInfo*)> mSpawnConnectionFunc;
   
   int mShutttingDown;

   // --
   
   DBQueryQueue(asio::io_service &svc);
   virtual ~DBQueryQueue();
   
   void shutdown();

   static void setConnection(OVDBConnection *ptr);
   static OVDBConnectionPtr getConnection();
   
   // Connection management
   void checkConnectionsLater(uint64_t delta_ms);
   void checkConnections();
   
   /// Configure DB connection pool
   void configurePool(const char* driver, ConfigInfo cfg, int numReaders, int numWriters, int numReadWriters);
   
   // Advance queue if we have a free connection
   void onAdvanceQueue();
   
   inline asio::io_service& pickDBQueue(bool write) { return mUseReadQueue ? ( write ? mWriteQueue : mReadQueue ) : mWriteQueue; }
   
   static inline void defaultBasicHandler(OVDBConnection* a, const std::error_code &ec, uint64_t affected_rows)
   {
   }
   
   // (OVDBConnection* connection, const std::error_code &ec, uint64_t insert_id)
   template<typename T> void addToQueueInsert(T queue, SQLPack* query, std::function<void(OVDBConnection*,const std::error_code&,uint64_t)> &&resultHandler)
   {
      if (!queue.isValid())
         return;
      
      std::shared_ptr<SQLPackQueueRef> ref = std::make_shared<SQLPackQueueRef>(query, 0);
      pickDBQueue(true).post([ref, resultHandler, queue]() {
         // We are now in the DB thread so can do queries etc in a sync fashion.
         Log::printf(LogEntry::Debug, "DBQueryQueue::addToQueueRows: %s", ref->mPack->query());
         
         OVDBConnectionPtr curConnection = DBQueryQueue::getConnection()->shared_from_this();
         std::error_code ec = curConnection->doBasicQuery(ref);
         
         uint64_t param = curConnection->insert_id();

         std::promise<void> waitQuery;
         std::future<void> waitDone = waitQuery.get_future();
         
         queue.getQueue().post([param, curConnection, &resultHandler, &waitQuery]{
            std::error_code ec(0, std::system_category());
            resultHandler(curConnection.get(), ec, param);
            waitQuery.set_value();
         });
         
         // Wait for result to be processed in main loop before we do another query
         waitDone.wait();
      });
   }
   
   // (OVDBConnection* connection, const std::error_code &ec, uint64_t affected_rows)
   template<typename T> void addToQueueAffected(T queue, SQLPack* query, std::function<void(OVDBConnection*,const std::error_code&,uint64_t)> &&resultHandler)
   {
      if (!queue.isValid())
         return;
      
      std::shared_ptr<SQLPackQueueRef> ref = std::make_shared<SQLPackQueueRef>(query, 0);
      pickDBQueue(true).post([ref, resultHandler, queue]() {
         Log::printf(LogEntry::Debug, "DBQueryQueue::addToQueueAffected: %s", ref->mPack->query());
         
         OVDBConnectionPtr curConnection = DBQueryQueue::getConnection()->shared_from_this();
         std::error_code ec = curConnection->doBasicQuery(ref);
         
         uint64_t param = curConnection->affected_rows();
         
         std::promise<void> waitQuery;
         std::future<void> waitDone = waitQuery.get_future();
         
         queue.getQueue().post([param, curConnection, &resultHandler, &waitQuery]{
            std::error_code ec(0, std::system_category());
            resultHandler(curConnection.get(), ec, param);
            waitQuery.set_value();
         });
         
         // Wait for result to be processed in main loop before we do another query
         waitDone.wait();
      });
   }
   
   // (OVDBConnection* connection, const std::error_code &ec, uint32_t,std::vector<DBResult>& results)
   template<typename T> void addToQueueRows(T queue, SQLPack* query, bool needWrite, std::function<void(OVDBConnection*,const std::error_code&,std::vector<DBResult>&)>  &&resultHandler)
   {
      if (!queue.isValid())
         return;
      
      std::shared_ptr<SQLPackQueueRef> ref = std::make_shared<SQLPackQueueRef>(query, 0);
      pickDBQueue(needWrite).post([ref, resultHandler, queue]() {
         Log::printf(LogEntry::Debug, "DBQueryQueue::addToQueueRows: %s", ref->mPack->query());
         
         OVDBConnectionPtr curConnection = DBQueryQueue::getConnection()->shared_from_this();
         std::vector<DBResult> resultSet;
         
         uint32_t count = 0;
         
         {
            std::error_code ec = curConnection->doRowQuery(ref, false, resultSet, [curConnection, &resultHandler, &resultSet, &queue](std::error_code& ec, uint32_t resultCount, std::vector<DBResult>& currentResults) {

               std::promise<void> waitQuery;
               std::future<void> waitDone = waitQuery.get_future();
               
               queue.getQueue().post([curConnection, &resultSet, &resultHandler, &waitQuery]{
                  std::error_code ec(0, std::system_category());
                  resultHandler(curConnection.get(), ec, resultSet);
                  waitQuery.set_value();
               });
               
               // Wait for result to be processed in main loop before we do another query
               waitDone.wait();
            });
            
            resultSet.clear();
         }
         
         curConnection->finishQuery();
      });
   }
   
   // (DBQueryQueue* std::error_code &ec, uint32_t set ,std::vector<DBResult>& results)
   template<typename T> void addToQueueMultipleRows(T queue, SQLPack* query, bool needWrite, std::function<void(OVDBConnection*,const std::error_code&,uint32_t,std::vector<DBResult>&)> &&resultHandler)
   {
      if (!queue.isValid())
         return;
      
      std::shared_ptr<SQLPackQueueRef> ref = std::make_shared<SQLPackQueueRef>(query, 0);
      pickDBQueue(needWrite).post([ref, resultHandler, queue]() {
         Log::printf(LogEntry::Debug, "DBQueryQueue::addToQueueRows: %s", ref->mPack->query());
         
         OVDBConnectionPtr curConnection = DBQueryQueue::getConnection()->shared_from_this();
         std::vector<DBResult> resultSet;
         
         std::error_code ec = curConnection->doRowQuery(ref, true, resultSet, [curConnection, &resultHandler, &resultSet, &queue](std::error_code& ec, uint32_t resultCount, std::vector<DBResult>& currentResults) {

            std::promise<void> waitQuery;
            std::future<void> waitDone = waitQuery.get_future();
            
            queue.getQueue().post([curConnection, resultCount, &resultSet, &resultHandler, &waitQuery]{
               std::error_code ec(0, std::system_category());
               resultHandler(curConnection.get(), ec, resultCount, resultSet);
               waitQuery.set_value();
            });
            
            // Wait for result to be processed in main loop before we do another query
            waitDone.wait();
         });
         
         curConnection->finishQuery();
      });
   }
   
   template<typename T> void addToQueueMassQuery(T queue, SQLPack* query, DBMassQueryFunc &&resultHandler)
   {
      if (!queue.isValid())
         return;
      
      std::shared_ptr<SQLPackQueueRef> ref = std::make_shared<SQLPackQueueRef>(query, 0);
      pickDBQueue(true).post([ref, resultHandler, queue]() {
         Log::printf(LogEntry::Debug, "DBQueryQueue::addToQueueMassQuery: %s", ref->mPack->query());
         
         OVDBConnectionPtr curConnection = DBQueryQueue::getConnection()->shared_from_this();
         
         std::error_code ec = curConnection->doMassQuery(ref, [curConnection, &resultHandler, &queue](OVDBConnection* connection, const std::error_code ec, SQLPack** packs, int numPacks) {

            std::promise<void> waitQuery;
            std::future<void> waitDone = waitQuery.get_future();
            int outValue = 0;
            
            queue.getQueue().post([curConnection, ec, packs, numPacks, &outValue, &resultHandler, &waitQuery]{
               outValue = resultHandler(curConnection.get(), ec, packs, numPacks);
               waitQuery.set_value();
            });
            
            // Wait for result to be processed in main loop before we do another query
            waitDone.wait();
            
            return outValue;
         });
         
         curConnection->finishQuery();
      });
   }
   
   struct DBStrandRef
   {
      DBQueryQueue* queue;
      
      inline bool isValid() const
      {
         return true;
      }
      
      inline asio::io_service::strand& getQueue() const
      {
         return queue->mMainQueue;
      }
   };
   
   static inline DBStrandRef getStrandRef()
   {
      return DBStrandRef{sDBQueryQueue};
   }
   
   inline static DBQueryQueue& getQueue()
   {
      return *sDBQueryQueue;
   }
};

OVDBConnectionPtr SQLiteConnectionSpawn(DBQueryQueue*,uint32_t mode, DBQueryQueue::ConfigInfo* cfg);
OVDBConnectionPtr PostgresConnectionSpawn(DBQueryQueue*,uint32_t mode, DBQueryQueue::ConfigInfo* cfg);

#endif /* dbQueue_hpp */
