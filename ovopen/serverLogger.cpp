/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "serverLogger.hpp"
#include <atomic>
#include <iostream>
#include <iomanip>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <functional>

ServerLogger *sLogger = NULL;

ServerLogger::ServerLogger(const char* logFile) : mStrand(mIOService), mFlushTimer(mIOService)
{
   sLogger = this;
   mPrintToConsole = true;
   reopen(logFile);
}

void ServerLogger::reopen(const char *logFile)
{
   mLock.lock();

   mLogStartTime = std::chrono::system_clock::now();

   // Close existing
   if (mLogFile)
   {
      fclose(mLogFile);
      mLogFile = NULL;
   }

   // Open new
   mLogFile = fopen(logFile, "w");
   if (mLogFile) {
      time_t in_time_t = std::chrono::system_clock::to_time_t(mLogStartTime);
      std::tm tm = *std::localtime(&in_time_t);
      
      char buf[64];
      strftime(buf, sizeof(buf), "%FT%TZ", &tm);
      
      fprintf(mLogFile, "-- Start of Master Log at %s --\n", buf);

	   mLock.unlock();
      notifyFlush();
   }
   else
   {
	   mLock.unlock();
   }
}

ServerLogger::~ServerLogger()
{
   mFlushTimer.cancel();
   mQueue.clear();
   sLogger = NULL;
}

ServerLogger *ServerLogger::getInstance()
{
   return sLogger;
}

void ServerLogger::stop()
{
	mIOService.stop();
	join();

	// flush queue out
	while (!mQueue.empty())
	{
		notifyLogEntry();
	}
}

void ServerLogger::run()
{
   mThread = new std::thread([this]{ mIOService.run(); });
}

void ServerLogger::join()
{
   mThread->join();
}

void ServerLogger::addLogEntry(uint32_t level, LogEntry *entry)
{
   auto now = OVRuntimeTimerType::clock_type::now().time_since_epoch();
   std::chrono::microseconds timeSinceEpoch = std::chrono::duration_cast<std::chrono::milliseconds>(now - mLogStartTime.time_since_epoch());
   double timeMS = timeSinceEpoch.count() / 1000.0;
   
   std::lock_guard<std::mutex> lock(mLock);
   mQueue.push_back(ServerLoggerEntry(entry->mLevel, entry->mType, entry->mData, timeMS));
   
#ifdef DEBUG_SYNC_LOG
   notifyFlush();
#else
   // notify
   //mStrand.post(std::bind(&ServerLogger::notifyLogEntry, this));
#endif
}

void ServerLogger::notifyLogEntry()
{
   if (mQueue.empty())
      return;

#ifndef DEBUG_SYNC_LOG
   std::lock_guard<std::mutex> lock(mLock);
#endif
   
   int maxEntriesToEmit = 1000;
   
   // Print everything out
   while (!mQueue.empty())
   {   
      //std::string id = boost::lexical_cast<std::string>(boost::this_thread::get_id());
      
      // Write to the log
      char buffer[128];
      snprintf(buffer, 128, "[%g]\t", mQueue.front().mTime);
      fwrite(buffer, 1, strlen(buffer), mLogFile);
      fwrite(&mQueue.front().mData[0], 1, mQueue.front().mData.size(), mLogFile);
      fwrite("\n", 1, 2, mLogFile);
      
      if (mPrintToConsole)
         printf("%s%s\n", buffer, mQueue.front().mData.c_str());
      
      mQueue.pop_front();
      maxEntriesToEmit--;
      if (maxEntriesToEmit == 0)
         break;
   }
}

void ServerLogger::notifyFlush()
{
   notifyLogEntry();
#ifndef DEBUG_SYNC_LOG
   std::lock_guard<std::mutex> lock(mLock);
#endif
   //fsync(mLogFile);
   
#ifndef DEBUG_SYNC_LOG
   // Queue again
	mFlushTimer.cancel();
   mFlushTimer.expires_after(std::chrono::seconds(10));
   mFlushTimer.async_wait(mStrand.wrap(std::bind(&ServerLogger::timerFlush, this, std::placeholders::_1)));
#endif
}

void ServerLogger::timerFlush(const std::error_code error)
{
   if (error)
      return;

   // Enqueue the flush
	notifyFlush();
}
