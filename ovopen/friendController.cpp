/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "friendController.hpp"
#include "packet.hpp"
#include "protocolHandler.hpp"
#include "dbQueue.hpp"
#include "userConnectionController.hpp"

FriendController* FriendController::sFriendController;

FriendController* FriendController::get()
{
   return sFriendController;
}

extern std::unordered_map<uint32_t, std::shared_ptr<OVProtocolHandler>> gUserLookup;
extern std::unordered_map<std::string, std::shared_ptr<OVProtocolHandler>> gUserNameLookup;

void FriendController::onDispatchOnlineStatusToFriends(std::shared_ptr<OVProtocolHandler> handler, FriendState state)
{
   // Send everyone 0xcb(FriendUpdate)
   
   // Go through the packet again and update the online status for everyone listed
   
   uint32_t uid = handler->mUid;
   uint8_t newState = state;
   
   StackMemStream<6 + 3 + 32 + 4 + 1> packetStream;
   PacketBuilder packet(&packetStream);
   packet.beginBasicPacket(PacketFriendUpdate);
   
   packetStream.writeUintBE(uid);
   packetStream.writeString(handler->mUsername.c_str(), 32);
   packetStream.write(newState);
   
   packet.commit();
   
   PacketMultipleUserData* sharedPacket = PacketMultipleUserData::createFromData(packetStream.getPosition(), packetStream.mPtr);
   OVServerGameConnectionState* connectionState = ((OVServerGameConnectionState*)handler->mState);
   
   for (FriendStatusRecord &record : connectionState->mFriendList)
   {
      // Only send to people we know
      if (record.state < FRIENDSTATE_REQUIRE_ACCEPT)
      {
         std::shared_ptr<OVProtocolHandler> connHandler = UserConnectionController::resolveUIDConnection(record.uid);
         if (connHandler)
         {
            connHandler->queuePacketData(sharedPacket->cloneMultiple());
         }
      }
   }
   
   sharedPacket->free();
}

// Should be executed within connection strand
void FriendController::onUpdateFriendList(std::shared_ptr<OVProtocolHandler> handler)
{
   uint32_t uid = handler->mUid;
   if (uid == 0)
      return;
   
   OVServerGameConnectionState* connectionState = ((OVServerGameConnectionState*)handler->mState);
   connectionState->mPreparingFriendsList = true;
   
   // Do a db query to grab friends
   DBQueryQueue::getQueue().addToQueueRows(handler->getStrandRef(), SQLBindPack("SELECT friend_id, friend_name, accept_state FROM friends WHERE user_id = $1 AND accept_state <= 4 OR accept_state = 254 ORDER BY id ASC LIMIT 2500", (int64_t)handler->mUid), false, [handler](OVDBConnection* async, const std::error_code &ec, std::vector<DBResult> &results){
      
      if (!ec)
      {
         if (handler->mClosed)
            return;
         
         if (results.size() != 0)
         {
            uint16_t numFriends = (uint16_t)results.size();
            uint32_t reservedSpace = 6 + 4 + (32 + 3 + 2 + 4) * numFriends;
            
            MemStream packetStream(reservedSpace);
            PacketBuilder packet(&packetStream);
            packet.beginBasicPacket(PacketFriendList);
            
            packetStream.writeUintBE(numFriends);
            
            //
            
            uint32_t idx = 0;
            for (const auto& rowv : results)
            {
               auto row = rowv.value;
               FriendStatusRecord record;
               record.uid = DBToU32(row[0]);
               record.state = FRIENDSTATE_OFFLINE;
               
               FriendState acceptState = (FriendState)DBToU32(row[2]);
               
               if (acceptState > FRIENDSTATE_ONLINE)
               {
                  record.state = acceptState;
               }
               
               packetStream.writeUintBE(record.uid);
               packetStream.writeString(row[1], 32);
               packetStream.write(record.state);
               
               idx++;
            }
            
            packet.commit();
            
            // Grab ownership of ptr
            packetStream.mOwnPtr = false;
            PacketMallocData* responseData = new PacketMallocData();
            responseData->mSize = packetStream.getPosition();
            responseData->mPtr = packetStream.mPtr;
            
            // Callback on main handler strand to update friend list and send the packet
            handler->mStrand.post(std::bind(&FriendController::onDispatchFriendsList, handler, responseData));
         }
      }
      
   });
         
}

void FriendController::onDispatchFriendsList(std::shared_ptr<OVProtocolHandler> handler, OVFreeableData* friendPacket)
{
   // Go through the packet again and update the online status for everyone listed
   MemStream mem(friendPacket->size(), friendPacket->ptr());
   
   uint16_t numFriends;
   mem.setPosition(6);
   mem.readUintBE(numFriends);
   OVServerGameConnectionState* connectionState = ((OVServerGameConnectionState*)handler->mState);
   connectionState->mFriendList.resize(numFriends);
   
   for (uint32_t i=0; i<numFriends; i++)
   {
      FriendStatusRecord record;
      mem.readUintBE(record.uid);
      mem.skipString();
      mem.read(record.state);
      connectionState->mFriendList[i] = record;
      
      // If we are currently friends, check if they are online
      if (record.state < FRIENDSTATE_REQUIRE_ACCEPT)
      {
         std::shared_ptr<OVProtocolHandler> connHandler = UserConnectionController::resolveUIDConnection(record.uid);
         if (connHandler)
         {
            record.state = FRIENDSTATE_ONLINE;
            
            // Update state
            mem.setPosition(mem.getPosition()-1);
            mem.write(record.state);
         }
      }
      
      if (record.state == FRIENDSTATE_BEING_IGNORED)
      {
         record.state = FRIENDSTATE_DELETE;
      }
   }
   
   // Safe to do in strand
   handler->onQueueWriteData(friendPacket);
   connectionState->mSubmittedFriendsList = true;
   connectionState->mPreparingFriendsList = false;
   
   // Also safe to do
   onDispatchOnlineStatusToFriends(handler, FRIENDSTATE_ONLINE);
}

// Sets a friend record to a required state
void FriendController::setFriendRecord(uint32_t user_id, uint32_t friend_id, FriendState state, const char* friendName)
{
   DBQueryQueue::getQueue().addToQueueAffected(DBQueryQueue::getStrandRef(), SQLBindPack("INSERT INTO friends (user_id, friend_id, friend_name, accept_state) "
   "VALUES ($1, $2, (SELECT (users.username) FROM users WHERE users.id = $3 LIMIT 1), $4)"
   "ON CONFLICT(user_id,friend_id) DO UPDATE SET accept_state=$4;", (int64_t)user_id, (int64_t)friend_id, (int64_t)friend_id, (uint32_t)state), &DBQueryQueue::defaultBasicHandler);
   
   // Send update to user if they are online
   std::shared_ptr<OVProtocolHandler> userConnection = UserConnectionController::resolveUIDConnection(user_id);
   if (userConnection)
   {
      std::string savedFriendName = friendName;
      userConnection->mStrand.post([userConnection, friend_id, savedFriendName, state](){
         
         
         OVServerGameConnectionState* connectionState = ((OVServerGameConnectionState*)userConnection->mState);
         
         auto itr = std::find_if(connectionState->mFriendList.begin(), connectionState->mFriendList.end(), [friend_id](const FriendStatusRecord &item){
            return item.uid == friend_id;
         });
         
         FriendState actual_state = state;
         
         // Change to online if they are actually online.
         if (state == FRIENDSTATE_OFFLINE)
         {
            if (UserConnectionController::resolveUIDConnection(friend_id))
            {
               actual_state = FRIENDSTATE_ONLINE;
            }
         }
         
         if (itr == connectionState->mFriendList.end())
         {
            FriendStatusRecord record;
            record.state = actual_state;
            record.uid = friend_id;
            connectionState->mFriendList.push_back(record);
         }
         else
         {
            itr->state = actual_state;
         }
         
         // Send the packet
         StackMemStream<6 + 3 + 32 + 4 + 1> packetStream;
         PacketBuilder packet(&packetStream);
         packet.beginBasicPacket(PacketFriendUpdate);
         
         packetStream.writeUintBE(friend_id);
         packetStream.writeString(savedFriendName.c_str(), 32);
         packetStream.write((uint8_t)actual_state);
         
         packet.commit();
         
         userConnection->onQueueWriteData(PacketMallocData::createFromData(packetStream.getPosition(), packetStream.mPtr));
      });
   }
}

void FriendController::startFriendRequest(uint32_t user_id, uint32_t friend_id, const char* userName, const char* friendName)
{
   // Start the friend request.
   // NOTE: We're going to assume here nobody is ignoring anyone
   setFriendRecord(user_id,   friend_id, FRIENDSTATE_REQUIRE_ACCEPT,        friendName);
   setFriendRecord(friend_id, user_id,   FRIENDSTATE_PENDING, userName);
}
