/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "log.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <algorithm>

LogEntry::Type LogEntry::General = 0;
LogEntry::Type LogEntry::Debug = 1;

namespace Log {
   
   std::vector<ConsumerCallback> gConsumers;
   int gNumConsumers[32];
   
   //------------------------------------------------------------------------------
   
   static void _printf(LogEntry::Level level, LogEntry::Type type, const char* fmt, va_list argptr)
   {
      char buffer[8192];
      uint32_t offset = 0;
      
      vsnprintf(buffer + offset, sizeof(buffer) - offset, fmt, argptr);
      
      LogEntry entry;
      entry.mData = buffer;
      entry.mLevel = level;
      entry.mType = type;
      for(uint32_t i = 0; i < gConsumers.size(); i++)
         gConsumers[i](level, &entry);
   }
   
   //------------------------------------------------------------------------------
   void printf(const char* fmt,...)
   {
      va_list argptr;
      va_start(argptr, fmt);
      _printf(LogEntry::Normal, LogEntry::General, fmt, argptr);
      va_end(argptr);
   }
   
   void printf(LogEntry::Type type, const char* fmt,...)
   {
      va_list argptr;
      va_start(argptr, fmt);
      _printf(LogEntry::Normal, type, fmt, argptr);
      va_end(argptr);
   }
   
   void warnf(LogEntry::Type type, const char* fmt,...)
   {
      va_list argptr;
      va_start(argptr, fmt);
      _printf(LogEntry::Warning, type, fmt, argptr);
      va_end(argptr);
   }
   
   void errorf(LogEntry::Type type, const char* fmt,...)
   {
      va_list argptr;
      va_start(argptr, fmt);
      _printf(LogEntry::Error, type, fmt, argptr);
      va_end(argptr);
   }
   
   void warnf(const char* fmt,...)
   {
      va_list argptr;
      va_start(argptr, fmt);
      _printf(LogEntry::Warning, LogEntry::General, fmt, argptr);
      va_end(argptr);
   }
   
   void errorf(const char* fmt,...)
   {
      va_list argptr;
      va_start(argptr, fmt);
      _printf(LogEntry::Error, LogEntry::General, fmt, argptr);
      va_end(argptr);
   }
   
   //---------------------------------------------------------------------------
   void addConsumer(ConsumerCallback consumer, uint32_t mask)
   {
      gConsumers.push_back(consumer);
      for (uint32_t i=0; i<32; i++)
      {
         if ((mask & (1<<i)) != 0)
         {
            gNumConsumers[i]++;
         }
      }
   }
   
   void removeConsumer(ConsumerCallback consumer, uint32_t mask)
   {
      auto itr = std::find(gConsumers.begin(), gConsumers.end(), consumer);
      
      if (itr != gConsumers.end())
      {
         gConsumers.erase(itr);
      }
      
      for (uint32_t i=0; i<32; i++)
      {
         if ((mask & (1<<i)) != 0)
         {
            gNumConsumers[i]--;
         }
      }
   }
   
   bool acceptType(LogEntry::Type type)
   {
      return gNumConsumers[type] > 0;
   }
   
}
