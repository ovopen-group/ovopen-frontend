/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef backendConnectionController_hpp
#define backendConnectionController_hpp

#include <vector>
#include "protocolHandler.hpp"

class BackendConnectionController
{
public:
   
   struct UserInfo
   {
      uint32_t uid;
      uint32_t permissions;
      uint32_t invID;
      const char* username;
      uint8_t* pwdHash;
      bool isVIP;
      uint8_t avatarSex;
   };
   
   static std::shared_ptr<OVProtocolHandler> getInstance(uint32_t locationID, uint32_t instanceID);
   static std::shared_ptr<OVProtocolHandler> getBySocketID(uint32_t socketID);
   
   static void registerConnection(std::shared_ptr<OVProtocolHandler>);
   static void registerLocation(std::shared_ptr<OVProtocolHandler> connection);
   static void markLocationReady(std::shared_ptr<OVProtocolHandler>);
   static void unregisterConnection(std::shared_ptr<OVProtocolHandler>);
   static void notifyLogout(std::shared_ptr<OVProtocolHandler> connection);
   
   static void checkInstanceReady(std::shared_ptr<OVProtocolHandler> handler, std::shared_ptr<OVProtocolHandler> waitingHandler);
   
   // Posts login queue message. Requires strand context.
   static void postUserLoginQueued(std::shared_ptr<OVProtocolHandler> handler, const UserInfo &info);
   
   struct InstanceInfo
   {
      uint8_t ipv4Address[4];
      uint16_t port;
   };
   
   // Expend ready queue for handler. Must be executed within handlers strand.
   static void expendReadyQueue(std::shared_ptr<OVProtocolHandler> handler, uint8_t acceptState);
   /*
    
    Wait for a location to be ready, calling resultHandler upon success or error. It's assumed that ready requests are processed
    in the order of dispatch.
    
    resultHandler should be of the form: (std::error_code ec, OVBackendGameConnectionState* state)
    
    */
   template<typename T> static void waitLocationReady(std::shared_ptr<OVProtocolHandler> clientHandler, uint32_t locationID, uint32_t instanceID, T resultHandler)
   {
      std::shared_ptr<OVProtocolHandler> instance = getInstance(locationID, instanceID);
      
      if (instance)
      {
         OVBackendGameConnectionState* state = dynamic_cast<OVBackendGameConnectionState*>(instance->mState);
         
         // NOTE: This should happen in the correct order
         
         instance->getStrand().post([clientHandler, state, instance, resultHandler]{
         
            // Not allowed
            if ((state->mAcceptPermissions & clientHandler->mPermissionsMask.code) == 0)
            {
               std::error_code ec;
               resultHandler(ec, NULL);
               return;
            }
            
            state->mLocationReadyQueue.post([state, instance, resultHandler]{
               std::error_code ec;
               state->mPendingLocationReadyRequests--;
               if (!instance->mClosed)
               {
                  resultHandler(ec, state);
               }
               else
               {
                  resultHandler(ec, NULL);
               }
            });
            state->mPendingLocationReadyRequests++;
            
            // Let server know we should
            UserInfo info;
            info.uid = clientHandler->mUid;
            info.permissions = clientHandler->mPermissionsMask.code;
            info.invID = clientHandler->mLastInventoryID;
            info.username = clientHandler->mUsername.c_str();
            info.pwdHash = &clientHandler->mLocationPWDHash[0];
            info.isVIP = clientHandler->mIsVIP;
            info.avatarSex = clientHandler->mAvatarSex;
            
            postUserLoginQueued(instance, info);
         });
      }
      else
      {
         std::error_code ec;
         resultHandler(ec, NULL);
      }
   }
};

#endif /* backendConnectionController_hpp */
