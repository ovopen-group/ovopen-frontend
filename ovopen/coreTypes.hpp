#ifndef _OV_CORE_TYPES_H_
#define _OV_CORE_TYPES_H_

#if defined(WINDOWS) && defined(__GNUC__)
#define PRIu64 "I64u"
#define PRId64 "I64"
#else
#include <inttypes.h>
#endif

typedef asio::system_timer OVRuntimeTimerType;
typedef asio::system_timer OVSystemTimerType;

#endif
