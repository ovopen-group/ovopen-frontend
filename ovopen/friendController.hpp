/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef friendManager_hpp
#define friendManager_hpp

#include <stdio.h>
#include "protocolHandler.hpp"

class FriendController
{
public:
   
   enum
   {
      FriendLimit = 2500
   };
   
   FriendController() {;}
   ~FriendController() {;}
   
   void addFriend(uint32_t user_id, uint32_t friend_id);
   void removeFriend(uint32_t user_id, uint32_t friend_id);
   
   // Dispatch online updates to each of users friends
   void setOnlineUpdate(uint32_t user_id, uint32_t state);
   
   static void onDispatchOnlineStatusToFriends(std::shared_ptr<OVProtocolHandler> handler, FriendState state);
   static void onUpdateFriendList(std::shared_ptr<OVProtocolHandler> handler);
   static void onDispatchFriendsList(std::shared_ptr<OVProtocolHandler> handler, OVFreeableData* friendPacket);
   
   // Sets a friend record to a required state
   // NOTE: names are required since the client always uses them in update packets
   static void setFriendRecord(uint32_t user_id, uint32_t friendId, FriendState state, const char* friendName);
   static void startFriendRequest(uint32_t user_id, uint32_t friendId, const char* userName, const char* friendName);
   
   static FriendController* sFriendController;
   static FriendController* get();
};

#endif /* friendManager_hpp */
