/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _SERVER_LOGGER_H_
#define _SERVER_LOGGER_H_

#include <asio.hpp>
#include <thread>
#include <mutex>
#include <deque>
#include <chrono>
#include "coreTypes.hpp"
#include "log.hpp"

typedef struct ServerLoggerEntry
{
   LogEntry::Level mLevel;
   uint32_t mType;
   std::string mData;
   double mTime;
   
   ServerLoggerEntry() : mLevel(LogEntry::Normal), mType(0) {;}
   ServerLoggerEntry(LogEntry::Level level, uint32_t type, const char * data, double time) : mLevel(level), mType(type), mData(data), mTime(time) { ; }
   
   ~ServerLoggerEntry() {;}
} ServerLoggerEntry;


class ServerLogger
{
public:
   ServerLogger(const char* logFile);
   ~ServerLogger();
   
   asio::io_service mIOService;
   
   std::deque<ServerLoggerEntry> mQueue;
   asio::io_service::strand mStrand;
   OVRuntimeTimerType mFlushTimer;
   std::chrono::system_clock::time_point mLogStartTime;
   
   std::mutex mLock;
   std::thread *mThread;
   
   FILE* mLogFile;
   bool mPrintToConsole;
   
   void reopen(const char *logFile);
   
   void addLogEntry(uint32_t level, LogEntry *entry);
   void notifyLogEntry();
   void notifyFlush();
   void timerFlush(const std::error_code error);
   
   void run();
   void join();
   void stop();
   
   static ServerLogger *getInstance();
};

#endif
