/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "packetHandlers.hpp"
#include "packet.hpp"
#include "protocolHandler.hpp"
#include <unordered_map>
#include "log.hpp"


int gForceLocationIp;
uint8_t gForceLocationIpAddress[4];

RPCTicketPtr RPCPacketHandler::createFireTicket(OVProtocolHandler& client)
{
   RPCTicketInfo ticketInfo;
   ticketInfo.mRequestID = client.newRPCTicketID();
   ticketInfo.mRPCHandler = this;
   ticketInfo.mFailPacketID = 0;
   ticketInfo.mFailResponseCode = -2;
   return client.beginRPCTicket(ticketInfo);
}

void RPCPacketHandler::endRPCResponseWithTicket(OVProtocolHandler& client, RPCTicketPtr ticket, uint16_t packet_code, uint32_t response_code, bool is_auto)
{
   if (ticket->mCancelled)
      return;
   
   std::shared_ptr<OVProtocolHandler> clientHandler = client.shared_from_this();
   
   if ((clientHandler->mProtocol & OVProtocolHandler::PROTOCOL_BACKEND) == 0)
   {
      if (ticket->mInfo.mRPCHandler->getRPCID() > 9000)
      {
         Log::errorf("Mixed in backend packets with client packets!!\n");
      }
   }
   
   client.mStrand.post([clientHandler, ticket, is_auto, packet_code, response_code]{
      if (ticket->mCancelled)
         return;
      
      uint32_t responsePacketSize = 6+6+6+4;
      PacketMallocData* responseData = PacketMallocData::createOfSize(responsePacketSize);
      MemStream mem(responsePacketSize, responseData->mPtr);
      
      PacketBuilder packet(&mem);
      packet.beginRPC(ticket->mInfo.mRPCHandler->getRPCID(), ticket->mInfo.mRequestID, packet_code, is_auto);
      mem.writeUintBE(response_code);
      packet.commit();
      
      clientHandler->onQueueWriteData(responseData);
      clientHandler->endRPCTicket(ticket);
   });
}

void RPCPacketHandler::createRPCResponseWithoutTicket(OVProtocolHandler& client, uint32_t request_id, uint16_t packet_code, uint32_t response_code, bool is_auto)
{
   uint32_t responsePacketSize = 6+6+6+4+4;
   PacketMallocData* responseData = PacketMallocData::createOfSize(responsePacketSize);
   MemStream mem(responsePacketSize, responseData->mPtr);
   
   PacketBuilder packet(&mem);
   packet.beginRPC(getRPCID(), request_id, packet_code, is_auto);
   mem.writeUintBE(response_code);
   packet.commit();
   
   client.queuePacketData(responseData);
}

class RPC_TorqueRPCHandler : public PacketHandler
{
public:
   DECLARE_PACKET_HANDLER(AutoPacketRPCHandler, true, OVProtocolHandler::PROTOCOL_COMMON);
   
   
   virtual int preHandle(OVProtocolHandler& client, OVPacketHeader* header)
   {
      if (header->isCompressed() || header->getNativeSize() < 8)
      {
         Log::client_printf(client, LogEntry::General, "Sent invalid RPC packet!");
         return -1;
      }
      
      if (client.cantAcceptMoreRPCRequests())
      {
         MemStream stream(8, header->getData());
         
         uint16_t ident = 0;
         uint32_t reqID = 0;
         
         stream.readUintBE(ident);
         stream.readUintBE(reqID);
         
         if (!client.hasRPCTicket(reqID))
            return 0;
      }
      
      return 1;
   }
   
   virtual void handle(OVProtocolHandler& client, OVPacketInfo info)
   {
      MemStream stream(info.size, info.data);
      
      uint16_t ident = 0;
      
      RPCTicketInfo ticketInfo;
      stream.readUintBE(ident);
      stream.readUintBE(ticketInfo.mRequestID); // this is managed entirely by the client
      
      info.data = ((uint8_t*)info.data) + stream.getPosition();
      info.size -= stream.getPosition();
      
      //request_id >>= 16; // to get REAL request number
      
      //info.cleanup();
      //return;
      
      RPCPacketHandler* handler = RPCPacketHandler::getHandlerForRPCCommand(ident);
      if (handler)
         Log::client_printf(client, LogEntry::Debug, "RPC_TorqueRPCHandler got handler %s", handler->getName());
      else
         Log::client_printf(client, LogEntry::Debug, "RPC_TorqueRPCHandler no handler for %u", (uint32_t)ident);
         
      if (handler && ((handler->getRPCProtocol() & client.mProtocol) != 0) )
      {
         Log::client_printf(client, LogEntry::Debug, "RPC_TorqueRPCHandler handler accepted");
         // We must decode the inner packet in this case, since it may be further compressed or encrypted
         // (though this is usually more the case with packets from the server)
         OVPacketHeader* subPacket = (OVPacketHeader*)stream.getPositionPtr();
         if (subPacket->isValidUnprocessed())
         {
            RPCTicketPtr ticket;
            
            // If we are a client, we'll be looking for RPC tickets. Otherwise we'll make them.
            
            // Tickets may technically be created either by the client or server.
            // If we are the server, we need to create tickets as new client rpc ids come in.
            // If we are the client, we need to create tickets as new server rpc ids come in.
            
            bool needToMakeTicket = false;
            bool isClient = (client.mProtocol & OVProtocolHandler::PROTOCOL_CLIENT) != 0;
            if (isClient)
            {
               needToMakeTicket = RPCTicket::isServerTicketID(ticketInfo.mRequestID);
            }
            else
            {
               needToMakeTicket = !RPCTicket::isServerTicketID(ticketInfo.mRequestID);
            }
            Log::client_printf(client, LogEntry::Debug, "RPC_TorqueRPCHandler needToMakeTicket=%s", needToMakeTicket ? "YES" : "NO");
            
            if (!needToMakeTicket)
            {
               ticket = client.findRPCTicket(ticketInfo.mRequestID);
               if (!ticket)
               {
                  Log::client_printf(client, LogEntry::Debug, "Server sending unknown RPC request, ignoring");
                  return;
               }
            }
            else
            {
               // First check if we need to rate-limit
               
               // BEGIN RPC
               ticketInfo.mRPCHandler = handler;
               ticket = client.beginRPCTicket(ticketInfo);
               Log::client_printf(client, LogEntry::Debug, "Beginning handling RPC code %i", handler->getRPCID());
               if (!ticket)
               {
                  // Usually too many tickets
                  handler->createRPCResponseWithoutTicket(client, ticketInfo.mRequestID, 0, -2);
                  return;
               }
            }
            
            // Decode
            OVPacketInfo subInfo = DecodePacket(subPacket, &client.mEncryptionKey, true);
            if (subInfo.freeData)
            {
               // Don't need main info anymore
               info.cleanup();
            }
            else
            {
               // Reuse main freeData for sub info
               subInfo.freeData = info.freeData;
               info.freeData = NULL;
            }
            
            handler->handle(client, subInfo, ticket); // NOTE: handler should end rpc requests
         }
      }
      else
      {
         Log::client_printf(client, LogEntry::Debug, "TODO: RPC id=%u code=%u", ident, ticketInfo.mRequestID);
         info.cleanup();
      }
   }
};
IMPL_PACKET_HANDLER(RPC_TorqueRPCHandler);

std::unordered_map<uint32_t, PacketHandler*> gPacketHandlerRegistry;
std::unordered_map<uint16_t, RPCPacketHandler*> gRPCHandlerRegistry;

PacketHandler* PacketHandler::gRoot;
RPCPacketHandler* RPCPacketHandler::gRoot;

PacketHandler* PacketHandler::getHandlerForPacket(OVPacketHeader* header)
{
   uint32_t hash = header->getPacketHash();
   return gPacketHandlerRegistry[hash];
}

RPCPacketHandler* RPCPacketHandler::getHandlerForRPCCommand(uint16_t code)
{
   return gRPCHandlerRegistry[code];
}

void PacketHandler::registerKlass(PacketHandler* klass)
{
   klass->mListNext = gRoot;
   gRoot = klass;
}

void RPCPacketHandler::registerKlass(RPCPacketHandler* klass)
{
   klass->mListNext = gRoot;
   gRoot = klass;
}

void PacketHandler::initKlasses()
{
   for (PacketHandler* handler = gRoot; handler; handler = handler->mListNext)
   {
      gPacketHandlerRegistry[handler->getPacketHash()] = handler;
   }
}

void RPCPacketHandler::initKlasses()
{
   for (RPCPacketHandler* handler = gRoot; handler; handler = handler->mListNext)
   {
      gRPCHandlerRegistry[handler->getRPCID()] = handler;
   }
}


