/*
ovopen-server
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "packet.hpp"
#include "log.hpp"
#include "blowfish.h"
#include "sha1.hpp"

const uint32_t MaxCompressedPacketSize = 65536;

void OVPacketInfo::ensureFreeable()
{
   if (freeData)
      return;
   
   freeData = std::make_shared<SharedMallocData>();
   freeData->mSize = size;
   freeData->mPtr = (uint8_t*)malloc(size);
   memcpy(freeData->mPtr, data, size);
   data = freeData->mPtr;
}

void OVPacketInfo::cleanup()
{
   freeData = NULL;
   data = NULL;
   isValid = false;
}

// Decodes a packet, returning a new block of allocated data if required
// Returns isValid=false if the packet was in some way invalid
OVPacketInfo DecodePacket(OVPacketHeader* rawPacket, BLOWFISH_CONTEXT* encKey, bool allowDecompress)
{
   OVPacketInfo outInfo;
   outInfo.code = rawPacket->getNativeId();
   outInfo.isValid = false;
   outInfo.isAuto = rawPacket->isAuto();
   outInfo.size = rawPacket->getNativeSize();
   outInfo.data = rawPacket->getData();
   
   bool statEnc=false;
   bool statComp=false;
   
   // Decrypt raw packet first. We'll do this in-place
   if (rawPacket->isEncrypted())
   {
      if (!rawPacket->isProcessed())
      {
        if (!DecryptPacketInPlace(rawPacket, encKey))
         {
            if (Log::acceptType(LogEntry::Debug)) Log::printf(LogEntry::Debug, "Invalid encrypted packet!");
            return outInfo;
         }
      }
      
      statEnc=true;
   }
   
   // Decompress packet, creating a new buffer for output
   if (rawPacket->isCompressed() && allowDecompress)
   {
      statComp=true;
      // Decode in chunks, though limit the actual output size to avoid fork bombs
      uint32_t currentOutBytes = 0;
      uint8_t* mem = NULL;
      
      z_stream stream;
      stream.zalloc = NULL;
      stream.zfree = NULL;
      stream.opaque = NULL;
      stream.next_in = rawPacket->getData();
      stream.avail_in = rawPacket->getNativeSize();
      stream.total_in = 0;
      inflateInit(&stream);
      
      int retVal = 0;
      
      for (currentOutBytes=0; currentOutBytes < MaxCompressedPacketSize; )
      {
         uint8_t tmpBuffer[16384];
         stream.next_out = &tmpBuffer[0];
         stream.avail_out = sizeof(tmpBuffer);
         stream.total_out = 0;
         
         retVal = inflate(&stream, Z_SYNC_FLUSH);
         if (retVal < 0)
         {
             if (Log::acceptType(LogEntry::Debug)) Log::printf(LogEntry::Debug, "Error in compressed stream!");
            inflateEnd(&stream);
            if (mem) free(mem);
            return outInfo;
         }
         
         // Append bytes to output
         uint32_t nextOutBytes = currentOutBytes + (uint32_t)stream.total_out;
         mem = (uint8_t*)realloc(mem, (nextOutBytes + 0x7) & ~0x7);
         memcpy(&mem[currentOutBytes], &tmpBuffer[0], stream.total_out);
         currentOutBytes = nextOutBytes;
         
         if (retVal == Z_STREAM_END || currentOutBytes > OVPacketHeader::MaxCompressedSize)
            break;
      }
      
      inflateEnd(&stream);
      
      if (retVal != Z_STREAM_END)
      {
         if (Log::acceptType(LogEntry::Debug)) Log::printf(LogEntry::Debug, "Invalid compressed stream");
         if (mem) free(mem);
         return outInfo;
      }
      
      // Set return data
      outInfo.freeData = std::make_shared<SharedMallocData>();
      outInfo.freeData->mPtr = mem;
      outInfo.freeData->mSize = currentOutBytes;
      outInfo.data = outInfo.freeData->mPtr;
      outInfo.size = currentOutBytes;
      rawPacket->setDecompressed();
   }
   
#ifdef DEBUG_PACKET_STATS
   if (statEnc && statComp)
      printf("Packet %u COMP ENC\n", rawPacket->getNativeId());
   else if (statEnc)
      printf("Packet %u ENC\n", rawPacket->getNativeId());
   else if (statComp)
      printf("Packet %u COMP\n", rawPacket->getNativeId());
   else
      printf("Packet %u NORMAL\n", rawPacket->getNativeId());
#endif
   
   outInfo.isValid = true;
   return outInfo;
}

bool PacketAlreadyDecrypted(OVPacketHeader* rawPacket, BLOWFISH_CONTEXT* encKey)
{
   uint32_t requiredSize = rawPacket->getPacketSize();
   uint8_t* encPtr = rawPacket->getData();
   uLong start_adler = adler32(0,0,0);
   uLong expected_adler = adler32(start_adler, encPtr, rawPacket->getNativeSize());
   
   uint32_t actual_adler = OV_BYTESWAP32(((uint32_t*)(encPtr + rawPacket->getNativeSize()))[0]);
   if (actual_adler == expected_adler)
   {
      assert(false);
      return true;
   }
   
   return false;
}

bool DecryptPacketInPlace(OVPacketHeader* rawPacket, BLOWFISH_CONTEXT* encKey)
{
   uint32_t requiredSize = rawPacket->getPacketSize();
   uint8_t* encPtr = rawPacket->getData();
   
#ifdef DEBUG
   // Check if adler matches. If it does, this packet is already decrypted!
   PacketAlreadyDecrypted(rawPacket, encKey);
#endif
  
   // Start decrypting
   for (uint32_t i=0; i<requiredSize; i+=8)
   {
      uint8_t tmp[8];
      ov_decipher((unsigned int*)(encPtr+i), (unsigned int*)&tmp[0], encKey);
      memcpy(encPtr+i, &tmp[0], 8);
   }

   // Calc adler to verify
   uLong start_adler = adler32(0,0,0);
   uLong expected_adler = adler32(start_adler, encPtr, rawPacket->getNativeSize());
   
   uint32_t actual_adler = OV_BYTESWAP32(((uint32_t*)(encPtr + rawPacket->getNativeSize()))[0]);
   return actual_adler == expected_adler;
}

void EncryptPacketInPlace(OVPacketHeader* rawPacket, BLOWFISH_CONTEXT* encKey)
{
   uint32_t requiredSize = rawPacket->getPacketSize() + 6;
   
   // Clear data at the end
   uint8_t* encPtr = rawPacket->getData();
   uint32_t startRand = rawPacket->getNativeSize() + 4;
   uint32_t endRand = rawPacket->getPacketSize();
   if (endRand > startRand)
   {
      memset(encPtr + startRand, '\0', endRand-startRand);
   }
   
   // Calc adler
   uLong start_adler = adler32(0,0,0);
   uLong expected_adler = adler32(start_adler, encPtr, rawPacket->getNativeSize());
   
   // Set adler
   ((uint32_t*)(encPtr + rawPacket->getNativeSize()))[0] = OV_BYTESWAP32((uint32_t)expected_adler);
   
   // Start encrypting
   for (uint32_t i=0; i<endRand; i+=8)
   {
      uint8_t tmp[8];
      ov_encipher((unsigned int*)(encPtr+i), (unsigned int*)&tmp[0], encKey);
      memcpy(encPtr+i, &tmp[0], 8);
   }
}

// Encodes a packet according to the header requirements, creating a freeable block of data in the process.
// Returns NULL if it wasn't possible to create a packet.
// NOTE: in the case of RPC packets, the outer packet can only be encrypted and the inner packet can
// only be compressed.
OVFreeableData* EncodePacket(OVPacketHeader* rawPacket, BLOWFISH_CONTEXT* encKey, uint32_t dataSize, uint8_t* data, bool padRPC, bool padEnc)
{
   PacketMallocData* dat = new PacketMallocData();
   uint32_t dataPadding = 6;
   if (padRPC)
      dataPadding += 12;
   
   // Compress packet to new stream
   if (rawPacket->isCompressed())
   {
      // Need to
      z_stream stream;
      stream.zalloc = NULL;
      stream.zfree = NULL;
      stream.opaque = NULL;
      stream.next_in = data;
      stream.avail_in = dataSize;
      stream.total_in = 0;
      
      uint32_t initialSize = dataSize + 100 + dataPadding;
      initialSize = (initialSize + 0x7) & ~0x7;
      uint8_t* predictedBuf = (uint8_t*)malloc(initialSize);
      
      stream.next_out = &predictedBuf[dataPadding];
      stream.avail_out = initialSize-dataPadding;
      stream.total_out = 0;
      
      int retVal = deflateInit(&stream, 0x9);
      
      retVal = deflate(&stream, Z_SYNC_FLUSH);
      
      if (retVal != Z_OK)
      {
         if (Log::acceptType(LogEntry::Debug)) Log::printf(LogEntry::Debug, "Error during compression!");
         delete dat;
         return NULL;
      }
      
      retVal = deflate(&stream, Z_FINISH);
      
      if (retVal != Z_STREAM_END)
      {
         if (Log::acceptType(LogEntry::Debug)) Log::printf(LogEntry::Debug, "Error during end compression!");
         delete dat;
         return NULL;
      }
      
      stream.next_in = NULL;
      retVal = deflateEnd(&stream);
      
      if (stream.total_out > 0xFFFF)
      {
         if (Log::acceptType(LogEntry::Debug)) Log::printf(LogEntry::Debug, "Packet too large");
         free(predictedBuf);
         delete dat;
         return NULL;
      }
      
      memcpy(&predictedBuf[dataPadding-6], rawPacket, 6);
      
      // Make sure we are big enough to fit encryption in the base packet if required
      uint32_t requiredSize = stream.total_out+4; // Compressed packet data
      requiredSize += (dataPadding-6); // RPC data + compressed packet header
      requiredSize = (((requiredSize) + 0x7) & ~0x7) + 6; // padding for encrypted packet + outer packet header
      
      if (initialSize != requiredSize)
      {
         predictedBuf = (uint8_t*)realloc(predictedBuf, requiredSize);
         assert(predictedBuf);
      }
      
      dat->mSize = requiredSize;
      dat->mPtr = predictedBuf;
      
      rawPacket = (OVPacketHeader*)&predictedBuf[dataPadding-6];
      rawPacket->setNativeSize(stream.total_out);
   }
   
   // Encrypt the packet
   if (rawPacket->isEncrypted())
   {
      uint32_t requiredSize = rawPacket->getPacketSize() + dataPadding;
      
      // Allocate a buffer of requiredSize if we haven't already
      if (!dat->mPtr)
      {
         uint8_t* ptr = (uint8_t*)malloc(requiredSize);
         dat->mPtr = ptr;
         dat->mSize = requiredSize;
         memcpy(&ptr[dataPadding-6], rawPacket, 6 + rawPacket->getNativeSize());
      }
      
      // Clear data at the end
      uint8_t* encPtr = dat->mPtr+dataPadding;
      uint32_t startRand = rawPacket->getNativeSize() + 4;
      uint32_t endRand = rawPacket->getPacketSize();
      if (endRand > startRand)
      {
         memset(encPtr + startRand, '\0', endRand-startRand);
      }
      
      // Calc adler
      uLong start_adler = adler32(0,0,0);
      uLong expected_adler = adler32(start_adler, encPtr, rawPacket->getNativeSize());
       
       // Set adler
      ((uint32_t*)(encPtr + rawPacket->getNativeSize()))[0] = OV_BYTESWAP32((uint32_t)expected_adler);
    
      // Start encrypting
      for (uint32_t i=0; i<endRand; i+=8)
      {
         uint8_t tmp[8];
         ov_encipher((unsigned int*)(encPtr+i), (unsigned int*)&tmp[0], encKey);
         memcpy(encPtr+i, &tmp[0], 8);
      }
      
      assert(dat->mSize == rawPacket->getPacketSize()+6);
   }
   else if (!padEnc)
   {
      // Make sure the size isn't padded
      dat->mSize = rawPacket->getPacketSize() + dataPadding;
   }
   
   // Duplicate data if we didn't encrypt or compress it
   if (!dat->mPtr)
   {
      dat->mPtr = (uint8_t*)malloc(dataSize+dataPadding);
      memcpy(dat->mPtr+(dataPadding-6), rawPacket, 6);
      memcpy(dat->mPtr+dataPadding, data, dataSize);
   }
   
   return dat;
}

OVFreeableData* PacketMallocData::cloneMultiple()
{
   PacketMultipleUserData* clone = PacketMultipleUserData::createFromData(size(), ptr());
   return clone;
}

OVFreeableData* PacketMultipleUserData::cloneMultiple()
{
   PacketMultipleUserData* clone = new PacketMultipleUserData();
   clone->mData = mData;
   return clone;
}
