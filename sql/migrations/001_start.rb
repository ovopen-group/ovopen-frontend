Sequel.migration do
  change do
    create_table(:users, :ignore_index_errors=>true) do
      primary_key :id, type: Integer
      String :username, size: 32, null:true, default:nil, collate: 'nocase'
      String :password, size: 128, null:true, default:nil
      
      Integer :is_vip, size: 1, null:false, default:0
      Integer :permissions_mask, type: :Bignum, size: 11, null:false, default:0
      Integer :banned_until_time, type: :Bignum, size: 11, null:false, default:0
      Integer :teleport_permission, size: 11, null:false, default:0
      Integer :travel_permission, size: 11, null:false, default:0
      
      Integer :cc, size: 11, null:false, default:0
      Integer :pp, size: 11, null:false, default:0

      Integer :last_instance_id, size: 11, null:false, default:-1  
      Integer :last_item_id, size: 11, null:false, default:0
      Integer :default_house_id, size: 11, null:false, default:0
      Integer :avatar_sex, size: 2, null:false, default:0
      
      index :username, :unique=>true
    end

    create_table(:friends, :ignore_index_errors=>true) do
      primary_key :id, type: Integer

      String :friend_name, size: 32, null:true, default:nil, collate: 'nocase'
      Integer :accept_state, size: 11, null:false, default:0

      Integer :user_id, size: 11, null:false, default:-1  
      Integer :friend_id, size: 11, null:false, default:0
      
      index [:user_id, :friend_id], unique: true
    end

    create_table(:house_plots, :ignore_index_errors=>true) do
      primary_key :id, type: Integer

      Integer :type_id, size: 11, null:false, default:0
      Integer :max_furniture, size: 11, null:false, default:0

      File :data, null: false
      String :name, size: 64, null:false, default:''
      String :poi_name, size: 64, null:false, default:''
      String :instance_name, size: 64, null:false, default:''
      String :location_name, size: 64, null:false, default:''
      String :description, size: 255, null:false, default:'', text: true

      Integer :cc, size: 11, null:false, default:0
      Integer :pp, size: 11, null:false, default:0

      Integer :owner_id, size: 11, null:true, default:-1  
      Integer :instance_id, size: 11, null:true, default:-1  
      Integer :location_id, size: 11, null:true, default:-1  
      Integer :local_id, size: 11, null:true, default:-1
      Integer :location_instance_id, size: 11, null:true, default:nil
      
      index [:local_id,:location_instance_id], name: :house_plot_index, :unique => true
    end

    create_table(:inventory_items, :ignore_index_errors=>true) do
      primary_key :id, type: Integer

      Integer :type_id, size: 11, null:false, default:0
      Integer :item_id, size: 11, null:false, default:0
      Integer :datablock_id, size: 11, null:false, default:0
      Integer :user_id, size: 11, null:false, default:0

      String :name, size: 80, null:false, default:''

      index :user_id
    end

    create_table(:location_instances, :ignore_index_errors=>true) do
      primary_key :id, type: Integer

      String :name, size: 64, null:false, default:''

      Integer :instance_id, size: 11, null:false, default:-1  
      Integer :location_id, size: 11, null:false, default:-1  
      Integer :owner_id, size: 11, null:false, default:-1  
      Integer :permissions, type: :Bignum, size: 11, null:false, default:0

      Integer :socket_id, type: :Bignum, size: 11, null:true, default:nil

      index :location_id
    end

    create_table(:locations, :ignore_index_errors=>true) do
      primary_key :id, type: Integer

      String :name, size: 64, null:false, default:''

      index :name
    end

    create_table(:patches, :ignore_index_errors=>true) do
      primary_key :id, type: Integer

      String :bundle_name, size: 64, null:false, default:''
      String :bundle_version, size: 64, null:false, default:''
      String :source_bundle_version, size: 64, null:false, default:''
      String :stage_bundle_version, size: 64, null:false, default:''

      String :filename, size: 255, null:true, default:''
      String :path, size: 255, null:true, default:nil

      Integer :crc, size: 60, type: :Bignum, :default => 0
      Integer :size, size: 60, type: :Bignum, :default => 0

      Integer :order_id, size: 11, null:false, default:0
      Integer :chain_length, size: 11, null:false, default:0
      
      index :bundle_name
      index :source_bundle_version
    end

    create_table(:patch_mirrors, :ignore_index_errors=>true) do
      primary_key :id, type: Integer

      String :name, size: 255, null:false, default:''
      String :hostname, size: 255, null:false, default:''
      String :path, size: 255, null:true, default:nil
    end

    create_table(:resource_categories, :ignore_index_errors=>true) do
      primary_key :id, type: Integer

      String :name, size: 45, null:false, default:'', collate: 'nocase'

      Integer :object_id, size: 11, null:false, default:0
      Integer :parent_id, size: 11, null:false, default:0
      
      index :parent_id
    end

    create_table(:resource_datas, :ignore_index_errors=>true) do
      primary_key :id, type: Integer

      String :name, size: 64, null:true, default:nil
      File :data, null: false

      Integer :object_id, size: 11, null:false, default:0
      Integer :category_id, size: 11, null:false, default:0
      
      index :category_id
    end

    create_table(:store_folders, :ignore_index_errors=>true) do
      primary_key :id, type: Integer

      Integer :parent_id, size: 11, null:false, default:0
      String :name, size: 45, null:true, default:nil, collate: 'nocase'
      
      index :parent_id
    end

    create_table(:store_items, :ignore_index_errors=>true) do
      primary_key :id, type: Integer

      Integer :folder_id, size: 11, null:false, default:0
      Integer :resource_data_id, size: 11, null:false, default:0
      
      index :folder_id
    end


    create_table(:transactions, :ignore_index_errors=>true) do
      primary_key :id, type: Integer
      
      Integer :action_id, size: 11, null:false, default:0

      Integer :cc, size: 11, null:false, default:0
      Integer :pp, size: 11, null:false, default:0

      Integer :item_id, size: 11, null:false, default:0
      Integer :user_id, size: 11, null:false, default:0
      Integer :obj_server_id, size: 11, null:false, default:0

      Integer :created_at, type: :Bignum, size: 11, null:false, default:0
      
      index :user_id
    end
  end
end
