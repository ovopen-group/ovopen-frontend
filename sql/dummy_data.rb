require 'rubygems'
require 'sequel'
require 'sqlite3'

puts ARGV.inspect

if ARGV.count < 1
	puts "usage: dummy_data.rb <connection string>"
	exit(1)
end

$db = Sequel.connect(ARGV[0])

$db[:users].delete
$db[:users].insert(:id => 1, :username => 'root', :password => '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', :permissions_mask => 4294967295, :is_vip => 1)
$db[:users].insert(:id => 2, :username => 'backend', :password => '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', :permissions_mask => 4294967295)
$db[:users].insert(:id => 3, :username => 'anonymous', :password => '532bb2f20a1782dcd572e4472f0c49fa2f7b1028', :permissions_mask => 255)

$dats = [
[9, 'Hub'],
[10, 'Island'],
[14, 'paradisefree'],
[18, 'metroviewdeluxe'],
[19, 'paradisedeluxe'],
[20, 'metroviewfree'],
[21, 'Tutorial'],
[23, 'guide_hall'],
[24, 'icefall'],
[25, 'ancientmoon'],
[28, 'metroview_ph'],
[29, 'soldeluxe'],
[30, 'solfree'],
[32, 'splatball'],
[36, 'paradise_estates'],
[37, 'sol_estates'],
[38, 'echo_canyon'],
[39, 'lodges'],
[40, 'magical_forest'],
[41, 'burning_lands'],
[42, 'paradise_land'],
[43, 'sol_land'],
[44, 'mountainview_land'],
[45, 'burning_housing'],
[51, 'trials_of_the_ancients'],
[52, 'echo_canyon_land'],
[53, 'forest_land'],
[54, 'forest_estates'],
[55, 'tang_coast'],
[56, 'metro_ph_deluxe'],
[58, 'ancient_cultures'],
[59, 'sharktank'],
[60, 'Events'],
[61, 'suburbia'],
[62, 'paradise_megaplots'],
[63, 'furniture_shop'],
[64, 'candyland'],
[65, 'candyland_plots'],
[66, 'casino'],
[67, 'winter_wonderland'],
[68, 'valentines'],
[69, 'mardi_gras'],
[70, 'spd'],
[100, 'test_area']
]

$db[:locations].delete
$dats.each do |d|
	$db[:locations].insert({:id => d[0], :name => d[1]})
end


$db[:location_instances].delete
$db[:location_instances].insert({:id => 1, :name => 'Temple Test', :instance_id => 0, :location_id => 51, :owner_id => 2, :permissions => 16777200})
$db[:location_instances].insert({:id => 2, :name => 'Test Area', :instance_id => 0, :location_id => 100, :owner_id => 2, :permissions => 0})



$db[:patch_mirrors].delete
$db[:patch_mirrors].insert({:name => 'Default', :hostname => '127.0.0.1', :path => 'ovpatch/'})


