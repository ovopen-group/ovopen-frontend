# ovopen-frontend

This provides a master server suitable for supporting onverse clients and location servers.

## Building

### Requirements

You will need a Linux, OSX, or Windows system to build (other platforms may work but are untested). In addition your compiler must support at least C++17.

Since currently the only database driver implemented is sqlite, no other dependencies are required.

## Installing

Assuming all requirements are met, make a build folder and supply the paths to where each lib is installed (if the defaults do not suffice)

	mkdir build
	cmake ..
	make

# Running

First you will want to modify config.ini to fit your setup.

You should create a new sqlite db based on the template. i.e.

	sqlite3 ovopen.db < sql/sqlite_db_template.sql

You can then boot the server by running the following:

	./ovopen

Automated unit tests can also be run via:

	./ovopen_test

In order to connect a client, it's best to change your HOSTS file so `onverseworld.onverse.com` and `patchdata.onverse.com` points to your server. You should be able to login using the default username + password combo `root`, `password`.

Right now there are no tools to manipulate the database so you will need to add users and location instances manually. 

# Recreating DB template

You will need ruby with the `sequel` gem installed, in addition to sqlite3 tools. Then simply run the following:

	sequel -m sql/migrations sqlite://ovopen.db
	ruby sql/dummy_data.rb sqlite://ovopen.db
	sqlite3 ovopen.db .dump > sql/sqlite_db_template.sql

It's advisable that `PRAGMA journal_mode=WAL;` is specified in the template for optimal performance.

# Adding a user

Simply add a new record into the `users` table, e.g.:

	INSERT INTO USERS(username,password,permissions_mask) VALUES ('root', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 4294967295);

The password should be a SHA1 hexdigest, and the permissions mask should be the integer value of the permissions bitset as defined by the `UserPermissions` struct in code (essentially, each higher bit set = higher access level).

Note that as passwords are currently just simple digests, it is not advisable to use a non-unique password. 

# Connecting Location Servers

A special user, `backend` is provided which is used to handle connecting location servers. This user needs to have the `LEVEL_SUPER_ADMIN` permission set in order for login to work. (an example backend user with the password `password` is already provided).

You'll need to specify a port which location services can connect to. e.g.:

	[backend]
	listen_port=3725
	packet_limit_kbps=400

For every location instance you want to connect to the frontend, a corresponding entry needs to be present in the `location_instances` table. e.g. assuming backend is user 2 and you want users with permission mask `16777200` to connect:

	INSERT INTO location_instances(instance_id, location_id, owner_id, permissions) VALUES (0, 'Temple Test', 2, 16777200);

Then make sure your client is loaded up with the client scripts, data, and server scripts. Launch a client using something like:

	onverse.exe --entry onverse/server/serverMain.cs -config temple.builder -port 28000 -nopatch -id 1 -user backend -regip [frontend server address] -pass password -regport 3725

The client should then connect to the frontend. You'll then be able to go to the location after logging in using a regular client - in this particular example, the temple trials area.

# Connecting clients

If you want the patcher to connect to your server, you will need to change `patch.dat` to point to your server. This can best be done with the `patch_tool` tool available in `ovopen-tools`. e.g.

	patch_tool dumpcfg patch.dat patch.json
	[modify patch.json, change `server` to your server address]
	patch_tool savecfg patch.json patch.dat

For the actual client, you will need to set the `$pref::MasterServer::default_address` pref to the address of your server, e.g.:

	$pref::MasterServer::default_address = 127.0.0.1;

And then login normally.

